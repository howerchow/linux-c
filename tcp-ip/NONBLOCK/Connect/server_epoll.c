#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <errno.h>
#include <fcntl.h>
#include <asm-generic/socket.h>

#define POLL_SIZE 1024

int
util_make_socket_nonblocking(int fd)
{
#ifdef _WIN32
    {
        unsigned long nonblocking = 1;
        if (ioctlsocket(fd, FIONBIO, &nonblocking) == SOCKET_ERROR) {
            event_sock_warn(fd, "ioctlsocket(%d, FIONBIO, &%lu)", (int)fd, (unsigned long)nonblocking);
            return -1;
        }
    }
#else
    {
        int flags;
        if ((flags = fcntl(fd, F_GETFL, NULL)) < 0) {
            fprintf(stderr,"fcntl(%d, F_GETFL)\n", fd);
            return -1;
        }
        if (!(flags & O_NONBLOCK)) {
            if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1) {
                fprintf(stderr,"fcntl(%d, F_SETFL)", fd);
                return -1;
            }
        }
    }
#endif
    return 0;
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        fprintf(stderr,"Useage: %s <port>\n" ,argv[0]);
        exit(EXIT_FAILURE);
    }

    int listen_sock = socket(AF_INET, SOCK_STREAM|SOCK_NONBLOCK, IPPROTO_TCP); /* TCP */
    if(listen_sock == -1){
        perror("socket() error! %s");
        exit(EXIT_FAILURE);
    }
    int one = 1;
    /* REUSEADDR on Unix means, "don't hang on to this address after the
     * listener is closed."  On Windows, though, it means "don't keep other
     * processes from binding to this address while we're using it. */
    setsockopt(listen_sock, SOL_SOCKET, SO_REUSEADDR, (void*) &one,
               sizeof(one));
    /* REUSEPORT on Linux 3.9+ means, "Multiple servers (processes or
     * threads) can bind to the same port if they each set the option. */
    setsockopt(listen_sock, SOL_SOCKET, SO_REUSEPORT, (void*) &one,
               sizeof(one));

    struct sockaddr_in serv_addr = {
        .sin_family      = AF_INET,
        .sin_addr.s_addr = htonl(INADDR_ANY),
        .sin_port        = htons(atoi(argv[1]))
    };

    int status = bind(listen_sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
    if (status == -1) {
        fprintf(stderr, "bind(): %s\n", strerror(status));
        exit(EXIT_FAILURE);
    }

    status = listen(listen_sock, SOMAXCONN);
    if (status == -1) {
        fprintf(stderr, "listen(): %s\n", strerror(status));
        exit(EXIT_FAILURE);
    }

    int epfd = epoll_create(1);

    struct epoll_event ev = {
        .events = EPOLLIN,
        //.events = EPOLLIN | EPOLLET,
        .data.fd = listen_sock
    };
    epoll_ctl(epfd, EPOLL_CTL_ADD, listen_sock, &ev);

    struct epoll_event events[POLL_SIZE] = {0};

    /* client */
    int clnt_sock = -1;
    struct sockaddr_in client_addr = {0};
    socklen_t client_addr_len;
    client_addr_len = sizeof(client_addr);

    int ready;
    while(true) {
        ready = epoll_wait(epfd, events, POLL_SIZE, 1000);
        if (ready == -1) {
            if (errno == EINTR) { /* interrupt */
                continue;
            } else { /* error */
                break;
            }
        } else if (ready == 0) { /* timeout */
            continue;
        }

        for (size_t i = 0; i < ready ; ++i) {
            if (events[i].events & EPOLLIN) {          /* READ */
                if (events[i].data.fd == listen_sock) {
                    clnt_sock = accept(listen_sock, (struct sockaddr*)&client_addr, &client_addr_len);
                    if (clnt_sock == -1) {
                        printf("accept(): ");
                        exit(EXIT_FAILURE);
                    } else {
                        printf("Connected client\n");
                    }
                    util_make_socket_nonblocking(clnt_sock);
                    //ev.events = EPOLLIN | EPOLLOUT | EPOLLET;
                    ev.events = EPOLLIN | EPOLLOUT;
                    ev.data.fd = clnt_sock;
                    if(epoll_ctl(epfd, EPOLL_CTL_ADD, clnt_sock, &ev) == -1) {
                        perror("epoll_ctl()");
                        exit(EXIT_FAILURE);
                    }
                } else {
                    char buffer[1] = {0}; /*recv one byte everytime*/
                    int n = recv(events[i].data.fd, buffer, sizeof(buffer), 0);
                    if (n == -1) {
                        if(errno != EWOULDBLOCK && errno != EINTR)  { /*error*/
                            epoll_ctl(epfd, EPOLL_CTL_DEL, events[i].data.fd, NULL);
                            close(events[i].data.fd);
                        }
                    }
                    if (n > 0) {
                        buffer[n] = '\0';
                        printf("receive message from client: %s\n", buffer);
                    } else if (n == 0) { /*client shutdow*/
                        epoll_ctl(epfd, EPOLL_CTL_DEL, events[i].data.fd, NULL);
                        close(events[i].data.fd);
                    }
                }
            } else if (events[i].events & EPOLLOUT) {  /* WRITE */
                                                      //printf("EPOLLOUT triggerd, client fd: %d \n", events[i].data.fd);

            } else if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP) ) { /* ERROR */
                fprintf(stderr, "epoll error\n");
                close(events[i].data.fd);
                continue;
            }
        }
    }

    close(listen_sock);
    close(clnt_sock);
    return 0;
}
