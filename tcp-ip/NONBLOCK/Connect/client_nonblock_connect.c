#include <arpa/inet.h>
#include <asm-generic/errno.h>
#include <asm-generic/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <unistd.h>

/* 异步connect*/

#define SERVER_ADDR "127.0.0.1"
#define SERVER_PORT 3000
#define SERVER_DATA "hello world"

int main() {
  /* 1. 创建非阻塞socket */
  int clientfd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
  if (clientfd == -1) {
    fprintf(stderr, "create client socket failed\n");
    exit(EXIT_FAILURE);
  }

  /* 2. 连接服务器 */
  struct sockaddr_in serveraddr = {.sin_family = AF_INET,
                                   .sin_addr.s_addr = inet_addr(SERVER_ADDR),
                                   .sin_port = htons(SERVER_PORT)};

  while (true) {
    int ret =
        connect(clientfd, (struct sockaddr *)&serveraddr, sizeof serveraddr);
    if (ret == 0) { /* 连接成功 */
      printf("connect to server successfully\n");
      close(clientfd);
      return 0;
    } else if (ret == -1) {
      if (errno == EINTR) { /* 被信号中断 */
        printf("Interruptted by signal, Try again\n");
        continue;
      } else if (errno == EINPROGRESS) { /* 正在连接 */
        break;
      } else { /* 真的出错 */
        close(clientfd);
        return -1;
      }
    }
  }

  fd_set writeset;
  FD_ZERO(&writeset);
  FD_SET(clientfd, &writeset);
  struct timeval tv = {.tv_sec = 3, .tv_usec = 0};

  /* 3. 调用select函数判断clientfd是否可写，可写则连接成功*/
  if (select(clientfd + 1, NULL, &writeset, NULL, &tv) != 1) {
    fprintf(stderr, "[select] connect to server error\n");
    close(clientfd);
    return -1;
  }

  /* 4. 调用getsockopt检测此时socket是否出错 */
  int err;
  socklen_t len = (socklen_t)sizeof(err);
  if (getsockopt(clientfd, SOL_SOCKET, SO_ERROR, &err, &len) == -1) {
    close(clientfd);
    return -1;
  }

  if (err == 0) {
    printf("connect sucessfully\n");
  } else {
    printf("connect failed\n");
  }

  return 0;
}
