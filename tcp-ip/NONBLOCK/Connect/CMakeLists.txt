cmake_minimum_required(VERSION 3.25)

project(Connect LANGUAGES C)

set(CMAKE_C_STANDARD 17)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_C_EXTENSIONS ON)
# set(CMAKE_BUILD_TYPE "Debug")

add_executable(Connect client_nonblock_connect.c)
add_executable(Server server_epoll.c)
