#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <errno.h>
#include <signal.h>
#include <sys/wait.h>

static void
read_childproc(int sig) {
	int status;
	pid_t id = waitpid(-1, &status, WNOHANG);
	printf("read_childproc\n");
	if (WIFEXITED(status)) {
		printf("remove proc id: %d\n" ,id);
		printf("Child send: %d \n", WEXITSTATUS(status));
	}
}

int main(int argc, char *argv[])
{
	if (argc != 3) {
		fprintf(stderr,"Useage: %s <ip> <port>\n" ,argv[0]);
		exit(EXIT_FAILURE);
	}
	pid_t pid;
	char buf[BUFSIZ] = {0};

	/* signal action */
	struct sigaction act = {
			.sa_handler = read_childproc, /* avoid zombie process */
			.sa_mask      = {0},
	};
	sigaction(SIGCLD, &act, NULL);

	/* server init */
	int serv_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); /* TCP */
	int flag = 1;
	setsockopt(serv_sock, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));
	/* serv addr*/
	struct sockaddr_in serv_addr = {
		.sin_family      = AF_INET,
		.sin_addr.s_addr = inet_addr(argv[1]),
		.sin_port        = htons(atoi(argv[2]))
	};

	int status = bind(serv_sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
	if (status == -1) {
		fprintf(stderr, "bind(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	status = listen(serv_sock, SOMAXCONN);
	if (status == -1) {
		fprintf(stderr, "listen(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	/* clnt addr*/
	int clnt_sock;
	struct sockaddr_in clnt_addr = {0};
	socklen_t clnt_addr_len = sizeof(clnt_addr);
	int str_len;

	while (true) {
		clnt_sock = accept(serv_sock, (struct sockaddr*)&clnt_addr, &clnt_addr_len);
		if (clnt_sock == -1) {
			continue;
		} else {
			printf("new client connected...\n");
		}

		pid = fork();
		if (pid < 0) {
			close(clnt_sock);
			continue;
		}

		if (pid == 0) { /* child */
			close(serv_sock);
			while ((str_len = read(clnt_sock, buf, BUFSIZ)) != 0) {
				write(clnt_sock, buf, str_len);
			}
			close(clnt_sock);
			printf("\nclient disconnectd...\n");
			return 0;
		} else { /* parent */
			close(clnt_sock);
		}
	}

	close(serv_sock);
	return 0;
}
