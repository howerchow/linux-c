#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>

int main(int argc, char *argv[])
{
  if (argc != 2)	{
    printf("Useage: %s <port>\n" ,argv[0]);
    exit(EXIT_FAILURE);
  }

  int serv_sock;
  int clnt_sock;

  struct sockaddr_in serv_addr;
  struct sockaddr_in clnt_addr;
  socklen_t clnt_addr_size;

  ssize_t str_len;

  char buffer[128];
  int status;

  /* int socket(int domain, int type, int protocol); */
  serv_sock = socket(AF_INET, SOCK_STREAM, 0);

  memset(&serv_addr,0,sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(atoi(argv[1]));
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);


  status  = bind(serv_sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
  if (status == -1) {
    fprintf(stderr, "bind(): ", strerror(status));
    exit(EXIT_FAILURE);
  }

  status = listen(serv_sock, 5);
  if (status == -1) {
    fprintf(stderr, "listen(): ", strerror(status));
    exit(EXIT_FAILURE);
  }

  clnt_addr_size = sizeof(clnt_addr);

  fd_set readfds, rfds;
  fd_set writefds, wfds;
  FD_ZERO(&readfds);
  FD_ZERO(&writefds);
  FD_SET(serv_sock, &readfds);
  int max_fd = serv_sock;
  int n;
  for (;;) {
    rfds = readfds;
    wfds = writefds;
    int ready = select(max_fd + 1, &rfds, NULL, NULL, NULL);
    if (FD_ISSET(serv_sock, &rfds)) {
      clnt_sock = accept(serv_sock, (struct sockaddr*)&clnt_addr, &clnt_addr_size);
      if (clnt_sock == -1) {
	printf("accept(): ");
	exit(EXIT_FAILURE);
      }
      else {
	printf("Connected client\n");
      }

      FD_SET(clnt_sock, &readfds);

      if (clnt_sock > max_fd) {
	max_fd = clnt_sock;
      }
    }
    if (--ready == 0 && FD_ISSET(serv_sock, &rfds)) continue;

    for (int i = serv_sock + 1; i <= max_fd; ++i) {
      if (FD_ISSET(i, &rfds)) {
	n = recv(i, buffer, sizeof(buffer), 0);
	if (n == -1) {
	  perror("recv()");
	  exit(EXIT_FAILURE);
	}

	if (n > 0) {
	  buffer[n] = '\0';
	  printf("receive message from client: %s\n", buffer);
	  if (send(i, buffer, n, 0) == -1) {
	    perror("send()");
	    exit(EXIT_FAILURE);
	  }

	} else if (n == 0) {
	  printf("close()\n");
	  FD_CLR(i, &readfds);
	  close(i);
	}

	if (--ready == 0) break;
      }
    }
  }
  close(serv_sock);
  return 0;
}
