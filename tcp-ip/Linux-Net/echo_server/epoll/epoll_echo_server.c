#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <errno.h>

#define POLL_SIZE 1024

int main(int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr,"Useage: %s <port>\n" ,argv[0]);
		exit(EXIT_FAILURE);
	}

	int serv_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); /* TCP */

	struct sockaddr_in serv_addr = {
		.sin_family      = AF_INET,
		.sin_addr.s_addr = htonl(INADDR_ANY),
		.sin_port        = htons(atoi(argv[1]))
	};

	int status = bind(serv_sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
	if (status == -1) {
		fprintf(stderr, "bind(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	status = listen(serv_sock, SOMAXCONN);
	if (status == -1) {
		fprintf(stderr, "listen(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	int epfd = epoll_create(1);

	struct epoll_event ev = {
		.events = EPOLLIN,
		.data.fd = serv_sock
	};
	epoll_ctl(epfd, EPOLL_CTL_ADD, serv_sock, &ev);

	struct epoll_event events[POLL_SIZE] = {0};

	/* client */
	int clnt_sock;
	struct sockaddr_in client_addr = {0};
	socklen_t client_addr_len;
	client_addr_len = sizeof(client_addr);

	char buffer[128] = {0};

	while(true) {

		int ready = epoll_wait(epfd, events, POLL_SIZE, 1000);
		if (ready == -1) { /* error */
			if (errno == EINTR) {
				continue;
			} else {
				break;
			}
		} else if (ready == 0) { /* timeout */
			continue;
		}

		for (size_t i = 0; i < ready ; ++i) {
			if (events[i].data.fd == serv_sock) {
				clnt_sock = accept(serv_sock, (struct sockaddr*)&client_addr, &client_addr_len);
				if (clnt_sock == -1) {
					printf("accept(): ");
					exit(EXIT_FAILURE);
				} else {
					printf("Connected client\n");
				}

				ev.events = EPOLLIN;
				ev.data.fd = clnt_sock;
				if(epoll_ctl(epfd, EPOLL_CTL_ADD, clnt_sock, &ev) == -1) {
					perror("epoll_ctl()");
					exit(EXIT_FAILURE);
				}
			} else if (events[i].events & EPOLLIN) { /* read */
				int n = recv(events[i].data.fd, buffer, sizeof(buffer), 0);
				if (n == -1) {
					perror("recv()");
					exit(EXIT_FAILURE);
				}
				if (n > 0) {
					buffer[n] = '\0';
					printf("receive message from client: %s\n", buffer);
					if (send(events[i].data.fd, buffer, n, 0) == -1) {
						perror("send()");
						exit(EXIT_FAILURE);
					}
				} else if (n == 0) {
				        printf("close()\n");
					ev.events = EPOLLIN;
					ev.data.fd = events[i].data.fd;
					epoll_ctl(epfd, EPOLL_CTL_DEL, events[i].data.fd, &ev);
					close(events[i].data.fd);
				}
			} else if (events[i].events & EPOLLOUT) { /* write */
				//

			} else if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP) ) { /* wrong */
				fprintf(stderr, "epoll error\n");
				close(events[i].data.fd);
				continue;
			}
		}
	}

	close(serv_sock);
	close(clnt_sock);
	return 0;
}
