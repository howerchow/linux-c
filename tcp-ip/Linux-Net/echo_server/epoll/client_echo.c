#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

void error_handling(char *message);

int main(int argc, char *argv[])
{
	if (argc != 3) {
		fprintf(stderr,"Usage : %s <IP> <port>\n" ,argv[0]);
		exit(EXIT_FAILURE);
	}
	int sock;

	char buffer[30] = {0};
	ssize_t str_len;
	ssize_t read_len = 0;
	int idx = 0;
	int status;
	int n;

	sock = socket(AF_INET, SOCK_STREAM, 0);

	struct sockaddr_in serv_addr = {0};
	serv_addr.sin_family = AF_INET;
	inet_pton(AF_INET, argv[1], &serv_addr.sin_addr);
	serv_addr.sin_port = htons(atoi(argv[2]));


	status = connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
	if (status != 0){
		perror("connect():");
		exit(EXIT_FAILURE);
	}

	for (;;) {
		fgets(buffer, sizeof(buffer), stdin);
		n = send(sock, buffer, sizeof(buffer), 0);
		if (n == -1) {
			perror("send(): ");
			exit(EXIT_FAILURE);
		}

		n = recv(sock, buffer, sizeof(buffer), 0);
		if (n > 0) {
			buffer[n] = '\0';
			printf("%s\n", buffer);
		}
	}

	close(sock);
	return 0;
}
