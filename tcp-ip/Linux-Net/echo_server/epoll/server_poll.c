#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <poll.h>

#define POLL_SIZE 1024

int main(int argc, char *argv[])
{
  if (argc != 2)	{
    fprintf(stderr,"Useage: %s <port>\n" ,argv[0]);
    exit(EXIT_FAILURE);
  }
  int serv_sock;
  int clnt_sock;

  struct sockaddr_in clnt_addr = {0};
  socklen_t clnt_addr_size;
  clnt_addr_size = sizeof(clnt_addr);

  ssize_t str_len;
  char buffer[128] = {0};
  int status;


  serv_sock = socket(AF_INET, SOCK_STREAM, 0);

  struct sockaddr_in serv_addr = {0};
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(atoi(argv[1]));
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);


  status  = bind(serv_sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
  if (status == -1) {
    fprintf(stderr, "bind(): %s", strerror(status));
    exit(EXIT_FAILURE);
  }

  status = listen(serv_sock, 5);
  if (status == -1) {
    fprintf(stderr, "listen(): %s", strerror(status));
    exit(EXIT_FAILURE);
  }



  struct pollfd fds[POLL_SIZE] = {0};
  fds[0].fd = serv_sock;
  fds[0].events = POLLIN;
  int n;
  int max_fd = serv_sock + 1;

  for(;;) {
    int ready = poll(fds, max_fd + 1, -1);

    if (fds[0].revents & POLLIN) {
      clnt_sock = accept(serv_sock, (struct sockaddr*)&clnt_addr, &clnt_addr_size);
      if (clnt_sock == -1) {
	printf("accept(): ");
	exit(EXIT_FAILURE);
      }
      else {
	printf("Connected client\n");
      }

      fds[clnt_sock].fd = clnt_sock;
      fds[clnt_sock].events = POLLIN;
    }

    if (clnt_sock > max_fd) {
      max_fd = clnt_sock;
    }

    if (--ready == 0 && (fds[0].revents & POLLIN)) continue;

    for (int i = serv_sock + 1; i <= max_fd; ++i) {
      if (fds[i].revents & POLLIN) {
	n = recv(i, buffer, sizeof(buffer), 0);
	if (n > 0) {
	  buffer[n] = '\0';
	  printf("receive message from client: %s\n", buffer);
	  send(clnt_sock, buffer, n, 0);
	} else if (n == 0) {
	  printf("close()\n");
	  fds[i].fd = -1;
	  close(i);
	}

	if (--ready == 0) break;
      }
      /* else if (FD_ISSET(i, &wfds)) { */
      /* 	send(i, buffer, n, 0); */
      /* 	FD_SET(i, &rfds); */
      /* } */
    }
  }
  close(serv_sock);
  return 0;
}
