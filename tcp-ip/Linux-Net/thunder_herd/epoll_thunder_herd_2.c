/**
 * 在fork之后创建epollfd
 * 每个进程都有一个epollfd
 * 加上EPOLLEXCLUSIVE 测验通过 无惊群 🤗
 * gcc epoll_thunder_herd_2.c -g -levent
 **/

#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<unistd.h>
#include<sys/epoll.h>
#include<netdb.h>
#include<stdlib.h>
#include<fcntl.h>
#include<sys/wait.h>
#include<errno.h>
#include <stdbool.h>
#include <event2/util.h>
#include <string.h>

#define MAXEVENTS 64


int sock_creat_bind(char * port);

int main(int argc, char *argv[])
{
  int serv_sock;


  if(argc < 2) {
    printf("usage: [port] %s\n", argv[1]);
    exit(EXIT_FAILURE);
  }

  if((serv_sock = sock_creat_bind(argv[1])) < 0) {
    perror("socket and bind");
    exit(1);
  }

  evutil_make_socket_nonblocking(serv_sock);

  if(listen(serv_sock, SOMAXCONN) < 0) {
    perror("listen");
    exit(1);
  }

  struct epoll_event *events;
  events = calloc(MAXEVENTS, sizeof(*events));

  /* client */
  int clnt_sock;
  struct sockaddr_in client_addr = {0};
  socklen_t client_addr_len;
  client_addr_len = sizeof(client_addr);
  struct epoll_event ev = {0};
  char buffer[128] = {0};

  /* create 4 worker process*/
  for (size_t j = 0; j < 4; ++j) {
    int pid = fork();
    /* child begin */
    if (pid == 0) {
      int epoll_fd = epoll_create(1);
      struct epoll_event event = {
	//.events = EPOLLIN,
	.events = EPOLLIN | EPOLLEXCLUSIVE,// 🤗
	.data.fd = serv_sock,
      };

      if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, serv_sock, &event) < 0 ) {
	perror("--epoll_ctl--");
	exit(EXIT_FAILURE);
      }

      while (true) {
	int ready = epoll_wait(epoll_fd, events, MAXEVENTS, -1);
	printf("process %d returnt from epoll_wait\n", getpid());
	//sleep(2);

	for (size_t i = 0; i < ready ; ++i) {
	  if (events[i].data.fd == serv_sock) {
	    clnt_sock = accept(serv_sock, (struct sockaddr*)&client_addr, &client_addr_len);
	    if (clnt_sock == -1) {
	      printf("accept(): ");
	      exit(EXIT_FAILURE);
	    } else {
	      printf("Connected client\n");
	    }

	    ev.events = EPOLLIN;
	    ev.data.fd = clnt_sock;
	    if(epoll_ctl(epoll_fd, EPOLL_CTL_ADD, clnt_sock, &ev) == -1) {
	      perror("epoll_ctl()");
	      exit(EXIT_FAILURE);
	    }
	  } else if (events[i].events & EPOLLIN) { /* read */
				int n = recv(events[i].data.fd, buffer, sizeof(buffer), 0);
				if (n < 0) {
				  perror("recv()");
				  exit(EXIT_FAILURE);
				} else if (n == 0) {
				  printf("close()\n");
				  ev.events = EPOLLIN;
				  ev.data.fd = events[i].data.fd;
				  epoll_ctl(epoll_fd, EPOLL_CTL_DEL, events[i].data.fd, &ev);
				  close(events[i].data.fd);
				} else {
				  buffer[n] = '\0';
				  printf("receive message from client: %s\n", buffer);
				  if (send(events[i].data.fd, buffer, n, 0) == -1) {
				    perror("send()");
				    exit(EXIT_FAILURE);
				  }
				}
	  } else if (events[i].events & EPOLLOUT) { /* write */
				//

	  } else if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP) ) { /* error */
				fprintf(stderr, "epoll error\n");
				close(events[i].data.fd);
				continue;
	  }
	}
      }
    }
    /* child end */
  }

  wait(0);
  free(events);
  close(serv_sock);

  return 0;
}



int sock_creat_bind(char * port) {
  int serv_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); /* TCP */

  struct sockaddr_in serv_addr = {
    .sin_family      = AF_INET,
    .sin_addr.s_addr = htonl(INADDR_ANY),
    .sin_port        = htons(atoi(port))
  };

  int status = bind(serv_sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
  if (status == -1) {
    fprintf(stderr, "bind(): %s\n", strerror(status));
    exit(EXIT_FAILURE);
  }

  return serv_sock;
}
