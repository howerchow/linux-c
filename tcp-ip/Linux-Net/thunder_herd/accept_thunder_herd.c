/**
 * accept惊群

 在内核2.6及之后，解决了惊群问题，在内核中增加了一个互斥等待变量。一个互斥等待行为与睡眠基本类似，主要的不同点在于：
1）当一个进程有WQ_FLAG_EXCLUSEVE标志置位，它被添加到等待队列的尾部。若没有这个标志置位，则添加到队列首部。
2）当wake_up被在一个等待队列上调用时，它在唤醒第一个有WQ_FLAG_EXCLUSIVE标志的进程后停止。
对于互斥等待的行为，比如对一个listen后的socket描述符，多线程阻塞accept时，系统内核只会唤醒所有正在等待此时间的队列中的第一个进程/线程，队列中的其他进程/线程则继续等待下一次事件的发生。这样，就避免了多个进程/线程同时监听同一个socket描述符时的惊群问题。

 * 测验未通过

 **/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <string.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdbool.h>

static void read_childproc(int sig);

int main(int argc, char *argv[])
{
  if (argc != 2) {
    fprintf(stderr,"Useage: %s <port>\n" ,argv[0]);
    exit(EXIT_FAILURE);
  }

  int connfd;
  int pid;
  char sendbuff[1024];
  int status;

  /* signal action */
  struct sigaction act = {
    .sa_handler = read_childproc, /* avoid zombie process */
    .sa_mask      = {0},
  };
  sigaction(SIGCLD, &act, NULL);

  int serv_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); /* TCP */

  struct sockaddr_in serv_addr = {
    .sin_family      = AF_INET,
    .sin_addr.s_addr = htonl(INADDR_ANY),
    .sin_port        = htons(atoi(argv[1]))
  };


  if (bind(serv_sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) == -1) {
    fprintf(stderr, "bind(): %s\n", strerror(status));
    exit(EXIT_FAILURE);
  }

  if (listen(serv_sock, SOMAXCONN) == -1) {
    fprintf(stderr, "listen(): %s\n", strerror(status));
    exit(EXIT_FAILURE);
  }

  for(size_t i = 0; i < 4; ++i) {
    pid = fork();
    if (pid < 0) { /* error,continue */
      continue;
    }

    if (pid == 0) { /* child */
      while (true) {
        connfd = accept(serv_sock, (struct sockaddr *)NULL, NULL);
	if (connfd == 0) {
	  snprintf(sendbuff, sizeof(sendbuff), "accept success process PID = %d\n", getpid());
          send(connfd, sendbuff, strlen(sendbuff)+1, 0);
          printf("process %d accept success\n", getpid());
          close(connfd);
	} else {
	  printf("process %d accept a connection failed: %s\n", getpid(), strerror(errno));
          close(connfd);
	}
      }
    }

  }

  wait(0);
  return 0;
}

static void
read_childproc(int sig) {
  int status;
  pid_t id = waitpid(-1, &status, WNOHANG);
  printf("read_childproc\n");
  if (WIFEXITED(status)) {
    printf("remove proc id: %d\n" ,id);
    printf("Child send: %d \n", WEXITSTATUS(status));
  }
}
