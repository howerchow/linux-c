/**
 * 在fork之后创建epollfd
 * 每个进程都有一个epollfd
 * 最优👍 加上resuserport 测验通过 无惊群 🤗
 * gcc epoll_thunder_herd_2.c -g -levent
 **/

#include <stddef.h>
#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<unistd.h>
#include<sys/epoll.h>
#include<netdb.h>
#include<stdlib.h>
#include<fcntl.h>
#include<sys/wait.h>
#include<errno.h>
#include <stdbool.h>
#include <event2/util.h>
#include <string.h>

#define MAXEVENTS 64

int main(int argc, char *argv[])
{
  if(argc < 2) {
    printf("usage: [port] %s\n", argv[1]);
    exit(EXIT_FAILURE);
  }

  int serv_sock[4] = {0};
  for(size_t i = 0; i < 4; ++i) {
    serv_sock[i] = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); /* TCP */
    if (serv_sock[i] == -1) {
      fprintf(stderr, "sock():\n");
      exit(EXIT_FAILURE);
    }
    evutil_make_listen_socket_reuseable(serv_sock[i]);
    evutil_make_listen_socket_reuseable_port(serv_sock[i]);
    evutil_make_socket_nonblocking(serv_sock[i]);

    struct sockaddr_in serv_addr = {
      .sin_family      = AF_INET,
      .sin_addr.s_addr = htonl(INADDR_ANY),
      .sin_port        = htons(atoi(argv[1]))
    };

    int status = bind(serv_sock[i], (struct sockaddr*)&serv_addr, sizeof(serv_addr));
    if (status == -1) {
      fprintf(stderr, "bind(): %s\n", strerror(status));
      exit(EXIT_FAILURE);
    }

    if(listen(serv_sock[i], SOMAXCONN) < 0) {
      perror("listen");
      exit(EXIT_FAILURE);
    }
  }


  /* create 4 worker process*/
  for (size_t j = 0; j < 4; ++j) {

    /* client */
    int clnt_sock;
    struct sockaddr_in client_addr = {0};
    socklen_t client_addr_len;
    client_addr_len = sizeof(client_addr);
    struct epoll_event ev = {0};
    char buffer[128] = {0};

    int pid = fork();
    struct epoll_event *events;
    events = calloc(MAXEVENTS, sizeof(*events));

    /* child begin */
    if (pid == 0) {

      int epoll_fd = epoll_create(1);
      struct epoll_event event = {
	.events = EPOLLIN,
	.data.fd = serv_sock[j],
      };

      if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, serv_sock[j], &event) < 0 ) {
	exit(EXIT_FAILURE);
      }

      while (true) {
	int ready = epoll_wait(epoll_fd, events, MAXEVENTS, -1);
	printf("process %d returnt from epoll_wait\n", getpid());

	for (size_t i = 0; i < ready ; ++i) {
	  if (events[i].data.fd == serv_sock[j]) {
	    clnt_sock = accept(serv_sock[j], (struct sockaddr*)&client_addr, &client_addr_len);
	    if (clnt_sock == -1) {
	      printf("accept(): ");
	      exit(EXIT_FAILURE);
	    } else {
	      printf("Connected client\n");
	    }

	    ev.events = EPOLLIN;
	    ev.data.fd = clnt_sock;
	    if(epoll_ctl(epoll_fd, EPOLL_CTL_ADD, clnt_sock, &ev) == -1) {
	      perror("epoll_ctl()");
	      exit(EXIT_FAILURE);
	    }
	  } else if (events[i].events & EPOLLIN) { /* read */
				int n = recv(events[i].data.fd, buffer, sizeof(buffer), 0);
				if (n < 0) {
				  perror("recv()");
				  exit(EXIT_FAILURE);
				} else if (n == 0) {
				  printf("close()\n");
				  ev.events = EPOLLIN;
				  ev.data.fd = events[i].data.fd;
				  epoll_ctl(epoll_fd, EPOLL_CTL_DEL, events[i].data.fd, &ev);
				  close(events[i].data.fd);
				} else {
				  buffer[n] = '\0';
				  printf("receive message from client: %s\n", buffer);
				  if (send(events[i].data.fd, buffer, n, 0) == -1) {
				    perror("send()");
				    exit(EXIT_FAILURE);
				  }
				}
	  } else if (events[i].events & EPOLLOUT) { /* write */
				//

	  } else if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP) ) { /* error */
				fprintf(stderr, "epoll error\n");
				close(events[i].data.fd);
				continue;
	  }
	}
      }
    }
    /* child end */
  }

  wait(0);
  free(events);
  for(size_t k = 0; k < 4; ++k) {
    close(serv_sock[k]);
  }

  return 0;
}
