#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <assert.h>
#include <stdbool.h>
#include <libgen.h>
#include <sys/socket.h>
#include <arpa/inet.h> /* strcut sockaddr_in */
#include <sys/sendfile.h>
#include <fcntl.h>
#include <sys/stat.h>

static bool stop = false;

static void handle_term(int sig)
{
	stop = true;
}


/* WIP */
int main(int argc, char *argv[])
{
	signal(SIGTERM, handle_term);
	if (argc < 3) {
		fprintf(stderr,"Useage: %s <ip_address> <port_number> <backlog> <file_name>\n",basename(argv[0]));
		exit(EXIT_FAILURE);
	}

	int filefd = open(argv[4], O_WRONLY);
	assert(filefd > 0);
	struct stat stat_buf = {0};
	fstat(filefd, &stat_buf);

	struct sockaddr_in servaddr = {
		.sin_family       = AF_INET,
		.sin_port         = htons(atoi(argv[2]))
	};
	inet_pton(AF_INET, argv[1], &servaddr.sin_addr.s_addr);

	int servfd = socket(AF_INET, SOCK_STREAM, 0);

	if (bind(servfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1) {
		perror("bind");
		exit(EXIT_FAILURE);
	}

	if(listen(servfd, atoi(argv[3])) == -1 ) {
		perror("listen");
		exit(EXIT_FAILURE);
	}

	struct sockaddr_in cliaddr = {0};
	socklen_t clilen = sizeof(cliaddr);
	int connfd;
	if ( (connfd = accept(servfd, (struct sockaddr*)&cliaddr, &clilen)) == -1 ) {
		perror("accept");
		exit(EXIT_FAILURE);
	}

	sendfile(connfd, filefd, NULL, stat_buf.st_size);

	close(servfd);
	close(connfd);
	close(filefd);

	return 0;
}
