#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

void error_handling(char *message);

int main(int argc, char *argv[])
{
	if (argc != 3) {
		fprintf(stderr,"Usage : %s <IP> <port>\n" ,argv[0]);
		exit(EXIT_FAILURE);
	}

	int clifd = socket(AF_INET, SOCK_STREAM, 0);

	struct sockaddr_in serv_addr = {
		.sin_family      = AF_INET,
		.sin_addr.s_addr = inet_addr(argv[1]),
		.sin_port        = htons(atoi(argv[2]))
	};

	int status = connect(clifd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
	if (status != 0){
		perror("connect():");
		exit(EXIT_FAILURE);
	}
	const char *normal_data = "123";
	const char *oob_data    = "abc";

	send(clifd, normal_data, strlen(normal_data), 0);/* 123 */
	send(clifd, oob_data, strlen(oob_data), MSG_OOB);/* abc */
	send(clifd, normal_data, strlen(normal_data), 0);/* 123 */

	close(clifd);
	return 0;
}
