#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <assert.h>
#include <stdbool.h>
#include <libgen.h>
#include <sys/socket.h>
#include <arpa/inet.h> /* strcut sockaddr_in */

static bool stop = false;

static void handle_term(int sig)
{
	stop = true;
}

int main(int argc, char *argv[])
{
	signal(SIGTERM, handle_term);
	if (argc < 3) {
		fprintf(stderr,"Useage: %s <ip_address> <port_number> <backlog>\n",basename(argv[0]));
		exit(EXIT_FAILURE);
	}

	struct sockaddr_in servaddr = {
		.sin_family       = AF_INET,
		.sin_port         = htons(atoi(argv[2]))
	};
	inet_pton(AF_INET, argv[1], &servaddr.sin_addr.s_addr);

	int servfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (bind(servfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1) {
		perror("bind");
		exit(EXIT_FAILURE);
	}

	if(listen(servfd, atoi(argv[3])) == -1 ) {
		perror("listen");
		exit(EXIT_FAILURE);
	}

	struct sockaddr_in cliaddr = {0};
	socklen_t clilen = sizeof(cliaddr);
	int connfd;
	if ( (connfd = accept(servfd, (struct sockaddr*)&cliaddr, &clilen)) == -1 ) {
		perror("accept");
		exit(EXIT_FAILURE);
	}

	char buffer[BUFSIZ] = {0};
	int ret = recv(connfd, buffer, sizeof(buffer), 0);/* 123 */
	printf("got %d bytes of normal data '%s'\n",ret,buffer);

	memset(buffer,'\0',sizeof(buffer));
	ret = recv(connfd, buffer, sizeof(buffer), MSG_OOB);/* c */
	printf("got %d bytes of oob data '%s'\n",ret,buffer);

	memset(buffer,'\0',sizeof(buffer));
	ret = recv(connfd, buffer, sizeof(buffer), 0);/* ab */
	printf("got %d bytes of normal data '%s'\n",ret,buffer);

	close(servfd);
	close(connfd);

	return 0;
}
