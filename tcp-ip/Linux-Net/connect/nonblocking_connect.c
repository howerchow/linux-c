/* 异步/非阻塞connect*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <event2/util.h>
#include <errno.h>
#include <sys/socket.h>
#include <unistd.h>
#include <poll.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

int connect_wait(int fd, int mask, long long milliseconds);
int get_sock_error(int fd);

int main(int argc, char *argv[])
{
  if (argc != 3) {
    fprintf(stderr,"Usage : %s <IP> <port>\n" ,argv[0]);
    exit(EXIT_FAILURE);
  }
  int clnt_sock;

  char buffer[30] = {0};
  ssize_t str_len;
  ssize_t read_len = 0;
  int idx = 0;

  int n;

  clnt_sock = socket(AF_INET, SOCK_STREAM, 0);
  if(evutil_make_socket_nonblocking(clnt_sock) == -1) {
    fprintf(stderr, "set socket to nonblock error\n");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_in serv_addr = {
    .sin_family = AF_INET,
    .sin_addr.s_addr = inet_addr(argv[1]),
    .sin_port = htons(atoi(argv[2])),
  };

  int status;
  while (true) {
    status = connect(clnt_sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
    if (status == 0) {
      fprintf(stdout, "connect to server successfuly\n");
      close(clnt_sock);
      exit(EXIT_SUCCESS);
    } else if (status == -1) {
      if(errno == EINTR) {
	printf("interupted by signal,continue...\n");
	continue;
      } else if (errno == EINPROGRESS) {
	break;
      } else {
	close(clnt_sock);
	exit(EXIT_FAILURE);
      }
    }
  }

  if( connect_wait(clnt_sock, POLLOUT, 3000) & POLLOUT ) {
    printf("[poll] connect success\n");
    exit(EXIT_SUCCESS);
  } else {
    printf("[poll] connect failed\n");
    exit(EXIT_FAILURE);
  }

  /* linux上即使连接失败 socket也是可写 所以判断下*/
  int e;
  socklen_t elen = sizeof(e);
  if (getsockopt(clnt_sock, SOL_SOCKET, SO_ERROR, &e, &elen) < 0) {
    close(clnt_sock);
    exit(EXIT_FAILURE);
  }

  if (e == 0) {
    printf("connect successfuly\n");
  } else {
    printf("connect failed\n");
  }

  /* for (;;) { */
  /* 	fgets(buffer, sizeof(buffer), stdin); */
  /* 	n = send(clnt_sock, buffer, sizeof(buffer), 0); */
  /* 	if (n == -1) { */
  /* 		perror("send(): "); */
  /* 		exit(EXIT_FAILURE); */
  /* 	} */

  /* 	n = recv(clnt_sock, buffer, sizeof(buffer), 0); */
  /* 	if (n > 0) { */
  /* 		buffer[n] = '\0'; */
  /* 		printf("%s\n", buffer); */
  /* 	} */
  /* } */

  close(clnt_sock);
  return 0;
}

/* Wait for milliseconds until the given file descriptor becomes
 * writable/readable/exception */
int connect_wait(int fd, int mask, long long milliseconds) {
  struct pollfd pfd;
  int retmask = 0, retval;

  memset(&pfd, 0, sizeof(pfd));
  pfd.fd = fd;
  if (mask & POLLIN) pfd.events |= POLLIN;
  if (mask & POLLOUT) pfd.events |= POLLOUT;

  if ((retval = poll(&pfd, 1, milliseconds))== 1) {
    if (pfd.revents & POLLIN) retmask |= POLLIN;
    if (pfd.revents & POLLOUT) retmask |= POLLOUT;
    return retmask;
  } else {
    return retval;
  }
}

int get_sock_error(int fd) {
  int sockerr = 0;
  socklen_t errlen = sizeof(sockerr);

  if (getsockopt(fd, SOL_SOCKET, SO_ERROR, &sockerr, &errlen) == -1)
    sockerr = errno;
  return sockerr;
}
