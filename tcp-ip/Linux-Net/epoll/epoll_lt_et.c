#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <stdbool.h>
#include <errno.h>
#include <error.h>
#include <unistd.h>
#include <libgen.h>
#include <assert.h>
#include <asm-generic/errno.h>

#define MAX_EVENT_NUMBER 1024
#define BUFFER_SIZE 10

/* 将文件描述符设置成非阻塞的 */
int setnonblocking(int fd) {
	int old_option = fcntl(fd, F_GETFL);
	int new_option = old_option | O_NONBLOCK;
	fcntl(fd, F_SETFL,new_option);
	return old_option;
}

void addfd(int epollfd,int fd,bool enable_et) {
	struct epoll_event event = {
		.events  = EPOLLIN,
		.data.fd = fd,
	};

	if (enable_et) {
		event.events |= EPOLLET;
	}

	epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &event);
	setnonblocking(fd);
}

void lt(struct epoll_event* events,int number,int epollfd,int listenfd) {
	char buf[BUFFER_SIZE] = {0};
	for (size_t i = 0; i < number; ++i) {
		if (events[i].data.fd == listenfd) {
			struct sockaddr_in client_address = {0};
			socklen_t client_addrlength = sizeof(client_address);
			int connfd = accept(listenfd, (struct sockaddr *)&client_address, &client_addrlength);
			addfd(epollfd, connfd, false);/* 对connfd禁用ET模式 */
		} else if (events[i].events & EPOLLIN) {
			/* 只要socket读缓存中还有未读出的数据，这段代码就被触发 */
			printf("event trigger once\n");
			memset(buf, '\0', BUFFER_SIZE);
			int ret = recv(events[i].data.fd, buf, BUFFER_SIZE - 1, 0);
			if (ret <= 0) {
				close(events[i].data.fd);
				continue;
			}
			printf("get %d bytes of content: %s\n",ret,buf);
		} else {
			printf("something else happened\n");
		}

	}
}

void et(struct epoll_event* events,int number,int epollfd,int listenfd) {
	char buf[BUFFER_SIZE] = {0};
	for (size_t i = 0; i < number; ++i) {
		if (events[i].data.fd == listenfd) {
			struct sockaddr_in client_address = {0};
			socklen_t client_addrlength = sizeof(client_address);
			int connfd = accept(listenfd, (struct sockaddr *)&client_address, &client_addrlength);
			addfd(epollfd, connfd, true);/* 对connfd启用ET模式 */
		} else if(events[i].events & EPOLLIN) {
			printf("event trigger once\n");
			while (true) {
				memset(buf, '\0', BUFFER_SIZE);
				int ret = recv(events[i].data.fd, buf, BUFFER_SIZE - 1, 0);
				if (ret < 0) {
					if (errno == EAGAIN || errno == EWOULDBLOCK ) {
						printf("read later\n");
						break;
					}
					close(events[i].data.fd);
					break;
				} else if (ret == 0) {
					close(events[i].data.fd);
				} else {
					printf("get %d bytes of content:%s\n",ret,buf);
				}
			}
		} else {
			printf("something else happened \n");
		}
	}
}

int main(int argc, char *argv[])
{
	if (argc < 3) {
		fprintf(stderr,"Useage: %s <ip_address> <port_number> <backlog>\n",basename(argv[0]));
		exit(EXIT_FAILURE);
	}

	struct sockaddr_in servaddr = {
		.sin_family       = AF_INET,
		.sin_port         = htons(atoi(argv[2]))
	};
	inet_pton(AF_INET, argv[1], &servaddr.sin_addr.s_addr);

	int listenfd = socket(AF_INET, SOCK_STREAM, 0);

	if (bind(listenfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1) {
		perror("bind");
		exit(EXIT_FAILURE);
	}

	if(listen(listenfd, atoi(argv[3])) == -1 ) {
		perror("listen");
		exit(EXIT_FAILURE);
	}

	struct epoll_event events[MAX_EVENT_NUMBER] = {0};
	int epollfd = epoll_create(5);
	assert(epollfd != -1);
	addfd(epollfd, listenfd, true);

	while (true) {
		int ret = epoll_wait(epollfd, events, MAX_EVENT_NUMBER, -1);
		if (ret < 0) {
			perror("epoll failure");
			break;
		}
		//lt(events,ret,epollfd,listenfd);
		et(events,ret,epollfd,listenfd);
	}
	close(listenfd);
	return 0;
}
