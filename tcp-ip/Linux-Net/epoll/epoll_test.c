#include <asm-generic/errno-base.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <errno.h>
#include <fcntl.h>

#define POLL_SIZE 1024

int setnonblockingmode(int fd)
{
	int flags;
	if ((flags = fcntl(fd, F_GETFL, NULL)) < 0) {
		fprintf(stderr,"fcntl(%d, F_GETFL)", fd);
		return -1;
	}
	if (!(flags & O_NONBLOCK)) {
		if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1) {
			fprintf(stderr,"fcntl(%d, F_SETFL)", fd);
			return -1;
		}
	}
	return 0;
}

int main(int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr,"Useage: %s <port>\n" ,argv[0]);
		exit(EXIT_FAILURE);
	}

	int serv_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); /* TCP */

	struct sockaddr_in serv_addr = {
		.sin_family      = AF_INET,
		.sin_addr.s_addr = htonl(INADDR_ANY),
		.sin_port        = htons(atoi(argv[1]))
	};

	int status = bind(serv_socket, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
	if (status == -1) {
		fprintf(stderr, "bind(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	status = listen(serv_socket, SOMAXCONN);
	if (status == -1) {
		fprintf(stderr, "listen(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	int epfd = epoll_create(1);

	setnonblockingmode(serv_socket);
	struct epoll_event ev = {
		.events = EPOLLIN,
		.data.fd = serv_socket
	};
	epoll_ctl(epfd, EPOLL_CTL_ADD, serv_socket, &ev);

	struct epoll_event events[POLL_SIZE] = {0};

	/* client */
	int clnt_sock;
	struct sockaddr_in client_addr = {0};
	socklen_t client_addr_size;
	client_addr_size = sizeof(client_addr);

	char buffer[4] = {0};

	while(true) {
		int ready = epoll_wait(epfd, events, POLL_SIZE, 1000);
		if (ready == -1) {
			continue;
		} else if (ready == 0) { /* timeout */
			continue;
		}
		printf("return epoll_wait\n");

		for (size_t i = 0; i < ready ; ++i) {
			if (events[i].data.fd == serv_socket) {
				clnt_sock = accept(serv_socket, (struct sockaddr*)&client_addr, &client_addr_size);
				if (clnt_sock == -1) {
					printf("accept(): ");
					exit(EXIT_FAILURE);
				} else {
					printf("Connected client\n");
				}
				setnonblockingmode(clnt_sock);
				ev.events = EPOLLIN | EPOLLET; // EPOLLET NONBLOCK
				ev.data.fd = clnt_sock;
				if(epoll_ctl(epfd, EPOLL_CTL_ADD, clnt_sock, &ev) == -1) {
					perror("epoll_ctl()");
					exit(EXIT_FAILURE);
				}
			} else if (events[i].events & EPOLLIN) { /* read */
				while(true) {
					int n = recv(events[i].data.fd, buffer, sizeof(buffer), 0);
					/* if (n == -1) { */
					/* 	perror("recv()"); */
					/* 	exit(EXIT_FAILURE); */
					/* } */
					if (n < 0) {
						if (errno == EAGAIN) {
							break;
						}
					}
					if (n == 0) { /* close request */
						printf("close()\n");
						epoll_ctl(epfd, EPOLL_CTL_DEL, events[i].data.fd, NULL);
						close(events[i].data.fd);
					} else { /* write back */
						buffer[n] = '\0';
						if (send(events[i].data.fd, buffer, n, 0) == -1) {
							perror("send()");
							exit(EXIT_FAILURE);
						}
					}
				}
			} else if (events[i].events & EPOLLOUT) { /* write */
				//

			} else if (events[i].events & EPOLLERR) { /* wrong */
				//

			}
		}
	}

	close(serv_socket);
	close(clnt_sock);
	return 0;
}
