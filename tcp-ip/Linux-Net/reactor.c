#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/epoll.h>

#define MAX_EVENTS 1024

#define BUFFER_SIZE 4096
struct item
{
	int fd;
	int events;
	//int (*callback)(int fd, int events, void *arg);
	int (*readcb)(int fd, int events, void *arg);
	int (*writecb)(int fd, int events, void *arg);

	unsigned char sbuffer[BUFFER_SIZE];
	int slength;
	char fbuffer[BUFFER_SIZE];
	unsigned int rlength;

};

struct reactor {
	int epollfd;
	struct item *items;
};

int init_server(char *port) {
	int listen_sock;
	int status;
	struct sockaddr_in listen_addr = {
		.sin_family      = AF_INET,
		.sin_addr.s_addr = htonl(INADDR_ANY),
		.sin_port        = htons(atoi(port)),
	};

	listen_sock = socket(AF_INET, SOCK_STREAM, 0);

	status  = bind(listen_sock, (struct sockaddr*)&listen_addr, sizeof(listen_addr));
	if (status == -1) {
		fprintf(stderr, "bind(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	status = listen(listen_sock, 5);
	if (status == -1) {
		fprintf(stderr, "listen(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}
	return listen_sock;
}

int read_callback(int fd, int events, void *arg) {
	epoll_ctl(fd, EPOLL_CTL_MOD, events, ev);
}

int main(int argc, char *argv[])
{

	int conn_sock;
	struct sockaddr_in conn_addr;
	socklen_t conn_addr_size;
	conn_addr_size = sizeof(conn_addr);

	char buffer[128];
	int n;

	int listen_sock = init_server(argv[1]);

	int epfd = epoll_create(1);

	struct epoll_event events[MAX_EVENTS] = {0};
	struct epoll_event ev = {
		.events  = EPOLLIN,
		.data.fd = listen_sock,
	};

	epoll_ctl(epfd, EPOLL_CTL_ADD, listen_sock, &ev);

	for(;;) {
		int ready = epoll_wait(epfd, events, MAX_EVENTS, 5);
		if (ready == -1) {
			continue;
		}
		for (int i = 0; i < ready ; ++i) {
			if (events[i].data.fd == listen_sock) {
				int clnt_sock = accept(listen_sock, (struct sockaddr*)&clnt_addr, &clnt_addr_size);
				if (clnt_sock == -1) {
					printf("accept(): ");
					exit(EXIT_FAILURE);
				}
				else {
					printf("Connected client\n");
				}

				ev.events = EPOLLIN;
				ev.data.fd = clnt_sock;
				if(epoll_ctl(epfd, EPOLL_CTL_ADD, clnt_sock, &ev) == -1) {
					perror("epoll_ctl()");
					exit(EXIT_FAILURE);
				}
			} else if (events[i].events & EPOLLIN) {
				n = recv(events[i].data.fd, buffer, sizeof(buffer), 0);
				if (n == -1) {
					perror("recv()");
					exit(EXIT_FAILURE);
				}
				if (n > 0) {
					buffer[n] = '\0';
					printf("receive message from client: %s\n", buffer);
					if (send(events[i].data.fd, buffer, n, 0) == -1) {
						perror("send()");
						exit(EXIT_FAILURE);
					}
				} else if (n == 0) {
					printf("close()\n");
					ev.events = EPOLLIN;
					ev.data.fd = events[i].data.fd;
					epoll_ctl(epfd, EPOLL_CTL_DEL, events[i].data.fd, &ev);
					close(events[i].data.fd);
				}
			}
		}
	}


	close(listen_sock);
	return 0;
}
