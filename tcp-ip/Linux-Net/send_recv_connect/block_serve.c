#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/epoll.h>
#include <netinet/in.h>

int main(int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr,"Useage: %s <port>\n" ,argv[0]);
		exit(EXIT_FAILURE);
	}

	int listen_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	struct sockaddr_in serv_addr = {
		.sin_family      = AF_INET,
		.sin_addr.s_addr = htonl(INADDR_ANY),
		.sin_port        = htons(atoi(argv[1]))
	};

	int status = bind(listen_sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
	if (status == -1) {
		fprintf(stderr, "bind(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	status = listen(listen_sock, SOMAXCONN);
	if (status == -1) {
		fprintf(stderr, "listen(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	int conn_sock;
	struct sockaddr_in client_addr = {0};
	socklen_t client_addr_size = sizeof(client_addr);

	char buf[1024];
	while (true) {
		conn_sock = accept(listen_sock, (struct sockaddr*)&client_addr, &client_addr_size);
		if (conn_sock != -1) {
			printf("accept a client connection.\n");
		}

		while(true) {
			int ret = recv(conn_sock, buf, sizeof(buf), 0);
			if (ret == 0 || ret == -1) {
				close(conn_sock);
				close(listen_sock);
				exit(EXIT_FAILURE);
			}
			break;
		}

	}
	close(conn_sock);
	close(listen_sock);
	return 0;
}
