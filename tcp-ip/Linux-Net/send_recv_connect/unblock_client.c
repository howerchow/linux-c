#include <asm-generic/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/poll.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <event2/util.h>
#include <poll.h>

/*在一个循环中发送buf_len长度的数据*/
bool send_data(int sock_fd,const char *buf, int buf_len);

int main(int argc, char *argv[])
{
	if (argc != 3) {
		fprintf(stderr,"Usage : %s <IP> <port>\n" ,argv[0]);
		exit(EXIT_FAILURE);
	}

	int client_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	// 客户端 socket设置为非阻塞模式
	evutil_make_socket_nonblocking(client_sock);

	struct sockaddr_in client_addr = {
		.sin_family      = AF_INET,
		.sin_addr.s_addr = inet_addr(argv[1]),
		.sin_port        = htons(atoi(argv[2]))
	};
	//非阻塞connect
	int status = 0;
	while (true) {
		status = connect(client_sock, (struct sockaddr*)&client_addr, sizeof(client_addr));
		if (status == -1) {
			if (errno == EINPROGRESS) {
				break; /* 尝试连接中 */
			} else if (errno == EINTR) { /* signal */
				continue;
			} else { /* error */
				close(client_sock);
				return -1;
			}
		} else {
			fprintf(stdout, "connect to server successfully!\n");
			break;
		}
	}

	if (status == -1) {
		struct pollfd pfd = {
			.fd     = client_sock,
			.events = POLLOUT
		};
		int err = 0;
		socklen_t err_len = sizeof(err);
		if(poll(&pfd, 1, 4) == 1) {
			if (getsockopt(pfd.fd, SOL_SOCKET, SO_ERROR, &err,&err_len) == -1) {
				close(pfd.fd);
				exit(EXIT_FAILURE);
			}  else {
				if(err != 0) {
					fprintf(stdout, "[poll][getsockopt] failed connect to server\n");
					close(pfd.fd);
					exit(EXIT_FAILURE);
				} else {
					fprintf(stdout, "[poll] connect to server successfully!\n");
				}

			}
		} else {
			fprintf(stdout, "[poll] failed connect to server\n");
			close(client_sock);
			exit(EXIT_FAILURE);
		}
	}

	int count = 0;
	char send_buf[] = "hello world";
	char recev_buf[1024] = {0};

	while (true) {
		/*
		 * 非阻塞模式下的send / recev
		 * == -1: EWOULDBLOCK EINTR ERROR
		 * == 0: 对端关闭
		 * >  0: send成功 / recev 成功
		 */

		/*
		  int ret = send(client_sock, send_buf, strlen(send_buf),0);
		  if (ret == -1) {
		  if (errno == EWOULDBLOCK) {
		  printf("send data error as tcp window size is too small\n");
		  continue;
		  } else if (errno == EINTR) {
		  printf("sending data interrupted by signal\n");
		  continue;
		  } else {
		  printf("send data error\n");
		  break;
		  }
		  } else if (ret == 0) {
		  printf("send data error\n");
		  close(client_sock);
		  } else {
		  ++count;
		  printf("send data successfully,cout = %d\n",count);
		  }
		*/

		/*

		  int ret = recv(client_sock, recev_buf, sizeof(recev_buf), 0);
		  if (ret == -1) {
		  if (errno == EWOULDBLOCK || errno == EAGAIN) {
		  fprintf(stdout, "There is no data available now.\n");
		  continue;
		  } else if (errno == EINTR) {
		  printf("recev data interrupted by signal\n");
		  continue;
		  } else {
		  break;
		  }
		  } else if (ret == 0) {
		  fprintf(stdout, "peer close the socket.\n");
		  close(client_sock);
		  } else {
		  fprintf(stdout, "recev %d bytes.\n",strlen(recev_buf));
		  }
		*/

		if (!send_data(client_sock, send_buf, sizeof(send_buf))) {
			fprintf(stdout, "send data error\n");
			exit(EXIT_FAILURE);
		}
	}
	close(client_sock);
}

bool send_data(int sock_fd,const char *send_buf, int buf_len) {
	int sent_bytes = 0;
	while (true) {
		int ret = send(sock_fd, send_buf + sent_bytes, buf_len-sent_bytes, 0);
		if (ret == -1) {
			if (errno == EWOULDBLOCK || errno == EAGAIN) {
				//正确的做法：缓存尚未发出的数据
				//break;
				continue;
			} else if (errno == EINTR) {
				printf("sending data interrupted by signal\n");
				continue;
			} else {
				fprintf(stdout, "send data error\n");
				return false;
			}
		} else if (ret == 0) {
			printf("send data error\n");
			//close(sock_fd);
			return false;
		} else {
			sent_bytes += ret;
			if (sent_bytes == buf_len) {
				break;
			}
		}
	}
	return true;

}
