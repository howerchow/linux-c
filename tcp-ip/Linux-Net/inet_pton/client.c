#include <netinet/in.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <ctype.h>

#define BUF_SIZE 10

int main(int argc, char *argv[])
{
	int cfd;
	char resp[BUF_SIZE] = {0};
	if (argc < 3 || strcmp(argv[1], "--help") == 0) {
		fprintf(stdout, "%s host-address msg...\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	cfd = socket(AF_INET6, SOCK_DGRAM, 0);
	if (cfd == -1) {
		perror("socket");
		exit(EXIT_FAILURE);
	}
	struct sockaddr_in6 svaddr = {
		.sin6_family = AF_INET6,
		.sin6_port = htons(49152)
	};
	inet_pton(AF_INET6, argv[1], &svaddr.sin6_addr);

	size_t mesLen;
	ssize_t numBytes;
	for (int j = 2 ; j < argc ; ++j) {
		mesLen = strlen(argv[j]);
		sendto(cfd, argv[j], mesLen, 0, (struct sockaddr *)&svaddr, sizeof(struct sockaddr_in6));
		numBytes = recvfrom(cfd, resp, BUF_SIZE, 0, NULL, NULL);
		printf("Response %d: %.*s\n", j-1, (int)numBytes, resp);
	}
	return 0;
}
