#include <netinet/in.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <ctype.h>

#define BUF_SIZE 10

int main(int argc, char *argv[])
{

	int listen_fd;
	char buf[BUF_SIZE];
	char claddrStr[INET6_ADDRSTRLEN];

	listen_fd =socket(AF_INET6, SOCK_DGRAM, 0);
	if (listen_fd == -1) {
		perror("socket");
		exit(EXIT_FAILURE);
	}

	struct sockaddr_in6 svaddr = {
		.sin6_family = AF_INET6,
		.sin6_addr   = in6addr_any, /*  Wildcard address */
		.sin6_port   = htons(49152)
	};

	if (bind(listen_fd, (struct sockaddr*)&svaddr, sizeof(struct sockaddr_in6)) == -1) {
		perror("socket");
		exit(EXIT_FAILURE);
	}

	ssize_t numByte;
	struct sockaddr_in6 claddr = {0};
	socklen_t len = sizeof(struct sockaddr_in6);

	while (true) {
		numByte = recvfrom(listen_fd, buf, BUF_SIZE, 0, (struct sockaddr *)&claddr, &len);
		if (numByte == -1) {
			perror("recvfrom");
			exit(EXIT_FAILURE);
		}

		if (inet_ntop(AF_INET6, &claddr.sin6_addr, claddrStr, INET6_ADDRSTRLEN) == NULL) {
			perror("inet_ntop");
			exit(EXIT_FAILURE);
		} else {
			printf("server receive %ld bytes, from (%s, %u)\n" , (long)numByte, claddr, ntohs(claddr.sin6_port));
		}

		for (size_t j = 0; j < numByte; ++j) {
			buf[j] = toupper((unsigned char)buf[j]);
		}

		if (sendto(listen_fd, buf, numByte, 0, (struct sockaddr*)&claddr, len) != numByte) {
			printf("sendto error");
			exit(EXIT_FAILURE);
		}
	}

	return 0;
}
