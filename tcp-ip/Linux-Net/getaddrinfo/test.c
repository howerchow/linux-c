#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>

int main(int argc, char *argv[])
{
  if (argc != 2) {
    fprintf(stderr, "Usage: %s hostname\n",argv[0]);
    exit(EXIT_FAILURE);
  }

  struct addrinfo *servinfo, *p;
  struct addrinfo hints = {
	  .ai_family = AF_INET,
	  .ai_socktype = SOCK_DGRAM,
  };

  char ipstr[16];

  int ret = getaddrinfo(argv[1], NULL, &hints, &servinfo);
  if (ret != 0) {
    fprintf(stderr, "getaddrinfo: %s\n",gai_strerror(ret));
    exit(EXIT_FAILURE);
  }

  for (p = servinfo; p != NULL; p = p->ai_next) {
    printf("%d\n",p->ai_addr->sa_data);
  }

  freeaddrinfo(servinfo);
  return 0;
}
