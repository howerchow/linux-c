#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/epoll.h>

#define POLL_SIZE 1024
#define BUFFER_SIZE 1024
#define MAX_EVENT 1024

//typedef int (*CALLBACK)(int fd, int event, void *arg);

struct item
{
	int fd;
	int events;
	void *arg;
	
	int (*readcb)(int fd, int event, void *arg);
	int (*writecb)(int fd, int event, void *arg);
	int (*acceptcb)(int fd, int event, void *arg);

	unsigned char send_buffer[BUFFER_SIZE];
	int send_length;
	
	unsigned char recv_buffer[BUFFER_SIZE];
	int recv_length;
};


struct itemblock
{
	struct item *items;
	struct itemblock *next;
	
};
struct reactor
{
	int epfd;
	struct itemblock *head;
	//struct itemblock **tail;
};

int init_reactor(struct reactor *ractor);

struct reactor *instance = NULL;

struct reactor *get_instance(void) { //singleton
	if (instance == NULL) {
		instance = (struct reactor*) malloc(sizeof(struct reactor));
	}
	if (instance == NULL) {
		return NULL;
	}
	memset(instance, 0, sizeof(struct reactor));

	if (init_reactor(instance) == -1) {
		free(instance);
		return NULL;
	}

	return instance;
}

int read_callback(int fd, int event, void *arg) {
	
}

int write_callback(int fd, int event, void *arg) {
	
}

int accept_callback(int fd, int event, void *arg) {

	struct sockaddr_in conn_sock;
	conn_sock = accept(listen_sock, (struct sockaddr*)&clnt_addr, &clnt_addr_size);
	if (conn_sock == -1) {
		printf("accept(): ");
		exit(EXIT_FAILURE);
	}
	else {
		printf("Connected client\n");
	}

	ev.events = EPOLLIN;
	ev.data.fd = conn_sock;
	if(epoll_ctl(epfd, EPOLL_CTL_ADD, conn_sock, &ev) == -1) {
		perror("epoll_ctl()");
		exit(EXIT_FAILURE);
	}
}


int init_server(int port) {
	int listen_sock;

	struct sockaddr_in serv_addr;
	struct sockaddr_in clnt_addr;
	socklen_t clnt_addr_size;
	clnt_addr_size = sizeof(clnt_addr);


	int status;
	
	
	listen_sock = socket(AF_INET, SOCK_STREAM, 0);

	memset(&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(atoi(port));

	status  = bind(listen_sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
	if (status == -1) {
		fprintf(stderr, "bind(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}
	
	status = listen(listen_sock, 5);
	if (status == -1) {
		fprintf(stderr, "listen(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	return listen_sock;
}

int init_reactor(struct reactor *ractor) {
	ractor->epfd = epoll_create(1);
	if (ractor->epfd == -1) {
		perror("epoll_create()");
		return -1;
	}

	ractor->head = (struct itemblock *)malloc(sizeof(struct itemblock));
	if (ractor->head == NULL) {
		close(ractor->epfd);
		return -1;
	}
	memset(ractor->head, 0, sizeof(struct itemblock));

	ractor->head->items = (struct item *)malloc(MAX_EVENT * sizeof(struct item));
	if (ractor->head->items == NULL) {
		free(ractor->head);
		close(ractor->epfd);
		return -1;
	}
	memset(ractor->head->items, 0, MAX_EVENT * sizeof(struct item));

	return 0;
}

//accept --> EPOLL
#define READ_CB 0
#define WRITE_CB 1
#define ACCEPT_CB 2
int reactor_set_event(int fd,  int (*callback)(int fd, int event, void *arg), int event, void *arg) {
	struct reactor *ractor = NULL;
	ractor = get_instance();
	if (ractor == NULL) {
		exit(EXIT_FAILURE);
	}

	struct epoll_event ev = {0};
	ev.data.ptr = arg;
	
	if (event == READ_CB) {
		ractor->head->items[fd].fd = fd;
		ractor->head->items[fd].readcb = callback;
		ractor->head->items[fd].arg = arg;
		
		ev.events = EPOLLIN;
		
	} else if (event == WRITE_CB) {
		ractor->head->items[fd].fd = fd;
		ractor->head->items[fd].writecb = callback;
		ractor->head->items[fd].arg = arg;
		
		ev.events = EPOLLOUT;
		
	} else if (event == ACCEPT_CB) {
		ractor->head->items[fd].fd = fd;
		ractor->head->items[fd].acceptcb = callback;
		ractor->head->items[fd].arg = arg;
		
		ev.events = EPOLLIN;
	}

	epoll_ctl(ractor->epfd, EPOLL_CTL_ADD, fd, &ev);
	
	return 0;
}



	
int main(int argc, char *argv[])
{
	int listen_sock, conn_sock;
	char buffer[128];
	int n;

	listen_sock = init_server(atoi(argv[1]));

	reactor_set_event(listen_sock, accept_callback, ACCEPT_CB, NULL);

	/* struct reactor ractor; */
	/* init_reactor(&ractor); */
	

	struct epoll_event events[POLL_SIZE] = {0};
	struct epoll_event ev;

	ev.events = EPOLLIN;
	ev.data.fd = listen_sock;
	
	epoll_ctl(epfd, EPOLL_CTL_ADD, listen_sock, &ev);
	
	for(;;) {
		int ready = epoll_wait(epfd, events, POLL_SIZE, 5);
		if (ready == -1) {
			continue;
		}
		for (int i = 0; i < ready ; ++i) {
			if (events[i].data.fd == listen_sock) {
			
			} else if (events[i].events & EPOLLIN) {
					n = recv(events[i].data.fd, buffer, sizeof(buffer), 0);
					if (n == -1) {
						perror("recv()");
						exit(EXIT_FAILURE);
					}
					if (n > 0) {
						buffer[n] = '\0';
						printf("receive message from client: %s\n", buffer);
						if (send(events[i].data.fd, buffer, n, 0) == -1) {
							perror("send()");
							exit(EXIT_FAILURE);
						}
					} else if (n == 0) {
						printf("close()\n");
						ev.events = EPOLLIN;
						ev.data.fd = events[i].data.fd;
						epoll_ctl(epfd, EPOLL_CTL_DEL, events[i].data.fd, &ev);
						close(events[i].data.fd);
					}
			}
		}
	}

	
	close(listen_sock);
	return 0;
}
