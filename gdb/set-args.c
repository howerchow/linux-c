#include <stddef.h>
#include <stdio.h>

int main(int argc, char *argv[]) {

    printf("输入的参数为: \n");
    for (size_t i = 0; i < argc; ++i) {
        printf("参数: %zu = %s\n", i, argv[i]);
    }

    printf("\n");
    return 0;
}
