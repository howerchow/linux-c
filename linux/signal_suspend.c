#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

static void sig_handler(int sig){
	psignal(sig, "Ouch");
}

int main(int argc, char *argv[])
{
	sigset_t sigset,sigset_before;
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGINT);

	if (signal(SIGINT, sig_handler) == SIG_ERR) {
		perror("signal():");
		exit(EXIT_FAILURE);
	}

	for(int i = 0; i < 10; ++i){
		sigprocmask(SIG_BLOCK, &sigset, &sigset_before);
		for (int j = 0; j < 5; ++j) {
			write(STDOUT_FILENO, "#", 1);
			sleep(1);
		}
		write(STDOUT_FILENO, "\n", 1);
		sigprocmask(SIG_SETMASK, &sigset_before, NULL);
		pause();

	}


	return 0;
}
