#include "unp.h"

#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <error.h>

ssize_t readn(int fd, void *buff, size_t n) {
	size_t nleft;
	ssize_t nread;
	char *ptr;
	ptr = buff;
	nleft = n;

	while (nleft > 0) {
		if ((nread = read(fd, ptr, nleft)) < 0) {
			if (errno == EINTR) {
				nread = 0;
			} else {
				return (-1);
			}
		} else if (nread == 0) {
			break;
		}
		nleft -= nread;
		ptr += nread;
	}
	return (n-nleft);
}


ssize_t written(int fd, const void *vptr, size_t n) {
	size_t nleft;
	ssize_t nwritten;
	const char *ptr;

	ptr = vptr;
	nleft = n;
	while (nleft > 0) {
		if ((nwritten = write(fd, ptr, nleft)) <= 0) {
			if (nwritten < 0 && errno == EINTR) {
				nwritten = 0;
			} else {
				return -1;
			}
		}
		nleft -= nwritten;
		ptr += nwritten;
	}
	return (ssize_t)n;
}

void str_echo(int sockfd) {
	ssize_t n;
	char buf[BUFSIZ] = { 0 };

	while ( (n = read(sockfd, buf, sizeof(buf))) > 0 ) {
		write(sockfd, buf, n);
	}

	if (n == -1) {
		perror("read");
		exit(EXIT_FAILURE);
	}
}

/* void str_cli(FILE *fp, int sockfd) { */
/* 	char sendline[BUFSIZ] = {0}; */
/* 	char recvline[BUFSIZ] = {0}; */

/* 	while (fgets(sendline, sizeof(sendline), fp) != NULL) { */
/* 		if (written(sockfd, sendline,strlen(sedline)) == 0) { */
/* 			perror("") */
/* 		} */
/* 	} */
/* } */
