#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

extern ssize_t readn(int filedes, void *buff, size_t nbytes);
extern ssize_t written(int filedes, const void *buff, size_t nbytes);
extern ssize_t readline(int filedes, void *buff, size_t nbytes);

/* server echo function */
extern void str_echo(int sockfd);
extern void str_cli(FILE *fp, int sockfd);
