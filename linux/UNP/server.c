#include "unp.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/epoll.h>
#include <netinet/in.h>

int main(int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr,"Usage : %s <port>\n" ,argv[0]);
		exit(EXIT_FAILURE);
	}
	int listenfd,connfd;
	pid_t childpid;

	struct sockaddr_in servaddr = {
		.sin_family      = AF_INET,
		.sin_addr.s_addr = htonl(INADDR_ANY),
		.sin_port        = htons(atoi(argv[1]))
	};

	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	if (bind(listenfd, (struct sockaddr*)&servaddr, sizeof(servaddr)) == -1) {
		perror("bind");
		exit(EXIT_FAILURE);
	}
	listen(listenfd, 5);

	struct sockaddr_in cliaddr = {0};
	socklen_t chilen = sizeof(cliaddr);
	char buf[BUFSIZ] = {0};
	int n;
	for (;;) {
		connfd = accept(listenfd, (struct sockaddr*)&cliaddr, &chilen);
		if ( (childpid = fork()) == 0 ) {
			close(listenfd);
			n = read(connfd, buf, sizeof(buf));
			write(connfd, buf, n);
			exit(EXIT_SUCCESS);
		}
		close(connfd);
	}
}
