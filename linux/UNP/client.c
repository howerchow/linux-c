#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/epoll.h>
#include <netinet/in.h>

int main(int argc, char *argv[])
{
	if (argc != 3) {
		fprintf(stderr,"Usage : %s <IP> <port>\n" ,argv[0]);
		exit(EXIT_FAILURE);
	}

	int sockfd;
	struct sockaddr_in servaddr = {
		.sin_family      = AF_INET,
		.sin_addr.s_addr = inet_addr(argv[1]),
		.sin_port        = htons(atoi(argv[2]))
	};

	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	int status = connect(sockfd, (struct sockaddr*)&servaddr, sizeof(servaddr));
	if (status != 0) {
		perror("connect():");
		exit(EXIT_FAILURE);
	}

	char buf[BUFSIZ] = {0};

	for (;;) {
		while (fgets(buf, sizeof(buf), stdin) != NULL) {
			write(sockfd, buf, sizeof(buf));
			read(sockfd, buf, strlen(buf));
			puts(buf);
		}
	}


	return 0;
}
