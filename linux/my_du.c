#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <glob.h>
int64_t my_du(const char *pathname){
	struct stat statbuf;
	char nextpathname[128];
	glob_t globbuf;
	if(lstat(pathname, &statbuf) == -1){
		perror("lstat():");
		exit(EXIT_FAILURE);
	}
	if(S_ISREG(statbuf.st_mode)){
		return statbuf.st_blocks/2;
	}
	strncpy(nextpathname, pathname, sizeof(nextpathname));
	strncat(nextpathname, "/*", sizeof(nextpathname));

	if(glob(nextpathname, 0, NULL, &globbuf) != 0){
		fprintf(stderr, "glob() failed!");
		exit(EXIT_FAILURE);
	}




}


int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr,"Useage....\n");
		exit(EXIT_FAILURE);
	}

	printf("%lld\n",my_du(argv[1]));
	return 0;
}
