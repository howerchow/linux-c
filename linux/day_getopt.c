#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
/* if (strcmp(optarg, "12") == 0) { */
/* 	strncat(fmtstr, "%I", 2); */
/* } */
/* else if (strcmp(optarg, "24") == 0) { */
/* 	strncat(fmtstr, "%H", 2); */
/* } */

int main(int argc, char *argv[])
{
	time_t tm;
	struct tm *timep;
	char outstr[128];
	int ch;
	char fmtstr[256];
	fmtstr[0] = '\0';

	time(&tm);
	if ((timep = localtime(&tm)) == NULL){
		perror("localtime()");
		exit(EXIT_FAILURE);
	}

	while((ch = getopt(argc, argv,"Y:mdH:MS")) != -1){
		switch (ch) {
		case 'Y':
			break;
		case 'm':
			strcat(fmtstr, "%m ");
			break;
		case 'd':
			break;
		case 'H':
			if (strcmp(optarg, "12") == 0) {
				strcat(fmtstr, "%I ");
			}else if (strcmp(optarg, "24") == 0) {
				strcat(fmtstr, "%H ");
			}else{
				fprintf(stderr, "Invalid arguments of H");
			}
			break;
		case 'M':
			break;
		case 'S':
			break;
		default:
			break;
		}
	}

	if (strftime(outstr, sizeof(outstr), fmtstr, timep) == 0){
		fprintf(stderr, "strftime returned 0");
		exit(EXIT_FAILURE);
	}
	puts(outstr);
	return 0;
}
