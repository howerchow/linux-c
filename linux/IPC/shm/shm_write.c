#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */
#include <unistd.h>
#include <stddef.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int i,fd;
	struct stat stat = {0};


	if (argc != 2) {
		fprintf(stderr, "Usage: shmwrite <name>");
		exit(EXIT_FAILURE);
	}

	if((fd = shm_open(argv[1], O_RDWR, 0666)) == -1)
	{
		perror("shm_open");
		exit(EXIT_FAILURE);
	}

	if(fstat(fd,&stat) == -1)
	{
		perror("fstat");
		exit(EXIT_FAILURE);
	}

	u_char *ptr = mmap(NULL,stat.st_size, PROT_READ | PROT_WRITE,MAP_SHARED,fd,0);
	close(fd);

	for (size_t i = 0; i < stat.st_size; ++i) {
		*ptr++ = i % 256;
	}

	return 0;
}
