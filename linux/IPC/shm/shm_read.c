#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */
#include <sys/types.h>
#include <unistd.h>
#include <stddef.h>
#include <string.h>


int main(int argc, char *argv[])
{
	int i,fd;
	struct stat stat = {0};
	u_char c, *ptr;

	if (argc != 2)
	{
		fprintf(stderr, "Usage: shm_read <name>");
		exit(EXIT_FAILURE);
	}

	fd = shm_open(argv[1], O_RDONLY, 0666);
	fstat(fd, &stat);

	ptr = mmap(NULL, stat.st_size, PROT_READ, MAP_SHARED, fd, 0);
	close(fd);

	for (size_t i = 0; i < stat.st_size; ++i) {
		if ((c = *ptr++) != i % 256) {
			fprintf(stderr, "prt[%d] = %d",i,c);
			break;
		}
	}


	return 0;
}
