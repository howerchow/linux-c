#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */
#include <unistd.h>

int main(int argc, char *argv[])
{
	char ch;
	char *ptr;
	int fd,flags;
	off_t length;

	flags = O_RDWR | O_CREAT;

	while ((ch = getopt(argc,argv,"e")) != -1) {
		switch (ch) {
		case 'e':
			flags |= O_EXCL;
			break;
		}
	}

	if (optind != argc -2) {
		fprintf(stderr, "Usage: shm_create [-e] <name> <length>");
		exit(EXIT_FAILURE);
	}

	length = atoi(argv[optind] + 1);


	fd = shm_open(argv[optind], flags, 0777);
	if (fd == -1) {
		perror("shm_open");
		exit(EXIT_FAILURE);
	}

	if(ftruncate(fd, length) == -1)
	{
		perror("ftruncate");
		exit(EXIT_FAILURE);
	}

	ptr = mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (ptr == MAP_FAILED) {
		perror("mmap");
		exit(EXIT_FAILURE);
	}

	return 0;
}
