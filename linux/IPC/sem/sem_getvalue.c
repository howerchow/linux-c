#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <semaphore.h>
#include <sys/types.h>
#include <unistd.h>


int main(int argc, char *argv[])
{
	sem_t *sem;
	int val;

	if (argc != 2 ) {
		fprintf(stderr, "Usage: sem_getvalue <name>");
		exit(EXIT_FAILURE);
	}

	sem = sem_open(argv[1], O_RDONLY);
	if (sem == SEM_FAILED) {
		perror("sem_open");
		exit(EXIT_FAILURE);
	}

	if(sem_getvalue(sem, &val) == -1) {
		perror("sem_getvalue");
		exit(EXIT_FAILURE);
	}

	printf("sem_getvalue: %d\n", val);

	return 0;
}
