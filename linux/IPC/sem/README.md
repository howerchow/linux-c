POSIX信号量和SystemV信号量作用相同，都是用于同步操作，达到无冲突的访问共享资源目的
信号量的本质是一个描述临界资源有效个数的计数器


./build/sem_create /sem_test

./build/sem_getvalue /sem_test

./build/sem_wait /sem_test
CTRL + c

./build/getvalue /sem_test

./build/sem_wait /sem_test &

./build/sem_wait /sem_test &

./build/sem_post /sem_test

./build/sem_post /sem_test

./build/sem_unlink /sem_test
