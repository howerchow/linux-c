#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <semaphore.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

int main(int argc, char *argv[])
{
	sem_t *sem;
	int val;

	if (argc != 2) {
		fprintf(stderr, "Usage: semwait <name>");
		exit(EXIT_FAILURE);
	}

	sem = sem_open(argv[1], O_RDWR);
	if (sem == SEM_FAILED) {
		perror("sem_open");
		exit(EXIT_FAILURE);
	}

	sem_wait(sem);

	if(sem_getvalue(sem, &val) == -1) {
		perror("sem_getvalue");
		exit(EXIT_FAILURE);
	}

	printf("pid %ld has semaphore, value = %d\n", (long)getpid(),val);

	pause();
	return 0;
}
