#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <semaphore.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	char ch;
	int flags;
	sem_t *sem;
	u_int value;

	flags = O_RDWR | O_CREAT;
	value = 1;

	while ((ch = getopt(argc, argv, "ei:")) != -1) {
		switch (ch) {
		case 'e':
			flags |= O_EXCL;
			break;
		case 'i':
			value = atoi(optarg);
			break;
		}
	}

	if (optind != argc-1) {
		fprintf(stderr, "Useage: sem_create [-e] [-i initalvalue] <name>");
		exit(EXIT_FAILURE);
	}

	sem = sem_open(argv[optind], flags, 0666, value );
	if (sem == SEM_FAILED) {
		perror("sem_open");
		exit(EXIT_FAILURE);
	}

	sem_close(sem);

	return 0;
}
