#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <semaphore.h>
#include <sys/types.h>
#include <unistd.h>


int main(int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr, "Useage: sem_unlink <name>");
		exit(EXIT_FAILURE);
	}

	sem_unlink(argv[1]);

	return 0;
}
