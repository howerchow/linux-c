#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <semaphore.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	sem_t *sem;
	int val;
	if (argc != 2 ) {
		fprintf(stderr, "Usage: sem_post <name>");
		exit(EXIT_FAILURE);
	}
	sem = sem_open(argv[1], O_RDWR);

	sem_post(sem);

	sem_getvalue(sem, &val);
	printf("value = %d\n",val);

	return 0;
}
