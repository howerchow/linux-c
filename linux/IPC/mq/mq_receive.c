#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <errno.h>
#include <stddef.h>
#include <memory.h>

int main(int argc, char *argv[])
{
	char ch;
	int flags;
	ssize_t n;
	u_int prio;
	char *buff;
	mqd_t mqd;
	struct mq_attr attr;

	flags = O_RDONLY;
	while ((ch = getopt(argc, argv, "n")) != -1) {
		switch (ch) {
		case 'n':
			flags |= O_NONBLOCK;
			break;
		}
	}

	if (optind != argc -1) {
		fprintf(stderr, "Useage: mqreceive [-n] <name>");
		exit(EXIT_FAILURE);
	}

	mqd = mq_open(argv[optind], flags);
	mq_getattr(mqd, &attr);

	buff = malloc(attr.mq_msgsize);

	n = mq_receive(mqd, buff, attr.mq_msgsize, &prio);
	if (n == -1) {
		perror("mq_receive");
		exit(EXIT_FAILURE);
	}

	printf("read %d bytes, priority = %u\n",(long)n, prio);

	return 0;
}
