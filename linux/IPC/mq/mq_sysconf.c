#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <errno.h>

int main(int argc, char *argv[])
{
	printf("_SC_MQ_OPEN_MAX: %ld\n",sysconf(_SC_MQ_OPEN_MAX));
	printf("_SC_MQ_PRIO_MAX: %ld\n",sysconf(_SC_MQ_PRIO_MAX));
	return 0;
}
