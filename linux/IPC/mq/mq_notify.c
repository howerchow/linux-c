#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <errno.h>
#include <signal.h>

static void handler(int signo);

mqd_t mqd;
struct mq_attr attr = {0};
char *buff = NULL;

struct sigevent sigev =
{
	.sigev_notify = SIGEV_SIGNAL,
	.sigev_signo  = SIGUSR1
};

int main(int argc, char *argv[])
{

	if (argc != 2) {
		fprintf(stderr, "Useage: mq_notify <name>");
		exit(EXIT_FAILURE);
	}


	mqd = mq_open(argv[1], O_RDONLY);
	if (mqd == -1) {
		perror("mq_open()");
		exit(EXIT_FAILURE);
	}


	if(mq_getattr(mqd, &attr) == -1) {
		perror("mq_getattr()");
		exit(EXIT_FAILURE);
	}


	buff = malloc(attr.mq_msgsize);

	signal(SIGUSR1, handler);



	while (true) {
		pause();
	}

	return 0;
}

static void handler(int signo)
{
	ssize_t n;
	mq_notify(mqd, &sigev);
	n = mq_receive(mqd, buff,attr.mq_msgsize, NULL);
	printf("SIGUSR! received, read %ld bytes\n",(long)n);
	return;
}
