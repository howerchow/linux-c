#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <errno.h>

int main(int argc, char *argv[])
{
	int flags;
	char ch;
	mqd_t mqd;
	struct mq_attr mstat ={
		.mq_flags   = O_NONBLOCK,
		.mq_maxmsg  = 10,
		.mq_msgsize = 1024
	};

	flags = O_RDWR | O_CREAT;
	while ((ch = getopt(argc,argv,"e")) != -1) {
		switch (ch) {
		case 'e':
			flags |= O_EXCL;
			break;
		}
	}

	if (optind != --argc) {
		fprintf(stderr, "Useage:mqcreate [ -e ] <name>\n"); // ./a.out -e /mqname
		exit(EXIT_FAILURE);
	}

	mqd = mq_open(argv[optind], flags, 0666, &mstat); // /dev/mqueue 可以看的到新建的mq
	if (mqd == -1) {
		perror("mq_open()");
		exit(EXIT_FAILURE);
	}

	struct mq_attr mqstat = {0};
	mq_getattr(mqd, &mqstat);

	mq_close(mqd);



	return 0;
}
