#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <errno.h>


int main(int argc, char *argv[])
{
	mqd_t  mqd;
	char   *ptr;
	size_t len;
	double  prio;

	if (argc != 4) {
		fprintf(stderr, "Useage: mqsend <name> <#byte> <priority>\n");
		exit(EXIT_FAILURE);
	}

	len  = strtoul(argv[2], NULL, 10);
	prio = strtod(argv[3], NULL);

	mqd = mq_open(argv[1], O_WRONLY);

	ptr = calloc(len, sizeof(char));

	mq_send(mqd, ptr, len, prio);

	return 0;
}
