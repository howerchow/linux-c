#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

int main(int argc, char *argv[])
{
	mqd_t mqd;

	mqd = mq_open("/testQueue2", O_RDWR | O_CREAT | O_EXCL, 0666,NULL);
	if (mqd == -1 )
	{
		fprintf(stderr, "mq_open failed: %s\n",strerror(errno));
		exit(EXIT_FAILURE);
	}

	pid_t pid = fork();
	if (pid == 0) { // children
		struct mq_attr attr = {0};
		if(mq_getattr(mqd, &attr) == -1)
		{
			fprintf(stderr, "mq_getattr failed!\n");
			exit(EXIT_FAILURE);
		}

		char *buf = calloc(attr.mq_maxmsg,attr.mq_msgsize);
		for(size_t i = 1; i <= 5; ++i) {
			if (mq_receive(mqd, buf, attr.mq_msgsize, NULL) == -1) {
				fprintf(stderr, "mq_receive failed: %s\n");
				exit(EXIT_FAILURE);
				continue;
			}

			printf("receive message %d : %s\n",i,buf);
		}
	}

	if (pid > 0) { // parent
		char msg[] = "yuki";
		for(size_t i = 1; i <= 5; ++i)
		{
			if (mq_send(mqd,msg,strlen(msg),i) == -1) {
				fprintf(stderr, "mq_send failed!\n");
				exit(EXIT_FAILURE);
			}

			printf("send message %d success.\n",i);
		}

	}

	if(mq_unlink("/testQueue2") == -1)
	{
		fprintf(stderr, "mq_unlink failed!\n");
		exit(EXIT_FAILURE);
	}

	return 0;
}
