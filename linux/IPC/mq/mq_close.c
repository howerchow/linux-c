#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <errno.h>

int main(int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr, "Useage: nqunlink <name> <>\n");
		exit(EXIT_FAILURE);
	}

	mq_unlink(argv[1]);
	return 0;
}
