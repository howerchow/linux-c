#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>

#define FILE_LENGTH 80

int main(int argc, char *argv[])
{
  int fd = -1;
  char buf[] = "quick brown fox jump over the lazy dog";
  char *ptr = NULL;

  fd = open("memp.txt", O_RDWR|O_CREAT|O_TRUNC,S_IRWXU);
  if (fd == -1) {
    perror("open");
    exit(EXIT_FAILURE);
  }

  /* 文件长度扩大至80 */
  lseek(fd, FILE_LENGTH-1, SEEK_SET);

  write(fd, "a",1);

  ptr = mmap(NULL, FILE_LENGTH, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
  if (ptr==MAP_FAILED) {
    perror("mmap");
    exit(EXIT_FAILURE);
  }

  memcpy(ptr+16, buf, strlen(buf));

  munmap(ptr, FILE_LENGTH);
  close(fd);

  return 0;
}
