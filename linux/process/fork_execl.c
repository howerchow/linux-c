#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>

int main(int argc, char *argv[])
{
	pid_t pid;
	int i;
	for (i = 0; i < 3; ++i)
	{
		pid = fork();

		if (pid == 0)
		{
			break;
		}
	}

	if (pid > 0)
	{
		printf("parent pid %d\nsleeping...\n",getpid());
		while (true)
		{
			sleep(1);
		}
	} else if (pid == 0)
	{
		if (i == 0)
		{
			printf("child no.%d pid %d exec firefox..\n",i, getpid());
			execl("/usr/bin/firefox", "firefox","www.baidu.com",NULL);
		}

		if (i == 1)
		{
			printf("child no.%d pid %d exec touch..\n",i, getpid());
			execl("/usr/bin/touch", "touch","huala",NULL);
		}

		if (i == 2)
		{
			printf("child no.%d pid %d exec ls..\n",i, getpid());
			execl("/bin/ls","ls","-l",NULL);
		}

	}

	return 0;
}
