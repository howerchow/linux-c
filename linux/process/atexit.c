#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void f1(void)
{
	puts("f1() is workding!");
}

static void f2(void)
{
	puts("f2() is workding!");
}

static void f3(void)
{
	puts("f3() is workding!");
}

int main(int argc, char *argv[])
{
	puts("begin!");

	atexit(f1);
	atexit(f2);
	atexit(f3);

	puts("end!");
	return 0;
}
