#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <search.h>

int main(int argc, char *argv[])
{
	pid_t pid;

	printf("[%d] BEGIN!\n" ,getpid());

	fflush(NULL);

	pid = fork();
	if (pid < 0) {
		perror("fork");
		exit(EXIT_FAILURE);
	}

	if (pid == 0) {
		printf("[%d] [%d]: Child is working!\n",getpid(),getppid());
	} else {
		printf("[%d]: Parent is working\n",getpid());
	}

	fprintf(stdout, "END!\n");
	return 0;
}
