#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

#define BUF_SIZE 1024

int main(int argc, char *argv[])
{
	pid_t pid;
	char buf1[BUF_SIZE];


	setvbuf(stdout, buf1, _IOLBF, BUF_SIZE);
	printf("hello world\n");
	//fflush(stdout);

	char buf2[BUF_SIZE] = "Ciao\n";
	write(STDOUT_FILENO, buf2, BUF_SIZE);

	pid = fork();
	if (pid == -1) {
		perror("fork():");
		exit(EXIT_FAILURE);
	}

	/* if (pid == 0) { */
	/*   fflush(stdout); */
	/* }else { */
	/*   waitpid(pid, &wstatus, 0); */

	/*   if (WEXITSTATUS(wstatus)) { */
	/*     fprintf(stdout, "chikd exit\n"); */
	/*   } */
	/* } */
	return 0;
}
