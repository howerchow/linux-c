#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/types.h>
#include <fcntl.h>

static void
handler(int sig)
{
	// do nothing
}

int main(int argc, char *argv[])
{
	pid_t childPid;
	sigset_t blockMask, origMask,emptyMask;

	struct sigaction sa;

	setbuf(stdout, NULL);

	sigemptyset(&blockMask);
	sigaddset(&blockMask, SYNC_SIG);

	return 0;
}
