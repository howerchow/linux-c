#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>

static int data = 111;

int
main (int argc, char *argv[])
{
	int stack = 222;
	pid_t pid;
	printf ("[%d]Begin!\n", getpid ());
	fflush (NULL);

	pid = fork ();
	if (pid < 0)
	{
		perror ("fork():");
		exit (EXIT_FAILURE);
	}

	if (pid == 0) // child
	{
		printf ("[%d]:Child is working!\n", getpid ());
		data  *= 3;
		stack *= 3;
	}
	else         // parent
	{
		sleep(3);
		printf ("[%d]:Parent is working!\n", getpid ());
	}
	printf ("[%d]:End!\n", getpid ());

	if (pid == 0) {
		printf("%d %d\n",data,stack);
	} else if (pid > 0) {
		printf("%d %d\n",data,stack);
	}

	return 0;
}
