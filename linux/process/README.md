# 进程的终止
正常终止：
 return
 exit
 _exit/_Exit
 pthread_exit
 最后一个线程从其启动例程返回

 异常终止
 abort
 signal
 线程取消

# 命令行参数的分析
getop
getop_long

# 多进程
1. 进程的标识pid
   pid_t

   ps axf
   ps axm
   ps ax -L
   ps -ef

   进程号是顺次向下使用

   getpid
   getppid

2. 进程的产生
   fork() vfork()
   未决信号和文件锁不继承，资源利用量归0

3. 进程的消亡及释放资源
   wait waitpid

4. exec函数族

5. 用户权限及组权限

6. 观摩课： 解释器文件

7. system()

8. 进程会计

9. 进程时间

10. 守护进程

11. 系统日志
