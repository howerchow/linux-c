#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

static void
read_childproc(int sig) {
	int status;
	pid_t id = waitpid(-1, &status, WNOHANG);
	printf("read_childproc\n");
	if (WIFEXITED(status)) {
		printf("remove proc id: %d\n" ,id);
		printf("Child send: %d \n", WEXITSTATUS(status));
	}
}

int main(int gc, char *argv[])
{
	pid_t pid;
	setbuf(stdout, NULL);

	struct sigaction act = {
		.sa_handler = read_childproc,
		.sa_mask      = {0},
	};
	sigaction(SIGCLD, &act, NULL);

	printf("Parent PID = %d\n",getpid());

	pid = fork();
	if (pid < 0) {
		perror("fork():");
		exit(EXIT_FAILURE);
	};
	if(pid == 0) { /* child */
		printf("Hi! I'm child: %d\n",getpid());
		sleep(5);
		return 12;
	} else {
		printf("Child proc id: %d\n", pid);
		pid =fork();
		if (pid < 0) {
			perror("fork():");
			exit(EXIT_FAILURE);
		};
		if (pid == 0) { /* child */
			printf("Hi! I'm child: %d\n",getpid());
			sleep(6);
			return 24;
		} else {
			printf("Child proc id: %d\n", pid);
			for (size_t i = 0; i < 10; ++i) {
				puts("wait...");
				sleep(5);
			}
		}
	}

	return 0;
}
