#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/types.h>
#include <fcntl.h>

int main(int argc, char *argv[])
{
	int fd, flags, pid;
	char template[] = "/tmp/testXXXXXX";

	setbuf(stdout, NULL);

	fd = mkstemp(template);
	if (fd == -1) {
		perror("mkstemp");
		exit(EXIT_FAILURE);
	}

	off_t offset;
	offset = lseek(fd, 0, SEEK_CUR);
	printf("file offset before fork(): %lld\n",offset);

	flags = fcntl(fd, F_GETFL);
	if (flags == -1) {
		perror("flags");
		exit(EXIT_FAILURE);
	}
	printf("O_APPEND flag before fork(): %s\n",
	       (flags & O_APPEND) ? "on" : "off");


	pid = fork();
	if (pid == -1) {
		perror("fork()");
		exit(EXIT_FAILURE);
	}
	if (pid == 0) { //child
		if (lseek(fd, 10000, SEEK_CUR) == -1) {
			perror("lseek");
			exit(EXIT_FAILURE);
		}

		flags = fcntl(fd, F_GETFL);
		if (flags == -1) {
			perror("flags");
			exit(EXIT_FAILURE);
		}

		flags |= O_APPEND;
		if (fcntl(fd, F_SETFL,flags) == -1) {
			perror("fcntl");
			exit(EXIT_FAILURE);
		}
		_exit(EXIT_SUCCESS);

	} else {        //parent
		if (wait(NULL) == -1) {
			exit(EXIT_FAILURE);
		}
		printf("child has exited!\n");
		printf("Parent file offset %lld\n",lseek(fd, 0, SEEK_CUR));

		flags = fcntl(fd, F_GETFL);
		if (flags == -1) {
			perror("flags");
			exit(EXIT_FAILURE);
		}

		printf("parent O_APPEND flags: %s",
		       (flags & O_APPEND)? "on" : "off");
		exit(EXIT_SUCCESS);
	}

}
