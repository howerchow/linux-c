#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */

int main(int argc, char *argv[])
{
	int fd;
	size_t len;
	char *addr;

	if (argc != 3 || strcmp(argv[1],"--help") == 0) {
		fprintf(stdout, "%s shm-name string", argv[0]);
		exit(EXIT_FAILURE);
	}

	fd = shm_open(argv[1],O_RDWR,0);
	if (fd == -1) {
		perror("shm_open()");
		exit(EXIT_FAILURE);
	}

	len = strlen(argv[2]);
	if (ftruncate(fd,len) == -1) {
		perror("ftruncate()");
		exit(EXIT_FAILURE);
	}

	printf("Resize to %ld bytes\n",len);

	return 0;
}
