#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int gc, char *argv[])
{
	pid_t pid;

	puts("Begin!");
	fflush(NULL);

	pid = fork();
	if (pid < 0) {
		perror("fork():");
		exit(EXIT_FAILURE);
	}else if (pid == 0) {
		execl("/bin/date", "date","+%s",NULL);
		perror("execl()");
		exit(EXIT_FAILURE);
	}else {
		wait(NULL);
		puts("End!");
	}



	return 0;
}
