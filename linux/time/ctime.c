#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <stdint.h>

int main(int gc, char *argv[])
{
	time_t tm;
	char utcbuf[128] = {0};
	char localbuf[128] = {0};

	tm = time(NULL);

	/* char *time_format = ctime(&tm); */
	/* puts(time_format); */

	// UTC
	struct tm *utctm = NULL;
	utctm = gmtime(&tm);
	strftime(utcbuf,sizeof(utcbuf), "%Y-%m-%d %H:%M:%S", utctm);
	puts(utcbuf);

	// Local
	struct tm *localtm = NULL;
	localtm = localtime(&tm);
	strftime(localbuf, sizeof(localbuf), "%Y-%m-%d %H:%M:%S", localtm);
	puts(localbuf);

	struct timeval tv = {0};
	if(!gettimeofday(&tv, NULL))
	{
		double sec = tv.tv_sec + tv.tv_usec/1.0e6;
		printf("seconds since epoch:%g\n",sec);
	}


	return 0;
}
