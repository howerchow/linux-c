#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, char *argv[])
{
	char date[1024] = { 0 };
	time_t now;
	time(&now);

	strcpy(date, ctime(&now));

	date[strlen(date) - 1] = '\0';

	printf("%s\n",date);

	return 0;
}
