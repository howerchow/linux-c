#include <stdio.h>
#include <time.h>

int main(int argc, char *argv[])
{
	struct tm *ptm = NULL;
	time_t t = time(NULL);
	ptm = localtime(&t);
	char *p = asctime(ptm);

	printf("%s\n",p);
	return 0;
}
