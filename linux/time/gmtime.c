#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>

int main(int gc, char *argv[])
{

  char buf[128] = {0};
  char buf1[128] = {0};


  time_t t;
  t = time(NULL);

  // UTC tm
  struct tm *utctm = NULL;
  utctm = gmtime(&t);
  strftime(buf, sizeof(buf), "UTC: %Y-%m-%d %A %H:%M:%S", utctm);

  // local tm
  struct tm *localtm = NULL;
  localtm = localtime(&t);
  strftime(buf1, sizeof(buf1), "Local: %Y-%m-%d %A %H:%M:%S", localtm);

  puts(buf);
  puts(buf1);

  return 0;
}
