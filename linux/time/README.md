-> time() -> time_t
-> gmtime()/localtime -> struct tm
-> strftime() -> 用户格式的本地化字符串

用户格式的本地化字符串
-> strptime() -> struct tm
-> mktime() -> time_t
-> stime()

time_t -> 固定格式字符串
ctime()
struct tm -> 固定格式字符串
asctime()
