#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <unistd.h>

#define FILE_NAME  "/tmp/out"

int main(int argc, char *argv[])
{

	FILE *fp = NULL;
	fp = fopen(FILE_NAME,"a+");
	if (!fp) {
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	int line = 0;
	char buffer[BUFSIZ] = {0};

	/*
	 * fgets做多可以读到size-1个字符,给\0预留一位
	 * fgets遇到\n就停止本次读取
	 * buf放得下\n就读\n,否则下一次读 \n\0
	 * 1. xxxxx\n\0
	 * 2. \n\0
	 */

	while(fgets(buffer, sizeof(buffer), fp)) {
		if (buffer[strlen(buffer)-1] == '\n') {
			++line; //统计行数
		}
	}

	time_t stamp;
	struct tm *tm = NULL;

	while (true) {

		time(&stamp);
		if( !(tm = localtime(&stamp)) ) {
			perror("localtime()");
			exit(EXIT_FAILURE);
		}

		char buf[BUFSIZ] = {0};
		if(strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", tm) == 0) {
			fprintf(stderr, "strftime returned 0");
			exit(EXIT_FAILURE);
		}

		fprintf(fp,"%-4d %s\n",++line, buf);
		fflush(fp);

		sleep(1);
	}

	fclose(fp);

	return 0;
}
