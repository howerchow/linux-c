#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>

int main(int argc, char *argv[])
{
	FILE *fp;

	char *line = NULL;
	size_t len = 0;
	ssize_t nread;
	int count;

	time_t tm;
	struct tm *timep;
	char outstr[128];

	if ((fp = fopen("log.txt", "a+")) == NULL) {
		perror("fopen():");
		exit(EXIT_FAILURE);
	}

	while ((nread = getline(&line, &len, fp)) != -1) {
		++count;
	}
	free(line);

	while (true) {
		time(&tm);
		if ((timep = localtime(&tm)) == NULL){
			perror("localtime()");
			exit(EXIT_FAILURE);
		}

		if (strftime(outstr, sizeof(outstr), "%Y-%m-%d %H:%M:%S", timep) == 0){
			fprintf(stderr, "strftime returned 0");
			exit(EXIT_FAILURE);
		}
		//fwrite(outstr, 1, sizeof(outstr), fp);
		fprintf(fp, "%3d %s\n",++count,outstr);
		fflush(fp);
		sleep(1);

	}
	fclose(fp);

	return 0;
}
