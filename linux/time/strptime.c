#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define _XOPEN_SOURCE
#include <time.h>

int
main(void)
{
	struct tm tm = {0};
	char buf[255] = {0};

	strptime("2001-11-12 18:31:01", "%Y-%m-%d %H:%M:%S", &tm);
	strftime(buf, sizeof(buf), "%d %b %Y %H:%M", &tm);
	puts(buf);
	exit(EXIT_SUCCESS);
}
