#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int fd = open("hello.txt", O_RDWR);
	if (fd < 0) {
		perror("open failed！");
		exit(EXIT_FAILURE);
	}

	char *buf = "hello";
	write(fd, buf, strlen(buf));

	lseek(fd, -1, SEEK_END);

	char buf1[4];
	read(fd, buf1, 4);

	printf("%s\n" ,buf1);

	close(fd);
	return 0;
}
