#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>

static int counter;

void *thread_routine (void *arg)
{
	printf("thread_routine starting\n");
	for (counter = 0; ; ++counter) {
		if (counter % 100 == 0)
		{
			printf("calling testcancel\n");
			pthread_testcancel();
		}
	}
}

int main(int argc, char *argv[])
{
	pthread_t thread_id;
	void *result;
	int status;

	pthread_create(&thread_id, NULL, thread_routine, NULL);

	pthread_cancel(thread_id);


	pthread_join(thread_id, &result);

	if (result == PTHREAD_CANCELED)
		printf("Thread canceled at iteration %d\n",counter);
	else
		printf("Thread was not cancel\n");
	return 0;
}
