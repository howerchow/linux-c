#include <stdio.h>
#include <error.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>

pthread_cond_t cond;

int main(int argc, char *argv[])
{
	pthread_condattr_t cond_attr;
	int status;

	status = pthread_condattr_init(&cond_attr);

	status  = pthread_condattr_getpshared(&cond_attr, PTHREAD_PROCESS_PRIVATE);

	status = pthread_cond_init(&cond, &cond_attr);


	return 0;
}
