#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <error.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

typedef struct my_struct_tag {
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	int value;
} my_struct_t;

my_struct_t data = {
	PTHREAD_MUTEX_INITIALIZER,
	PTHREAD_COND_INITIALIZER,
	0
};

int hibernation = 1;

void *wait_thread(void *arg) {
	int status;

	sleep(hibernation);

	pthread_mutex_lock(&data.mutex);
	data.value = 1;
	pthread_cond_signal(&data.cond);
	pthread_mutex_unlock(&data.mutex);

	return NULL;
}

int main(int argc, char *argv[])
{
	int status;
	pthread_t wait_pthread_id;
	struct timespec timeout =
	{
		.tv_sec  = time(NULL) + 2,
		.tv_nsec = 0,
	};

	if (argc > 1) {
		hibernation = atoi(argv[1]);
	}

	pthread_create(&wait_pthread_id, NULL, wait_thread, NULL);

	pthread_mutex_lock(&data.mutex);

	while (data.value == 0)
	{
		if(pthread_cond_timedwait(&data.cond, &data.mutex, &timeout) == ETIMEDOUT) {
			printf("Condition wait timed out\n");
			break;
		}

		if (data.value != 0) {
			pthread_mutex_unlock(&data.mutex);
			printf("unlock\n");
		}
	}

	printf("hello\n");
	return 0;
}
