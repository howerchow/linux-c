#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>


typedef struct private_tag
{
	pthread_t thread_id;
	char *string;
} private_t;

pthread_key_t identify_key;

pthread_mutex_t identify_key_mutex = PTHREAD_MUTEX_INITIALIZER;

long identify_key_counter = 0;

void identify_key_destructor (void *value)
{
	private_t *private = (private_t*)value;
	int status;

	printf("thread \"%s\" exiting...\n",private->string);

	free(value);

	pthread_mutex_lock(&identify_key_mutex);

	--identify_key_counter;

	if (identify_key_counter <= 0)
	{
		pthread_key_delete(identify_key);
		printf("key deleted...\n");
	}

	pthread_mutex_unlock(&identify_key_mutex);
}

void *identity_key_get (void)
{
	void *value;

	value = pthread_getspecific (identify_key);
	if (value == NULL)
	{
		value = malloc (sizeof (private_t));
		pthread_setspecific (identify_key, (void*)value);
	}

	return value;
}

void *thread_routine (void *arg)
{
	private_t *value;
	value = (private_t *)identity_key_get();
	value->thread_id = pthread_self();
	value->string = (char *)arg;

	printf("thread \"%s\" starting...\n" ,value->string);

	sleep(2);
	return NULL;
}

int main (int argc, char *argv[])
{
	pthread_t thread_1, thread_2;
	private_t *value;

	pthread_key_create (&identify_key, identify_key_destructor);

	identify_key_counter = 3;
	value = (private_t *)identity_key_get();
	value->thread_id = pthread_self ();
	value->string = "Main thread";

	pthread_create(&thread_1, NULL, thread_routine, "thread1");
	pthread_create(&thread_1, NULL, thread_routine, "thread2");

	pthread_exit(NULL);

	return 0;
}
