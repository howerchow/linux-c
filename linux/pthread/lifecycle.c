#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
/*
 * Thread start routine
 */

void *thread_routine(void *arg){
	// while(1);
	sleep(5);
	return arg;
}

int main(int argc, char *argv[])
{
	pthread_t thread_id;
	void *thread_result;
	int status;

	status = pthread_create(&thread_id, NULL, thread_routine, "world");
	if (status != 0) {
		fprintf(stderr, "pthread_create(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	status = pthread_join(thread_id, &thread_result);
	if (status != 0) {
		fprintf(stderr, "pthread_join(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	if (thread_result == NULL) {
		return 0;
	}else {
		printf("%s\n",thread_result);
		return 1;
	}
	return 0;
}
