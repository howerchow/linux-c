#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>


pthread_cond_t cond;

int main(int argc, char *argv[])
{
	pthread_condattr_t cond_attr;
	pthread_condattr_init(&cond_attr);
	pthread_condattr_setpshared(&cond_attr, PTHREAD_PROCESS_PRIVATE);

	pthread_cond_init(&cond, &cond_attr);

	pthread_cond_destroy(&cond);

	return 0;
}
