#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include <errno.h>
#include <string.h>

pthread_once_t once_block = PTHREAD_ONCE_INIT;
pthread_mutex_t mutex;

void once_init_routine(void){
	int status;
	status = pthread_mutex_init(&mutex, NULL);
	if (status < 0) {
		fprintf(stderr, "pthread_mutex_init(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

}

void *thread_routine(void *agr){
	int status;
	status = pthread_once(&once_block, once_init_routine);
	if (status < 0) {
		fprintf(stderr, "pthread_mutex_init(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	status = pthread_mutex_lock(&mutex);
	if (status < 0) {
		fprintf(stderr, "pthread_mutex_lock(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	printf("thread_routine has locked the mutex.\n");

	status = pthread_mutex_unlock(&mutex);
	if (status < 0) {
		fprintf(stderr, "pthread_mutex_unlock(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

}

int main(int argc, char *argv[])
{
	pthread_t thread_id;
	char *input,buffer[64];
	int status;

	status = pthread_create(&thread_id, NULL, thread_routine,NULL);
	if (status < 0) {
		fprintf(stderr, "pthread_create(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	status = pthread_once(&once_block, once_init_routine);
	if (status < 0) {
		fprintf(stderr, "pthread_mutex_init(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	status = pthread_mutex_lock(&mutex);
	if (status < 0) {
		fprintf(stderr, "pthread_mutex_lock(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	printf("Main thread has locked the mutex.\n");

	status = pthread_mutex_unlock(&mutex);
	if (status < 0) {
		fprintf(stderr, "pthread_mutex_unlock(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	status = pthread_join(thread_id, NULL);
	if (status < 0) {
		fprintf(stderr, "pthread_join(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}


	return 0;
}
