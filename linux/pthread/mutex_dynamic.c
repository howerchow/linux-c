#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include <string.h>

typedef struct my_struct_tag{
	pthread_mutex_t mutex;
	int value;
} my_struct_t;


int main(int argc, char *argv[])
{
	my_struct_t *data;
	int status;

	data = malloc(sizeof(my_struct_t));
	if (data == NULL) {
		fprintf(stderr, "malloc()");
		exit(EXIT_FAILURE);
	}

	status = pthread_mutex_init(&data->mutex, NULL);
	if (status != 0) {
		fprintf(stderr, "pthread_mutex_init(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	status = pthread_mutex_destroy(&data->mutex); // 销毁互斥量
	if (status != 0) {
		fprintf(stderr, "pthread_mutex_destroy(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	free(data);
	return 0;
}
