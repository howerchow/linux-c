#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <stdbool.h>

void *count1(void *arg) {
	for (size_t i = 0; i < 10; ++i) {
		printf("count1: %d\n",i);
		sleep(1);
	}


}

void *count2(void *arg) {
	for (size_t i = 0; i < 10; ++i) {
		printf("count2: %d\n",i);
		sleep(2);
		pthread_exit(NULL);
	}
}

int main(int argc, char *argv[])
{
	pthread_t id1,id2;
	if(pthread_create(&id1, NULL, count1, NULL) < 0) {
		perror("pthread_create():");
		exit(EXIT_FAILURE);
	}
	if(pthread_create(&id2, NULL, count2, NULL) < 0) {
		perror("pthread_create():");
		exit(EXIT_FAILURE);
	}

	pthread_join(id1, NULL);
	pthread_join(id2, NULL);

	return 0;
}
