#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>

#define SPIN 10000000

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
long counter;
time_t end_time;

void *counter_thread(void *arg){
	int status;

	while (time(NULL) < end_time) {
		status = pthread_mutex_lock(&mutex);
		if (status < 0) {
			fprintf(stderr, "pthread_mutex_lock(): ", strerror(status));
			exit(EXIT_FAILURE);
		}

		for (int spin = 0; spin < SPIN; ++spin) {
			++counter;
		}

		status = pthread_mutex_unlock(&mutex);
		if (status < 0){
			fprintf(stderr, "pthread_mutex_unlock(): ", strerror(status));
			exit(EXIT_FAILURE);
		}

		printf("Counter is %#lx\n" ,counter);
		return NULL;
	}

}

void *monitor_thread(void *arg){
	int status;
	int misses = 0;

	while (time(NULL) < end_time) {
		sleep(3);
		status = pthread_mutex_trylock(&mutex);
		if (status != EBUSY) {

		}
	}
}
