# 线程标识
pid_t
pthread_t
ps axm
一个正在运行的函数
线程的创建
线程的终止
栈的清理
线程的取消的选项

# 线程的创建
pthread_crate

栈的清理
pthread_cleanup_push
pthread_cleanup_pop

# 线程同步
pthread_equal
pthread_self

# 线程的终止：
1. 线程从启动例程中返回，返回值就是线程的退出码
2. 线程可以被同一进程中的其他线程取消
3. 线程调用pthread_exit()函数

# 线程的取消选项
pthread_cancel()
取消有2种状态： 允许和不允许
允许取消又分为：
异步cancel,推迟cancel(默认)->推迟至cancel点在响应
cancel: POSIX定义的cancel点，都是可能引发阻塞的系统调用

pthread_setcancelstat(): 设置是否允许取消
pthread_setcanceltype(): 设置取消方式
pthread_testcancel(): 本函数什么都不做，就是一个取消点

pthread_join()

# 线程分离
pthread_detach()已经detach的线程不能join()

# 线程同步
互斥量:
pthread_mutex_t
pthread_mutex_init
pthread_mutex_destroy
pthread_mutext_lock
ptrhead_mutex_trylock
pthread_mutex_unlock

条件变量:
pthread_cond_t
pthread_cond_init
pthread_cond_destroy
pthread_cond_broadcast
pthread_cond_signal

信号量:
