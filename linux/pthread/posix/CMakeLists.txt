cmake_minimum_required(VERSION 3.23)

project(sem
  VERSION 2.3.1
  LANGUAGES C
)

add_executable(add add.c)
target_link_libraries(add PUBLIC pthread)

add_executable(abcd abcd.c)
target_link_libraries(abcd PUBLIC pthread)

add_executable(test_prime_pool test_prime_pool.c)
target_link_libraries(test_prime_pool PUBLIC pthread m)
