#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stddef.h>

#define THRNUM 20
#define FNAME "/tmp/out"

static pthread_mutex_t mut = PTHREAD_MUTEX_INITIALIZER;

static void *thr_add(void *p)
{
	char buf[BUFSIZ] = {0};

	FILE *fp;
	fp = fopen(FNAME, "r+");
	if (!fp) {
		perror("fopen");
		exit(EXIT_FAILURE);
	}
	pthread_mutex_lock(&mut);

	fgets(buf, sizeof(buf), fp);
	fseek(fp, 0, SEEK_SET);
	fprintf(fp, "%d\n",atoi((buf)) + 1);
	fclose(fp);

	pthread_mutex_unlock(&mut);
	pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
	int err;
	pthread_t tid[THRNUM];

	for (size_t i = 0; i < THRNUM; ++i) {
		err = pthread_create(tid+i, NULL, thr_add, NULL);
		if (err) {
			fprintf(stderr, "pthread_create(): %s\n", strerror(err));
			exit(EXIT_FAILURE);
		}
	}

	for (size_t j = 0; j < THRNUM; ++j) {
		pthread_join(tid[j], NULL);
	}

	pthread_mutex_destroy(&mut);

	return 0;
}
