#include <sched.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>

#define LEFT   30000000
#define RIGHT  30000200
#define THRNUM 4

static int num = 0;
static pthread_mutex_t mut_num = PTHREAD_MUTEX_INITIALIZER;

static void *thr_prime(void *p)
{
	int i, flag = true;
	while (true) {
		pthread_mutex_lock(&mut_num);
		while (num == 0) {
			pthread_mutex_unlock(&mut_num);
			sched_yield();
			pthread_mutex_lock(&mut_num);
		}
		if (num == -1) {
			pthread_mutex_unlock(&mut_num); // 解锁要先解锁
			break;
		}
		i  = num;
		num = 0;
		pthread_mutex_unlock(&mut_num);

		if (i <= 1) { // x<=1，则不是质数
			flag = false;
		}
		double sqrti = sqrt(i);

		for (size_t j =2; j < sqrti && flag; ++j) {
			if (i % j == 0) {  // 若x能被2~x-1中的整数整除，则不是素数
				flag = false ;
				break; // 只要找到第一个,即可跳出循环
			}
		}

		if (flag) {
			printf("[%d]%d is a primer\n",*(int*)p,i);
		}
	}
	pthread_exit(NULL);
	//return flag; //若是素数则返回true，否则返回false
}

int main(int argc, char *argv[])
{
	int err;
	pthread_t tid[THRNUM];
	for (size_t i = 0; i <= THRNUM; ++i) {
		err = pthread_create(tid+i, NULL, thr_prime, &i);
		if (err) {
			fprintf(stderr, "pthread_create: %s", strerror(err));
			exit(EXIT_FAILURE);
		}
	}

	for (size_t i = LEFT; i <= RIGHT; ++i) {
		pthread_mutex_lock(&mut_num);
		while (num != 0) {
			pthread_mutex_unlock(&mut_num);
			sched_yield();
			pthread_mutex_lock(&mut_num);
		}
		num = i; // 放任务
		pthread_mutex_unlock(&mut_num);
	}

	pthread_mutex_lock(&mut_num);
	while (num != 0) {
		pthread_mutex_unlock(&mut_num);
		sched_yield();
		pthread_mutex_lock(&mut_num);
	}
	num = -1;
	pthread_mutex_unlock(&mut_num);

	for (size_t j = 0; j <= THRNUM; ++j) {
		pthread_join(tid[j], NULL);
	}

	pthread_mutexattr_destroy(&mut_num);

	return 0;
}
