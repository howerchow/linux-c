#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <pthread.h>

#define LEFT   30000000
#define RIGHT  30000200
#define THRNUM (RIGHT - LEFT + 1)

static void *thr_prime(void *p)
{
	int i, flag = true;
	i = *(int*)p;

	if (i <= 1) { // x<=1，则不是质数
		flag = false;
	}
	double sqrti = sqrt(i);

	for (size_t j =2; j < sqrti && flag; ++j) {
		if (i % j == 0) {  // 若x能被2~x-1中的整数整除，则不是素数
			flag = false ;
			break; // 只要找到第一个,即可跳出循环
		}
	}

	if (flag) {
		printf("%d is a primer\n",i);
	}

	pthread_exit(NULL);
	//return flag; //若是素数则返回true，否则返回false
}


int main(int argc, char *argv[])
{
	int err;
	pthread_t tid[THRNUM];
	for (size_t i = LEFT; i <= RIGHT; ++i) {
		err = pthread_create(tid+(i - LEFT), NULL, thr_prime, &i);
		if (err) {
			fprintf(stderr, "pthread_create: %s", strerror(err));
			exit(EXIT_FAILURE);
		}
	}
	for (size_t j = LEFT; j <= RIGHT; ++j) {
		pthread_join(tid[j - LEFT], NULL);
	}

	return 0;
}
