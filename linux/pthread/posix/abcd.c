#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>

#define THRNUM 4

static void *thr_func(void *p)
{
	int c = 'a' + *(int*)p;

	while (true) {
		write(STDOUT_FILENO, &c, 1);
	}

	pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
	pthread_t tid[THRNUM];
	int err;

	for (size_t i = 0; i < 4; ++i) {
		err = pthread_create(tid+i, NULL, thr_func, (void*)&i);
		if (err) {
			fprintf(stderr, "pthread_create: %s", strerror(err));
			exit(EXIT_FAILURE);
		}
	}

	alarm(1);

	for (size_t j = 0; j < THRNUM; ++j) {
		pthread_join(tid[j], NULL);
	}

	return 0;
}
