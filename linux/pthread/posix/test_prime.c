#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#define LEFT   30000000
#define RIGHT  30000200
#define THRNUM (RIGHT - LEFT + 1)

bool isPrime(unsigned long x)
{
	int i, flag = true;
	if (x < 1) { // x<=1，则不是质数
		flag = false;
	}
	double sqrtx = sqrt(x);

	for (size_t i =2; i < sqrtx && flag; ++i) {
		if (x % i == 0) {  // 若x能被2~x-1中的整数整除，则不是素数
			flag = false ;
			break; // 只要找到第一个,即可跳出循环
		}
	}
	return flag; //若是素数则返回true，否则返回false
}


int main(int argc, char *argv[])
{
	bool mark;
	for (size_t i = LEFT; i < RIGHT; ++i) {
		mark = true;
		for (size_t j = 2 ; j < sqrt(i); ++j) {
			if (i % j == 0) {
				mark = false;
				break;
			}
		}

		if (mark) {
			printf("%d is a primer\n" ,i);
		}
	}

	return 0;
}
