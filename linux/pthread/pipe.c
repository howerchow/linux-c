#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <error.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>


typedef struct stage_tag {
	pthread_mutex_t       mutex;
	pthread_cond_t        avail;
	pthread_cond_t        ready;
	int                   data_ready;
	long                  data;
	pthread_t             thread;
	struct stage_tag      *next;
} stage_t;

typedef struct pipe_tag {
	pthread_mutex_t  mutex;
	stage_t         *head;
	stage_t         *tail;
	int             statges;
	int             active;
} pipe_t;
