#include "alarm_cond.h"

#include <stdio.h>
#include <stdlib.h>
#include <error.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>


time_t current_alarm = 0;

int alarm_list_create(alarm_list_t *alarm_list) {
	alarm_list  = malloc(sizeof(alarm_list_t));
	if (!alarm_list) {
		return -1;
	}
	alarm_list->head = NULL;
	pthread_mutex_init(&alarm_list->alarm_mutex, NULL);
	pthread_cond_init(&alarm_list->alarm_cond, NULL);
	return 0;
}

void  alarm_list_insert(alarm_list_t *alarm_list, alarm_t *alarm) {
	if (!alarm_list->head) {
		alarm_list->head = alarm;
	} else {
		alarm_t **cur = &alarm_list->head;
		while (*cur) {
			if ( (*cur)->time > alarm->time ) break;
			cur = &(*cur)->next;
		}
		alarm->next = *cur;
		*cur = alarm;
	}

	if (current_alarm == 0 || alarm->time < current_alarm) {
		current_alarm = alarm->time;
		int status = pthread_cond_signal(&alarm_list->alarm_cond);
		if (status != 0) {
			fprintf(stderr, "pthread_cond_signal: %s\n", strerror(status));
			exit(EXIT_FAILURE);
		}
	}
}

void *alarm_list_thread(void *arg) {
	alarm_list_t *alarm_list = (alarm_list_t *)arg;
	alarm_t **alarm;
	struct timespec cond_time = {0};
	time_t now;
	int status, expired;

	status = pthread_mutex_lock(&alarm_list->alarm_mutex);
	if (status != 0) {
		fprintf(stderr, "pthread_mutex_lock: %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	while (true) {
		while (alarm_list->head == NULL) {
			status = pthread_cond_wait(&alarm_list->alarm_cond, &alarm_list->alarm_mutex);
			if (status != 0) {
				fprintf(stderr, "pthread_mutex_lock: %s\n", strerror(status));
				exit(EXIT_FAILURE);
			}
		}

		alarm         = &alarm_list->head;
		now           = time(NULL);
		expired       = 0;
		current_alarm = (*alarm)->time;

		if ((*alarm)->time > now) {
 			cond_time.tv_sec  = (*alarm)->time;
			while (current_alarm == (*alarm) ->time) {
				status = pthread_cond_timedwait(&alarm_list->alarm_cond, &alarm_list->alarm_mutex,&cond_time);
				if (status == ETIMEDOUT) {
					expired = 1;
					break;
				}
				if (status != 0) {
					fprintf(stderr, "pthread_cond_timedwait: %s\n", strerror(status));
					exit(EXIT_FAILURE);
				}
			}
			if(!expired)
				alarm_list_insert(alarm_list,*alarm);
		} else {
			expired = 1;
		}

		if (expired) {
			printf("(%d) %s\n" , (*alarm)->seconds, (*alarm)->message);
			alarm_list->head = (*alarm)->next;
			free(*alarm);
		}
	}

}
