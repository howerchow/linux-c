#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <error.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>

#include "alarm_cond.h"

int main(int argc, char *argv[])
{
	char line[128] = {0};
	alarm_t *alarm;
	pthread_t thread;

	alarm_list_t *alarm_list;
	alarm_list_create(alarm_list);

	int status = pthread_create(&thread, NULL, alarm_list_thread, alarm_list);
	if (status != 0) {
		fprintf(stderr, "pthread_create(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	while(true) {
		printf("alarm>");
		if (fgets(line, sizeof(line), stdin) == NULL) {
			exit(EXIT_FAILURE);
		}
		if (strlen(line) <= 1) {
			continue;
		}

		alarm = malloc(sizeof(alarm_t));
		if (alarm == NULL) {
			fprintf(stderr, "malloc()");
			exit(EXIT_FAILURE);
		}

		if(sscanf(line, "%d %64[^\n]",&alarm->seconds,alarm->message) < 2) {
			fprintf(stderr, "Bad command\n");
			free(alarm);
		} else {
			status = pthread_mutex_lock(&alarm_list->alarm_mutex);
			if (status != 0) {
				fprintf(stderr, "pthread_mutex_lock: %s",strerror(status));
				exit(EXIT_FAILURE);
			}
			alarm->time = time(NULL) + alarm->seconds;
			alarm_list_insert(alarm_list,alarm);
			status = pthread_mutex_unlock(&alarm_list->alarm_mutex);
			if (status != 0) {
				fprintf(stderr, "pthread_mutex_unlock: %s",strerror(status));
				exit(EXIT_FAILURE);
			}
		}
	}

	return 0;
}
