#ifndef ALARM_COND_H
#define ALARM_COND_H
#include <time.h>
#include <pthread.h>

typedef struct alarm_t {
	int seconds;
	time_t time;
	char message[64];
	struct alarm_t *next;
} alarm_t;

typedef struct alarm_list_t {
	alarm_t *head;
	pthread_mutex_t alarm_mutex;
	pthread_cond_t  alarm_cond;
} alarm_list_t;

int alarm_list_create(alarm_list_t *alarm_list);
void  alarm_list_insert(alarm_list_t *alarm_list, alarm_t *alarm);
void *alarm_list_thread(void *arg);

#endif /* ALARM_COND_H */
