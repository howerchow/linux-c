#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <error.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

typedef struct alarm_tag {
	struct alarm_tag *link;
	int seconds;
	time_t time;
	char message[64];
} alarm_t;

pthread_mutex_t alarm_mutex = PTHREAD_MUTEX_INITIALIZER;

alarm_t *alarm_list = NULL;

void *alarm_thread(void *arg);

int main(int argc, char *argv[])
{
	int status;
	char line[128];
	alarm_t *alarm, **last, *next;
	pthread_t thread;

	status = pthread_create(&thread, NULL, alarm_thread, NULL);
	if (status != 0) {
		fprintf(stderr, "pthread_create(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	while(true) {
		printf("alarm>");
		if (fgets(line, sizeof(line), stdin) == NULL) {
			exit(EXIT_FAILURE);
		}
		if (strlen(line) <= 1) {
			continue;
		}

		alarm = malloc(sizeof(alarm_t));
		if (alarm == NULL) {
			fprintf(stderr, "malloc()");
			exit(EXIT_FAILURE);
		}

		if(sscanf(line, "%d %64[^\n]",&alarm->seconds,alarm->message) < 2){
			fprintf(stderr, "Bad command\n");
			free(alarm);
		}else{
			status = pthread_mutex_lock(&alarm_mutex);
			if (status != 0) {
				fprintf(stderr, "pthread_mutex_lock: %s",strerror(status));
				exit(EXIT_FAILURE);
			}
			alarm->time = time(NULL) + alarm->seconds;

			last = &alarm_list;
			next = *last;
			while (next != NULL) {
				if (next->time >= alarm->time) {
					alarm->link = next;
					*last = alarm;
					break;
				}
				last = &next->link;
				next = next->link;
			}

			if (next == NULL) {
				*last = alarm;
				alarm->link = NULL;
			}

			status = pthread_mutex_unlock(&alarm_mutex);
			if (status != 0) {
				fprintf(stderr, "pthread_mutex_unlock: %s",strerror(status));
				exit(EXIT_FAILURE);
			}
		}
	}
	return 0;
}

void *alarm_thread(void *arg)
{
	alarm_t *alarm;
	int sleep_time;
	time_t now;
	int status;

	while(true){
		status = pthread_mutex_lock(&alarm_mutex);
		if (status != 0) {
			fprintf(stderr, "pthread_mutex_lock(): %s\n", strerror(status));
			exit(EXIT_FAILURE);
		}

		alarm = alarm_list;
		if (alarm == NULL) {
			sleep_time = 1; //列表中没有新的请求，阻塞一段时间，解锁互斥量,以便主线程添加新的闹铃
		} else {
			alarm_list = alarm->link;
			now = time(NULL);
			if (alarm->time < now) { //闹玲过期
				sleep_time = 0;
			}else {
				sleep_time = alarm->time - now;
			}
		}
		status = pthread_mutex_unlock(&alarm_mutex);
		if (status !=0) {
			fprintf(stderr, "pthread_mutex_unlock(): %s\n", strerror(status));
			exit(EXIT_FAILURE);
		}

		if (sleep_time > 0) {
			sleep(sleep_time);
		} else {
			sched_yield();
		}

		if (alarm != NULL) {
			printf("(%d) %s\n",alarm->seconds,alarm->message);
			fflush(stdout);
			free(alarm);
		}


	}
}
