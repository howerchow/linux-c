#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>

typedef struct tsd_tag {
	pthread_t thread_id;
	char *string;
} tsd_t;


pthread_key_t tsd_key;

pthread_once_t key_once = PTHREAD_ONCE_INIT;

void once_create_key (void)
{
	int status;

	printf("initializing key\n");
	pthread_key_create(&tsd_key, NULL);

}

void *thread_routine (void *arg)
{
	tsd_t *value;
	int status;

	pthread_once(&key_once, once_create_key);

	value = malloc(sizeof(tsd_t));

	pthread_setspecific(tsd_key, value);

	value->thread_id = pthread_self();
	value->string = (char *)arg;

	value = pthread_getspecific(tsd_key);
	printf("%s done...\n",value->string);

	return NULL;
}

int main(int argc, char *argv[])
{
	pthread_t thread1, thread2;
	int status;

	pthread_create(&thread1, NULL, thread_routine, "thread1");
	pthread_create(&thread2, NULL, thread_routine, "thread2");

	pthread_exit(NULL);
	return 0;
}
