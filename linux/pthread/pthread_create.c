#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

static void cleanup_func(void *p){
	puts(p);
}

static void *thread_func(void *p){
	puts("Thread is working!");

	pthread_cleanup_push(cleanup_func, "cleanup_func1");
	pthread_cleanup_push(cleanup_func, "cleanup_func2");
	pthread_cleanup_push(cleanup_func, "cleanup_func3");

	puts("push over!");

	pthread_cleanup_pop(1);
	pthread_cleanup_pop(1);
	pthread_cleanup_pop(1);

	pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
	pthread_t tid;
	int status;

	puts("Begin");

	status = pthread_create(&tid, NULL, thread_func,NULL);
	if (status != 0) {
		fprintf(stderr, "pthread_create(): %s\n", strerror(status));
		exit(EXIT_FAILURE);
	}

	pthread_join(tid, NULL);
	puts("End");
	return 0;
}
