#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>


pthread_mutex_t mutex;

int main(int argc, char *argv[])
{
	/* mutexattr */
	pthread_mutexattr_t mutex_attr;
	int status = pthread_mutexattr_init(&mutex_attr);
	if (status != 0) {
		fprintf(stderr, "pthread_mutexattr_init(): ", strerror(status));
		exit(EXIT_FAILURE);
	}
	status = pthread_mutexattr_setpshared(&mutex_attr, PTHREAD_PROCESS_SHARED);
	if (status != 0) {
		fprintf(stderr, "pthread_mutexattr_setpshared(): ", strerror(status));
		exit(EXIT_FAILURE);
	}
	int att;
	pthread_mutexattr_getpshared(&mutex_attr, &att);

	pthread_mutexattr_destroy(&mutex_attr);

	/* mutex */
	status = pthread_mutex_init(&mutex, &mutex_attr);
	if (status != 0) {
		fprintf(stderr, "pthread_mutex_init ", strerror(status));
		exit(EXIT_FAILURE);
	}

	pthread_mutex_destroy(&mutex);

	return 0;
}
