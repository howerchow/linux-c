#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>


void *thread_routine (void *arg)
{
	printf("The thread is here\n");
	return NULL;
}

int main(int argc, char *argv[])
{
	pthread_attr_t thread_attr;
	pthread_t thread_id;

	struct sched_param thread_param;
	size_t stack_size;
	int status;

	status = pthread_attr_init (&thread_attr);

	status = pthread_attr_setdetachstate (&thread_attr, PTHREAD_CREATE_DETACHED);
	status = pthread_attr_getstacksize (&thread_attr, &stack_size);
	printf("Default stack size is %u;minimum is %u\n",stack_size,PTHREAD_STACK_MIN);
	status = pthread_attr_setstacksize (&thread_attr, PTHREAD_STACK_MIN * 2);

	status = pthread_create (&thread_id, &thread_attr, thread_routine, NULL);

	printf("Main exiting\n");

	pthread_exit (NULL);

	return 0;
}
