#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int status = 99;

	pthread_t thread_id;

//    status = pthread_join(thread_id, NULL);

	fprintf(stderr,"pthread_join() at \"%s\":%d: %s\n", __FILE__,__LINE__,strerror(status));
	exit(EXIT_FAILURE);

	return 0;
}
