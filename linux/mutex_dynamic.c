#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include <memory.h>
typedef struct{
  pthread_mutex_t mutex;
  int value;
} my_struct_t;

int main(int argc, char *argv[])
{
  my_struct_t *data;
  int status;

  data = (my_struct_t *)malloc(sizeof(my_struct_t));
  if (data == NULL) {
    fprintf(stderr, "malloc():");
    exit(EXIT_FAILURE);
  }

  pthread_mutex_init(&data->mutex, NULL);
  pthread_mutex_destroy(&data->mutex);
  free(data);
  return 0;
}
