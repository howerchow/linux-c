#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "src/link_list.h"

int main(int argc, char *argv[])
{
	char line[128];
	int value;
	LinkList *list = create_list();
	for (size_t i = 0; i < 5; ++i) {
		printf("intput>");
		if (fgets(line, sizeof(line), stdin) == NULL) {
			exit(EXIT_FAILURE);
		}
		if (strlen(line) <= 1) {
			continue;
		}

		if(sscanf(line, "%d",&value) < 1){
			fprintf(stderr, "Bad command\n");
			return 0;
		}
		insertSorted2(list,value);//降序
	}

	print_list(list);

	int last1 = 1;
	Node *k1 = last_k(list, last1);
	if (k1) {
		printf("倒数第%d个节点的值为:%d\n",last1,k1->value);
	}

	int last4 = 4;
	Node *k4 = last_k(list,last4);
	if (k4) {
		printf("倒数第%d个节点的值为:%d\n",last4,k4->value);
	}
	k1->next = k4;


	// linklist_reverse(list);
	// print_list(list);

	// delete_duplicates(list);
	// print_list(list);

	if(has_cycle(list)) {
		printf("list has cycle\n");
	}

	Node *entry = cycle_entry(list);
	printf("cycle entry: %d\n" ,entry->value);
	/* destroy_list(&list); */
	/* if (!list) { */
	/* 	printf("list has been delted\n"); */
	/* } */

	return 0;
}
