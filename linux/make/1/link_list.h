#ifndef LIND_LIST_H
#define LIND_LIST_H
#include <stdbool.h>

typedef struct _Node {
	int value;
	struct _Node* next;
} Node;

typedef struct _LikList {
	int  len;
	Node *head;
} LinkList;

/* 创建单链表 */
extern LinkList * create_list();

/* 删除链表 */
extern void destroy_list(LinkList **list);

/* 打印单链表 */
extern void print_list(LinkList *list);

/* 逆置单链表 */
extern void linklist_reverse(LinkList *list);

/* 降序插入单链表 */
extern void insertSorted2(LinkList *list, int value); // 降序

/* 倒数第K个节点 */
extern Node * last_k(LinkList *list, int k);

/* 删除链表节点(指定值)*/
extern void delete_value(LinkList *list, int value);

/* 删除有序链表重复元素 */
extern void delete_duplicates(LinkList *list);

/* 是否有环*/
extern bool has_cycle(LinkList *list);

/* 环入口 */
extern Node* cycle_entry(LinkList *list);
#endif /* LIND_LIST_H */
