#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[])
{
	time_t tm;
	struct tm *timep;
	char outstr[128];
	time(&tm);
	if ((timep = localtime(&tm)) == NULL){
		perror("localtime()");
		exit(EXIT_FAILURE);
	}

	timep->tm_mday += 180;
	mktime(timep);
	if (strftime(outstr, sizeof(outstr), "%Y-%m-%d %H:%M:%S", timep) == 0){
		fprintf(stderr, "strftime returned 0");
		exit(EXIT_FAILURE);
	}
	puts(outstr);

	return 0;
}
