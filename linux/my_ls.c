#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>

void list_files(const char *dirpath){
	DIR *dirp;
	struct dirent *dp;
	struct stat statbuf;

	if ((dirp = opendir(dirpath)) == NULL) {
		perror("opendir()");
		exit(EXIT_FAILURE);
	}

	while((dp = readdir(dirp)) !=NULL) {
		if (strcmp(dp->d_name,".")  == 0 || strcmp(dp->d_name,"..")  == 0 ) {
			continue;
		}

		if(stat(dp->d_name,&statbuf)  == -1){
			perror("stat():");
			exit(EXIT_FAILURE);
		}
		printf("%s\n", dp->d_name);
	}
	closedir(dirp);
}

int main(int argc, char *argv[])
{
	int i = 0;
	if(argc > 1 && strcmp(argv[1],"--help") == 0){
		fprintf(stdout,"%s [dir...]\n",argv[0]);
	}
	if (argc == 1) {
		list_files(".");
	}else{
		while(argv[i+1] != NULL){
			list_files(argv[i++]);
		}

	}
	return 0;
}
