# 动态库
soname      别名 libxxx.o
realname    真名 libxxx.o.1.2.3
linker name 链接名

1. 生成动态库
```shell
	gcc -g -c -fPIC -Wall link_list.c link_list.h
    gcc -g -shared -W1,-soname,liblinklist.so -o liblinklist.1.12.1.so link_list.o
	ln -s liblinklist.so liblinklist.1.12.1.so
```

2. 链接动态库

```shell
	gcc -o test main.c -L./ -llinklist
```
