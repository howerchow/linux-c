# 静态库

1. 生成静态库
```shell
	ar -rcs liblinklist.a link_list.o
```

2. 链接静态库

```shell
	gcc -o test main.c liblinklist.a
```

```shell
	gcc -o test main.c -L./ -llinklist
```
