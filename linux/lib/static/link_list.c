#include "link_list.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <memory.h>
#include <stdbool.h>

LinkList * create_list()
{
	LinkList *list = malloc(sizeof(LinkList));
	list->len  = 0;
	list->head = NULL;
	return list;
}

void destroy_list(LinkList **list)
{
	Node **cur = &(*list)->head;
	while (*cur) {
		Node *entry = *cur;
		*cur = (*cur)->next;
		free(entry);
	}
	(*list)->head = NULL;
	(*list)->len = 0;
	free(*list);
	*list = NULL;
}

void print_list(LinkList *list)
{
	Node **cur = &list->head;
	while(*cur) {
		printf("%d->" ,(*cur)->value);
		fflush(stdout);
		cur = &(*cur)->next;
	}
	printf("NULL\n");
	return;
}

void linklist_reverse(LinkList *list)
{

	if (!list->head) {
		return;
	}

	Node *last = list->head;
	while (last->next != NULL) {
		Node *entry = last->next;
		last->next = last->next->next;
		/* 头插法 */
		++list->len;
		entry->next = list->head;
		list->head = entry;
	}
	return;
}

/* 降序 */
void insertSorted2(LinkList *list, int value)
{
	Node *n  = malloc(sizeof(Node));
	n->value = value;
	n->next  = NULL;

	++list->len;

	Node **cur = &list->head;
	while (*cur) {
		if ((*cur)->value < n->value) break;
		cur = &(*cur)->next;
	}
	n->next = *cur;
	*cur    = n;
}

Node *last_k(LinkList *list, int k)
{
	if (!list->head) {
		return NULL;
	}
	Node *slow,*fast;
	slow = fast = list->head;

	for (size_t i = 0; i < k - 1; ++i) {
		if (!fast->next) {
			return NULL;
		}
		fast = fast->next; // fast先走到第k个节点
	}

	while (fast->next) {
		slow = slow->next;
		fast = fast->next;
	}
	return slow;
}

void delete_value(LinkList *list, int value)
{
	Node **cur = &list->head;
	while (*cur) {
		Node *entry = *cur;
		if (entry->value == value) {
			*cur = entry->next;
			free(entry);
			--list->len;
		} else {
			cur = &entry->next;
		}
	}
}

void delete_duplicates(LinkList *list)
{
	Node **cur = &list->head;
	while (*cur && (*cur)->next) {
		if ((*cur)->value == (*cur)->next->value) {
			Node *entry = *cur;
			*cur = (*cur)->next;
			free(entry);
		} else {
			cur = &(*cur)->next;
		}
	}
	return;
}

bool has_cycle(LinkList *list)
{
	if (!list->head) {
		return false;
	}

	Node *slow,*fast;
	slow = fast = list->head;
	while (fast->next && fast->next->next) {
		fast = fast->next->next;
		slow = slow->next;
		if (slow == fast) {
			return true;
		}
	}
	return false;

}

Node* cycle_entry(LinkList *list)
{
	if (!list->head) {
		return NULL;
	}

	Node *slow,*fast;
	slow = fast = list->head;
	while (fast->next && fast->next->next) {
		fast = fast->next->next;
		slow = slow->next;
		if (slow == fast) {
			break;
		}
	}
	/* fast slow相遇则有环, fast开始从第一个节点开始走
	   slow从相遇的位置开始走, fast与slow再相遇则是环入口
	 */
	fast = list->head;
	while (fast != slow) {
		fast = fast->next;
		slow = slow->next;
	}
	return fast;
}
