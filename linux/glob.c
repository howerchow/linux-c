#include <stdio.h>
#include <stdlib.h>
#include <glob.h>

#define PAT "/etc/*"

int main(int gc, char *argv[])
{
	glob_t pglob;
	int i = 0;
	if(glob(PAT, 0,NULL,&pglob) != 0){
		perror("glob()");
		exit(EXIT_FAILURE);
	}
	while (pglob.gl_pathv[i] != NULL) {
		puts(pglob.gl_pathv[i++]);
	}
	globfree(&pglob);
	return 0;
}
