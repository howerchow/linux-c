#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <signal.h>

bool loop = true;
int count = 0;

static void
handler(int sig,siginfo_t *si,void *uc)
{
	printf("Cought signo: %d\n", sig, ++count);
	if (count == 5) {
		loop = false;
	}
}

int main(int argc, char *argv[])
{

	/* Establish handler for timer signal. */
	struct sigaction sa = {
		.sa_flags = SA_SIGINFO,
		.sa_sigaction = handler,
	};
	sigemptyset(&sa.sa_mask);

	if (sigaction(SIGALRM, &sa, NULL) == -1) {
		fprintf(stderr, "sigaction()\n");
		exit(EXIT_FAILURE);
	}

	/* Create a timer */
	timer_t timer;
	timer_create(CLOCK_REALTIME, NULL, &timer);
	/* Start the timer */
	struct itimerspec newit = {
		.it_value.tv_sec = 1,
		.it_interval.tv_sec = 1
	};
	timer_settime(&timer, 0, &newit, NULL);

	/* Loop */
	struct timespec sl = {
		.tv_sec  = 0,
		.tv_nsec = 200000000
	};
	while (loop) {
		printf("do something\n");
		nanosleep(&sl,NULL);
	}

	return 0;
}
