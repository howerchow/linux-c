#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>

/*
 * -y: year
 * -m: month
 * -d: day
 * -H: hour
 * -M: minute
 * -S: second
 */

int main(int argc, char *argv[])
{
	time_t stamp;
	struct tm *tm;
	char timerstr[BUFSIZ] = {0};
	int c;

	char fmt[BUFSIZ] = {0};

	stamp = time(NULL);
	tm = localtime(&stamp);



	while (true) {
		c = getopt(argc, argv, "HMSymd");
		if (c == -1) {
			break;
		}
		switch (c) {
		case 'H':
			if (strcmp(optarg,"12") == 0) {
				strncat(fmt, "%I(%P)", BUFSIZ);
			} else if (strcmp(optarg, "24") == 0) {
				strncat(fmt, "%H", BUFSIZ);
			} else {
				fprintf(stderr, "Invalid argument");
			}
			break;
		case 'M':
			strncat(fmt, "%M",BUFSIZ);
			break;
		case 'S':
			break;
		case 'y':
			break;
		case 'm':
			break;
		case 'd':
			break;
		default:
			break;

		}
	}

	strftime(timerstr, sizeof(timerstr), fmt, tm);
	puts(timerstr);

	return 0;
}
