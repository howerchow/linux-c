#include <stdio.h>
#include <stdlib.h>
#include <mqueue.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>

static void
usageError(const char *progName)
{
	fprintf(stderr, "Usage: %s [-cx] [-m maxmsg] [-s msgsize] mq-name" "[octal-perms\n]",progName);
	fprintf(stderr, "    -c              Create queue (O_CREAT)\n");
	fprintf(stderr, "    -m maxmsg       Set maximum # of messages\n");
	fprintf(stderr, "    -s msgsize      Set maximum message size\n");
	fprintf(stderr, "    -x              Create exclusive (O_EXCL)\n");
	exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	int flags, opt;
	mode_t perms;
	mqd_t mqd;

	struct mq_attr attr = {
		.mq_maxmsg  = 50,
		.mq_msgsize = 1024
	};
	struct mq_attr *attrp = NULL;

	flags = O_RDWR;

	while ((ch = getopt(argc, argv, "cm:s:x")) != 1) {
		switch (ch) {
		case 'c':
			flages |= O_CREAT;
			break;
		case 'm':
			attr.mq_maxmsg = atoi(optarg);
			attrp = &attr;
			break;
		case 's':
			attr.mq_msgsize = atoi(optarg);
			attrp = &attr;
			break;
		case 'x':
			flags |= O_EXCL;
			break;
		default:
			usageError(argv[0]);
		}
	}

	if (optind > gargc) {
		usageError(argv[0]);
	}


	mqd = mq_open(argv[optind], flags,perms,attrp);
	if (mqt == -1) {
		exit(EXIT_FAILURE);
	}

	exit(EXIT_FAILURE);

}
