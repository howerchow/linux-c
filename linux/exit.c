#include <stdio.h>
#include <stdlib.h>

static void func1(void){
  printf("atexit func1 called\n");
}

static void func2(void){
  printf("atexit func2 called\n");
}

int main(int argc, char *argv[])
{

  if (atexit(func1) != 0) {
    fprintf(stderr, "atexit1 error\n");
    exit(EXIT_FAILURE);
  }

    if (atexit(func2) != 0) {
    fprintf(stderr, "atexit2 error\n");
    exit(EXIT_FAILURE);
  }
    return 0;
}
