#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

void int_handler(int sig){
	printf("%s\n",strsignal(sig));
}

int main(int argc, char *argv[])
{
	sigset_t sig_set,sig_old_set,save_set;
	sigemptyset(&sig_set);
	sigaddset(&sig_set, SIGINT);

	if(signal(SIGINT, int_handler) == SIG_ERR){
		perror("signal()");
		exit(EXIT_FAILURE);
	}

	sigprocmask(SIG_UNBLOCK, &sig_set, &save_set);
	for (int i = 0; i < 1000; ++i) {
		sigprocmask(SIG_BLOCK, &sig_set, &sig_old_set);
		for (int j = 0; j < 10; ++j) {
			write(STDOUT_FILENO, "#", 1);
			sleep(1);
		}
		printf("\n");
		sigprocmask(SIG_SETMASK, &sig_old_set, NULL);
		pause();
	}
	sigprocmask(SIG_SETMASK, &save_set, NULL);
	return 0;
}
