#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int main(int argc, char *argv[])
{
	FILE *fps, *fpd;
	char buf[BUFSIZ] = {0};

	fps = fopen(argv[1],"r");
	if (!fps) {
		perror("fopen1");
		exit(EXIT_FAILURE);
	}

	fpd = fopen(argv[2],"w");
	if (!fps) {
		fclose(fps);
		perror("fopen2");
		exit(EXIT_FAILURE);
	}

	while ( fgets(buf, BUFSIZ, fps) != NULL ) {
		fputs(buf, fpd);
	}


	fclose(fpd);
	fclose(fps);


	return 0;
}
