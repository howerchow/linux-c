#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>

int main(int argc, char *argv[])
{
	struct stat st;
	if(stat("README.md", &st) == -1) {
		perror("stat");
		exit(EXIT_FAILURE);
	}

	if (S_ISREG(st.st_mode)) {
		printf("regular file\n");
	}

	char buf[BUFSIZ];
	strcpy(buf, ctime(&st.st_atim.tv_sec));
	buf[strlen(buf)-1] = '\0';

	/* char *p = ctime(&st.st_atim.tv_sec); */
	/* p[strlen(p)-1] = '\0'; */

	printf("%s\n",buf);
	fflush(stdout);

	return 0;
}
