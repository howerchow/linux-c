#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>


int main(int argc, char *argv[])
{
	size_t len;
	off_t offset;
	int fd,ap,j;

	char *buf;
	ssize_t numRead, numWritten;

	if (argc < 3 || strcmp(argv[1], "--help") == 0) {
		fprintf(stderr, "Useage: %s file {r<length>}|R<length>|w<string>|s<offset>}...\n");
		exit(EXIT_FAILURE);
	}

	//fd = open(argv[1], O_RDWR | O_CREAT,S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH );
	fd = open(argv[1], O_RDWR | O_CREAT, 0666);
	if (fd == -1) {
		perror("open");
		exit(EXIT_FAILURE);
	}

	close(fd);
	return 0;
}
