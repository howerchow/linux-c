#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{
	FILE *fp;
	char buf[5] = {0};
	int count;

	fp = fopen("temp", "r");
	if (!fp) {
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	while (fgets(buf, 5, fp) != NULL) {
		++count;
	}

	printf("count = %d", count);


	return 0;
}
