#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

int main(int argc, char *argv[])
{
	FILE *fp;
	char buf[BUFSIZ] = "hello";
	fp = fopen("temp", "a+");
	if (!fp) {
		fprintf(stderr, "fopen(): %s", strerror(errno));
		//perror("fopen()");
		exit(EXIT_FAILURE);
	}
	fseek(fp, -10L, SEEK_END);

	fwrite(buf, 1, strlen(buf),fp);
	fflush(fp);

	char *lineptr = NULL;
	size_t n = 0,nn = 0;
	while ((nn = getline(&lineptr,&n,fp)) != -1 )
	{
		fputs(lineptr, stdout);
	}

	fclose(fp);
	free(lineptr);

	return 0;
}
