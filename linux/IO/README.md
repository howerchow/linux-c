# 标准IO
FILE 类型贯穿始终

fopen
fclose

fgetc
fputc

fgets

fputs

fread
fwrite

printf
fprintf
sprintf
snprintf

scanf
fscanf
sscanf

fseek 定位
ftell
feeko
ftello
rewind

fflush

getline

umask 022

0666 & ~umask 0644

临时文件

tmpnam
tmpfile

# 系统调用IO

文件IO操作: open close read write lseek
文件共享
原子操作
重定向: dup dup2
同步： sync fsync fdatasync
fcntl
ioctl
/dev/fd

lseek
fileno fdopen

# 标准IO与系统调用IO 不可以混合调用
# 高级IO
