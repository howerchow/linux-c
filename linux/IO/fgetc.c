#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Usage...\n");
		exit(EXIT_FAILURE);
	}

	FILE *fp;
	int count;

	fp = fopen(argv[1], "r");
	if (!fp) {
		perror("fopen()");
		exit(EXIT_FAILURE);
	}

	while (fgetc(fp) != EOF) { // 统计文件字符个数
		++count;
	}

	printf("count = %d\n",count);

	fclose(fp);
	return 0;
}
