#!/bin/bash

function add_array_1() {
    local sum=0
    local new_array=($(echo $@)) # 在函数內构建新的数组
    for var in ${new_array[*]}; do
	sum=$[ $sum + $var ];
    done
    echo $sum # 函数输出
}

function add_array_2() {
    local sum=0
    for var in $@; do
	let sum+=var
    done
    echo $sum
}


my_array=(1 2 3 4 5)
echo "The original array is: ${my_array[*]}"
echo "The original array is: ${my_array[@]}"

result_1=$(add_array_1 ${my_array[@]}) # 传递数组所有元素
echo "result_1 is: $result_1"

result_2=`add_array_2 ${my_array[*]}`
echo "result_2 is: $result_2"
