#!/usr/bin/env bash

# tempfile=$(mktemp test19.XXXXXX)
# exec 3>$tempfile

# echo "This script writes to temp file $tempfile"

# echo "This is first line" >&3
# echo "This is second line" >&3
# echo "This is the last line" >&3

# exec 3>&-

# echo "Done creating temp file, the content are:"
# cat $tempfile
# rm -f $tempfile 2> /dev/null

# 创建临时文件
# tempfile=$(mktemp -t tmp.XXXXXX)

# echo "This is a test file" > $tempfile
# echo "This is the second line of the test." >>$tempfile

# echo "The tmp file is located at: $tempfile"
# cat $tempfile
# rm -rf $tempfile

# 创建临时目录
tempdir=$(mktemp -d dir.XXXXXX)
cd $tempdir || exit
tempfile1=$(mktemp temp.XXXXXX)
tempfile2=$(mktemp temp.XXXXXX)

exec 7> $tempfile1
exec 8> $tempfile2

echo "Sending data to directory $tempdir"
echo "This is a test line of data for $tempfile1" >&7
echo "This is a test line of data for $tempfile2" >&8

exec 7>&-
exec 8>&-
