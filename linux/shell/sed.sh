#!/usr/bin/env bash

# sed [option] 'command' file(s)
# 只有当命令在语法错误时, sed的退出状态才是非0
# 不管是否找到指定的模式, 它的退出状态都是0

# 支持正则
# ^ $ . * [] [^] \<\> \(\) \{\}
# ? + {} | ()

head /etc/passwd  > /tmp/passwd.txt
# sed  -r 's/root/alice/gi' /tmp/passwd.txt
sed -r '/root/d' /tmp/passwd.txt

# 地址
# 地址用于对哪些行进行编辑
# 地址可以是数字 正则表达式 或二者的结合
#
# sed -r 'd'   /tmp/passwd.txt
# sed -r '3d'  /tmp/passwd.txt
# sed -r '1,3d'  /tmp/passwd.txt
# sed -r '/root/d'  /tmp/passwd.txt
# sed -r '/root/,5d' /tmp/passwd.txt  #删到5行
# sed -r '/root/,+5d' /tmp/passwd.txt #再删5行
# sed -r 's/root/alice/g' /tmp/passwd.txt
# sed -r 's/root/d' /tmp/passwd.txt   #有root行都删除
# sed -r 's/root/!d' /tmp/passwd.txt  #除root行都删除
# sed -r '1~2d' /tmp/passwd.txt  #删除奇数行
# sed -r '0~2d' /tmp/passwd.txt  #删除偶数行

# 命令
#
#!/usr/bin/env bash

# sed [option] 'command' file(s)
# 只有当命令在语法错误时, sed的退出状态才是非0
# 不管是否找到指定的模式, 它的退出状态都是0

# 支持正则
# ^ $ . * [] [^] \<\> \(\) \{\}
# ? + {} | ()

head /etc/passwd  > /tmp/passwd.txt
# sed  -r 's/root/alice/gi' /tmp/passwd.txt
sed -r '/root/d' /tmp/passwd.txt

# 地址
# 地址用于对哪些行进行编辑
# 地址可以是数字 正则表达式 或二者的结合
#
# sed -r 'd'   /tmp/passwd.txt
# sed -r '3d'  /tmp/passwd.txt
# sed -r '1,3d'  /tmp/passwd.txt
# sed -r '/root/d'  /tmp/passwd.txt
# sed -r '/root/,5d' /tmp/passwd.txt  #删到5行
# sed -r '/root/,+5d' /tmp/passwd.txt #再删5行
# sed -r 's/root/alice/g' /tmp/passwd.txt
# sed -r 's/root/d' /tmp/passwd.txt   #有root行都删除
# sed -r 's/root/!d' /tmp/passwd.txt  #除root行都删除
# sed -r '1~2d' /tmp/passwd.txt  #删除奇数行
# sed -r '0~2d' /tmp/passwd.txt  #删除偶数行

# 命令
#!/usr/bin/env bash

# sed [option] 'command' file(s)
# 只有当命令在语法错误时, sed的退出状态才是非0
# 不管是否找到指定的模式, 它的退出状态都是0

# 支持正则
# ^ $ . * [] [^] \<\> \(\) \{\}
# ? + {} | ()

head /etc/passwd  > /tmp/passwd.txt
# sed  -r 's/root/alice/gi' /tmp/passwd.txt
sed -r '/root/d' /tmp/passwd.txt

# 地址
# 地址用于对哪些行进行编辑
# 地址可以是数字 正则表达式 或二者的结合
#
# sed -r 'd'   /tmp/passwd.txt
# sed -r '3d'  /tmp/passwd.txt
# sed -r '1,3d'  /tmp/passwd.txt
# sed -r '/root/d'  /tmp/passwd.txt
# sed -r '/root/,5d' /tmp/passwd.txt  #删到5行
# sed -r '/root/,+5d' /tmp/passwd.txt #再删5行
# sed -r 's/root/alice/g' /tmp/passwd.txt
# sed -r 's/root/d' /tmp/passwd.txt   #有root行都删除
# sed -r 's/root/!d' /tmp/passwd.txt  #除root行都删除
# sed -r '1~2d' /tmp/passwd.txt  #删除奇数行
# sed -r '0~2d' /tmp/passwd.txt  #删除偶数行

# 命令
# a 在当前行后添加一行或多行
# c 用新文本修改(替换)当前行中的文本
# d 删除行
# i 在当前行之前插入文本
# l 列出非打印字符
# p 打印行
# n 读入下一输入行，并从下一条命令而不是第一条命令开始对其处理
# q 结束或退出sed
# ! 对所选行以外的所有行应用命令
# s 用一个字符串替换另一个 s替换 g全局替换 i忽略大小写
# r 从文件读入
# w 将行写入文件
# y 将字符转换为另一个字符(不支持正则)

# 选项
# -e 允许多项编辑
# -f 指定sed脚本文件名
# -n 取消默认的输出
# -i inplace就地编辑
# -r 支持扩展元字符

# 示例
# 删除: d
# sed -r '3d' datafile   #删除第三行
# sed -r '3{d;}' datafile
# sed -r '3{d}' datafile
# sed -r '3,$d' datafile #删除第3行到最末行
# sed -r '$d' datafile   #删除最末行
# sed -r '/north/d' datafile
# sed -r '/sout/d' datafile

# 替换 s
# sed -r 's/west/north/g' datafile
# sed -r 's/^west/north' datafile
# sed -r 's/[0-9][0-9]$/&.5/' datafile
# sed -r 's/Hemenway/Jones/g' datafile
# sed -r 's/(Mar)got/\1ianne/g' datafile

# 读文件 r
# sed -r '/Suan/r /etc/newfile' datafile
# sed -r '2r /ect/hosts' a.txt
# sed -r '/2/r /etc/hosts' a.txt

# 写文件
# sed -r '/north/w newfile' datafile
# sed -r '3,$w /new1.txt' datafile

# 追加命令: a
# sed -r '2a\11111111111' /etc/hosts
# sed -r '2a\11111111111\

# 插入命令: i
# sed -r '2i\111111111' /etc/hosts



# https://www.bilibili.com/video/BV11b41147tW?p=70
