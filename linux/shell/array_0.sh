#!/usr/bin/env bash

# array3=(`cat /ect/passwd`) 文件每一行赋值给数组元素

# 关联数组
declare -A info
info=([name]=jack [sex]=mal [age]=36 [height]=185)
echo ${info[name]}
echo ${!info[@]} # 所有索引

# 普通数组
# array3[#array3[@]] 元素个数
# array3[!array3[@]] 所有索引
# array3[array3[@]:2:2] 从下标2开始 取两个元素
array3=(jack peter rose tom)
for i in ${!array3[@]}; do # 通过索引 遍历数组
    echo ${array3[i]}
done

for ele in ${array3[*]}; do # 通过元素 遍历数组
    echo $ele
done
ec
ec
