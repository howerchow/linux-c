#!/bin/bash

# 局部变量
#ip=10.18.42.2
#ping -cl $ip &>/dev/null && echo "$ip is up" || echo "$ip is down!"

# 环境变量
# export var_name
# env 查看环境变量

# 位置变量
# $1 $2.....$9 ${10}

# 预定义变量
# $0 脚本名
# $* 所有的参数
# $@ 所有的参数
# $# 参数个数
# $$ 当前进程的PID
# $! 上一个后台进程的PID
# $? 上一个命令的返回值

if [ $# -eq 0 ]
then
    echo "Usage: `basename $0` <file>"
fi
if [ ! -f $1 ]
then
    echo "error file!"
    exit
fi

# 变量赋值
# 显示赋值
# read从键盘输入变量值
# 定义或引用变量时注意事项：
# ""弱引用
# ''强引用 是啥就是啥

# 变量的运算
# sum='expr 1 \* 2'
# echo $((2**3)) echo $((num1/num2))
# echo $[2%2]
# let num=1+2

# 变量内容的删除和替换
# url=www.sina.com; echo ${url#*.} 最短匹配 从前往后
# url=www.sina.com; echo ${url##*.} 贪婪匹配
# url=www.sina.com; echo ${url%.*} 最短匹配 从后往前
# url=www.sina.com; echo ${url%%.*}
#
# 索引及切片
# url=www.sina.com; echo ${url:0:5}
#
# 替换
# url=www.sina.con; echo ${url//n/N}
# www.siNa.coN
# echo ${var2-aaaaa}
# echo ${var3:-bbbb} 变量没有被赋值(包括空值): 都会被"新的变量值"替代
# :+ := :?

# ()   子shell执行
# (()) 数值比较
# $()  命令替换
# $(()) $[] 整数运算
# {}
# ${} 变量引用
# []   条件测试
# [[]] 条件测试,支持正则

# 调试
# sh -n 02.sh  仅调试syntax error
# sh -vx 02.sh 以调试的方式,查询整个执行过程
