#!/bin/bash

if [ $# -ne 3 ]
then
    echo "Usage: test9.sh a b"
else
    let total=($1 + $2 + $3)
    echo "The last is ${!#}" # 最后一个参数: ${!#}
    echo "The total is $total"
fi

for param in $*; do
    echo "$param" | tee -a out.txt
done

cat -n out.txt
