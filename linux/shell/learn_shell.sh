#!/bin/bash

start=$(date +%s)
ls -alFh &>/dev/null
end=$(date +%s)
difference=$((end- start))
echo $difference
