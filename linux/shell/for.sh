#!/bin/bash

# https://www.bilibili.com/video/BV11b41147tW?p=36

# for 变量名 in [list]
# do
#     循环体
# done

# for ((初值;条件;步长))
# do
#     循环体
# done

# file_name="/etc/yp.conf /etc/nsswitch.conf /etc/auto.master /etc/resolve.conf"
# for file in $file_name; do
#     [ -f ${file} ] && echo "The file ${file} was found." || echo "*** Error: The file ${file} was missing.***"
# done

# for i in {2..254}
# do
#     ip=127.0.0.$i
#     pint -cl -W1 $ip &>/dev/null
#     if [ $? -eq 0 ]
#     then
# 	echo "$ip" | tee -a ip.txt
#     fi
# done

if [ $# -eq 0 ]
then
    echo "Usage: `basename $0` <file>"
    exit 1
fi

if [ ! -f $1 ]
then
    echo "error file"
    exit2
fi
