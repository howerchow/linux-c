#!/usr/bin/bash

# https://blog.csdn.net/wisdom_futrue/category_12168120.html

read -p "input a username: " user

id $user &>/dev/null
if  [ $? -ne 0 ]
then
    echo "no sucher user: $user"
    exit 1
fi

read -p "Are you sur?[y/n]: " action
case $action in
    y|Y|yes|YES )
	userdel -r $user
        ;;
    * )
	echo "error"
	;;
esac
