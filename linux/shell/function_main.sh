#!/usr/bin/env bash

# 加载函数文件

# . ./my_function.sh

source ./my_function.sh

# echo $(to_lower $1)

# result=$(db1)
# echo "The new value is $value"

# read -p "Enter a number: " value
# db1
# echo $value


function func2() {
    local temp=$[ $value + 5 ] #局部变量
    result=$[ $temp * 2 ]
}
temp=4
value=6
func2
echo "The result is: $result"
echo "The temp is: $temp"
echo "The value is: $value"
