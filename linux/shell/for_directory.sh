#!/usr/bin/env bash

for file in $HOME/Documents/*; do
    if [ -d $file ] # 如果是目录
    then
	echo "$file is a directory"
    elif [ -f $file ] # 如果是文件
    then
	echo "$file is a file"
fi

done
