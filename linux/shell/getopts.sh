#!/bin/sh
# simple demonstration of the getopts command
#

# while getopts ab:c opt
# do
#     case "$opt" in
# 	a) echo "Found the -a option: $opt $OPTIND";;
# 	b) echo "Found the -b option: $OPTARG $OPTIND";;
# 	c) echo "Found the -c option $opt";;
# 	*) echo "Unknown option: $opt"
#     esac
# done


# 处理带值的选项
echo
while getopts :ab:cd opt
do
    case "$opt" in
	a) echo "Found the -a option" ;;
	b) echo "Found the -b option, with value $OPTARG" ;;
	c) echo "Found the -c option" ;;
	d) echo "Found the -d option" ;;
	*) echo "Unknown option: $opt" ;;
    esac
done

shift $[ $OPTIND - 1 ]
count=1
echo
for param in $@; do
    echo "$param"
done
