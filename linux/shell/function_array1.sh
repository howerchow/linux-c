#!/bin/bash

function add_array_1() {
    local original_array=($@) # 函数接受所有参数赋值给数组
    local new_array=($*)
    for (( i=0;i<$#;++i )) { # $# 参数个数
	new_array[$i]=$(( 2 * ${original_array[$i]} ))
    }
	#函数输出数组(所有元素)
	echo ${new_array[@]}
}

function add_array_2() {
    local i=0
    for elem  in $*; do
	local new_array[i++]=$[$elem*2]
    done
	echo ${new_array[@]}
}

my_array=(1 2 3 4 5)
result_1=`add_array_1 ${my_array[*]}` # 传递所有数组元素
echo ${result_1[@]}

result_2=`add_array_2 ${my_array[*]}` # 传递所有数组元素
echo ${result_2[@]}

# 函数接受位置参数 $1 $2 $3
# 函数接收数组变量 $* $@
# 函数使用参数的个数 $#
# 函数接受所有参数赋值给数组 new_array=($*)
