#!/usr/bin/bash

# while true; do
#     echo "Do something..."
#     echo "Hit [CTRL + C] to stop!"
#     sleep 1
# done

while read line; do
    if [ -z "$line" ]
    then
	continue;
    fi
    echo $line
done < user.txt
