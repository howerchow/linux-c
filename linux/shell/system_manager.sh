#!/usr/bin/bash

function menu() {
    cat <<-EOF
###########################
#    h. help              #
#    f. disk partition    #
#    d. filesystem        #
#    m. memory		  #
#    u. system load	  #
#    q. exit   		  #
###########################
EOF
}

menu
while true; do
    read -p "Please input[h for help]: " action
    case $action in
	h )
	    clear
            menu
	    read -p "Please input[h for help]: " action
            ;;
	f )
	    fdisk -l
	    ;;
	d )
	    df -Th
	    ;;
	m )
	    free -m
	    ;;
	u )
	    uptime
	    ;;
	q )
	    break;
	    ;;
	"" )
	    ;;
	* )
	    echo "error"
	    ;;
    esac
done

echo "finish..."
