#!/usr/bin/env bash

# -s 压缩空行
# cat -s lines.txt

# -T 显示tab
# cat -T lines.txt

# 显示行号
cat -n lines.txt
