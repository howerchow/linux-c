#!/usr/bin/env bash

# awk [option]  'commands' filename

# command
# BEGIN{} {} END{}
#行处理前 行处理 行处理后
# awk 'BEGIN{print 1/2} {print "OK"} END{'--------'}' /etc/hosts

# awk -F: '{print $1 $2}' /etc/passwd

# 记录与字段相关内部变量
# $0 保存当前记录的内容
# NR
# FNR
# RS
# ORS
# FS
# OFS
# NF

# awk '/^root/' /etc/passwd
# awk '$0 ~ /^root/' /etc/passwd
# awk '!/root/' /etc/passwd

# awk -F: '$1 ~ /^root/' /etc/passwd
# awk -F: '$NF !~ /root$/' /etc/passwd

# awk -F: '{username[++x]=$1} END{for(i in username) {print i,username[i]}}' /etc/passwd
