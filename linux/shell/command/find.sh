#!/usr/bin/env bash

# fine pathname -options [-exec]
# -options:
# -name 按文件名查找
# -perm
# -user
# -group
# -mtime -n 几天內 +n 几天前
# -type b块文件 d目录 c字符设备 p管道文件  l符号链接文件 f普通文件
# -regex 正则搜索
# -iregex 正则搜索 忽略大小写

# find . -name *.txt
# find . -perm 764
# find . -mtime -2
 find . -mtime -3 -name "*.txt" -exec rm -i {} \;
# find . -name *.txt -delete
# find . -name "*.sh" -o -name "*.txt"
# 排除目录
# find . -path "command" -prune | xargs grep 'http'
