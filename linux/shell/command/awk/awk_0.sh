#!/usr/bin/env bash

# awk 'BEGIN{print "hello"} {print $0} END{print "bye"}' txt/a.txt
# awk -f test.awk -e 'END{print $0}' txt/a.txt

# awk '3>2 && 3>1{print "hello"}' txt/a.txt
# awk '!0 {print "world"}' txt/a.txt

# RS: Record Separator
# awk 'BEGIN{RS="^$"} {print $0}' txt/a.txt
# awk 'BEGIN{RS="\n+"} {print $0}' txt/a.txt

# awk '{print $(NF-1)}' txt/a.txt

# awk 'BEGIN{print "hello world" > "a.txt"}'

# awk 'BEGIN{OFMT="%.2f"; print 3.9999}'

awk 'BEGIN{printf "%c %E %i %o %#x %3d\n", 65,234,4,8,20,50}'
