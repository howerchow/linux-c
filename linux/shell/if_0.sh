#!/bin/bash

# 条件测试
# test 条件表达式
# [ 条件表达式 ]
# [[ 条件表达式 ]]

# 字符串比较
# 使用双引号

pass="jack"
if [ "$pass" = "jack" ] #字符串比较是否相同
then
    echo "verified pass"
elif [ "${pass}" != "jack"  ] #字符串比较不相同
then
    echo "Access denied."
fi

integer=4
if [ $integer -gt 5 ] #数值比较
then
    echo "interger greater than 5"
else
    echo "interger is not greater than 5"
fi

# 检查目录
jump_directory=/home/raymond
if [ -d $jump_directory ]
then
    cd $jump_directory || exit
    ls -alFh
    cd - || exit
else
    echo "The $jump_directory doesn't exist."
fi

# 检查文件
location=$(pwd)
file_name="hi.txt"
if [ -d $location ]
then
    echo "$location exists!"
    if [ -f $location/$file_name ] && [ -s $location/$file_name ] && [ -w $location/$file_name ] # 复合条件测试
    then
	date >> $location/$file_name
	echo "date done"
    else
	echo "$file_name can't write"
    fi
fi


# 双方括号 字符串比较(模式匹配)
if [[ $USER == r* ]]
then
    echo "Hello $USER"
else
    echo "Sorry, I don't know you"
fi
