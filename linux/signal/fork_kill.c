#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	pid_t pid;
	int status;
	fflush(stdout);

	pid = fork();
	if (pid == -1) {
		perror("fork");
		exit(EXIT_FAILURE);
	}
	if (pid == 0) {
		printf("Child\n");
		sleep(10);
		return;
	} else {
		sleep(1);
		kill(pid, SIGABRT); //传送信号给指定进程
		wait(&status);

		if (WIFSIGNALED(status)) {
			printf("Child receive signal: %d\n",WTERMSIG(status));
		}
	}
}
