#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int main(int argc, char *argv[])
{
	sigset_t blockSet, preMask;
	sigemptyset(&blockSet);
	sigaddset(&blockSet, SIGINT);

	/*
	  int sigprocmask(int how, const sigset_t *_Nullable restrict set,
                                  sigset_t *_Nullable restrict oldset);

	how: SIG_BLOCK SIG_UNBLOCK SIG_SETMASK

	 */
	int reval = sigprocmask(SIG_BLOCK, &blockSet, &preMask);
	if (reval == -1) {
		fprintf(stderr, "sigprocmask SIG_BLOCK failed!");
		exit(EXIT_FAILURE);
	}


        // Restore previout signal mask, Unblock SIGINT
	reval = sigprocmask(SIG_SETMASK, &preMask,NULL);
	if (reval == -1) {
		fprintf(stderr, "sigprocmask SIG_SETMASK failed!");
		exit(EXIT_FAILURE);
	}
	return 0;
}
