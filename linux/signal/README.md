信号会打断阻塞的系统调用
标准信号的响应没有严格的顺序

# 常用函数
kill()
raise()
alarm()
pause()
abort()
system()
sleep()
