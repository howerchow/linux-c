#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <error.h>
#include <fcntl.h>

#define CPS 10;
#define BURST 100;
static volatile int token = 0;

static void
alarm_handler() {
	alarm(1);
	++token;
	if (token > BURST) {
		token = BURST;
	}
}

int main(int argc, char *argv[])
{
	int sfd, dfd = 1;
	char buf[BUFSIZ] = {0};
	int len, ret, pos;

	if (argc < 2) {
		fprintf(stderr, "Useage....\n");
                exit(EXIT_FAILURE);
	}

	signal(SIGALRM, alarm_handler);
	alarm(1);
	do {
		sfd = open(argv[1], O_RDONLY);
		if (sfd == -1) {
			if (errno != EINTR) {
				perror("open()");
				exit(EXIT_FAILURE);
			}
		}
	} while (sfd < 0);

	while (true) {
		while (token <= 0) {
			pause();
		}
		--token;

		len = read(sfd, buf, BUFSIZ);
		if (len < 0) {
			if (errno == EINTR) {
				continue;
			}
			perror("read()");
			break;
		}

		if(len == 0) {
			break;
		}

		pos = 0;
		while (len > 0) {
			ret = write(dfd, buf +pos, len);
			if (ret < 0) {
				perror("write()");
				exit(EXIT_FAILURE);
			}
		}

	}
	return 0;
}
