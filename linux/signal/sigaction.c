#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>

void sig_action(int sg, siginfo_t *sginfo, void *u)
{
	printf("%d\n" ,sg);
}

int main(int argc, char *argv[])
{
	sigset_t sigset = {0};
	sigemptyset(&sigset);

	/*
	  struct sigaction {
               void     (*sa_handler)(int);
               void     (*sa_sigaction)(int, siginfo_t *, void *);
               sigset_t   sa_mask;
               int        sa_flags;
               void     (*sa_restorer)(void);
           };
	 */
	struct sigaction act = {
		.sa_sigaction = sig_action ,
		.sa_mask      = {0}, //信号搁置
		.sa_flags     = SA_SIGINFO // SA_NOCLDSTP SA_NOCLDWAIT SA_NODEFER
	};

	/*
	  int sigaction(int signum,
                     const struct sigaction *_Nullable restrict act,
                     struct sigaction *_Nullable restrict oldact);
	*/
	sigaction(SIGINT, &act, NULL); // 查询或设置信号处置函数

	while (true) {
		printf("Runing!");
		pause();
	}

	return 0;
}
