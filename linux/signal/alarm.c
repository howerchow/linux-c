#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>

static void
handler(int sig)
{
	printf("alarm(): %d\n", sig);
}

int main(int argc, char *argv[])
{
	signal(SIGALRM, handler);
	alarm(5);

	for (size_t i = 0; i < 7; ++i) {
		printf("hello world %d\n",i);
		sleep(1);
	}

	return 0;
}
