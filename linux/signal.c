#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

/*
   ./a.out &
   kill -SIGINT pid
*/

static void sig_handler(int sig){
	//printf("%s\n" ,strsignal(
	psignal(sig, " Ouch");
}

int main(int argc, char *argv[])
{
	int j = 1;
	if (signal(SIGINT, sig_handler) == SIG_ERR) {
		perror("signal():");
		exit(EXIT_FAILURE);
	}

	while(1){
		pause();
	}

	return 0;
}
