#include <stdio.h>
#include <stdlib.h>

union {
        int i;
        char x[ 2 ];
} a;

union {
        int i;
        char x[ 2 ];
} b;

int main( int argc, char *argv[] ) {
        a.i = 0;
        a.x[ 0 ] = 0xa1;
        a.x[ 1 ] = 0x80;
        printf( "%#x\n", a.i );

        b.x[ 0 ] = 11;
        b.x[ 1 ] = 1;
        printf( "%d\n", b.i );

        return 0;
}
