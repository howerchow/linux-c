#include <stdio.h>
#include <stdlib.h>
#define NDEBUG //clsoe asser()
//#undef NDEGUG //open deug 
#include <assert.h>
#include <signal.h>

static int val = 0;

static void field_abort (int sig) {
	if (val == 1) {
		puts("SUCCESS testing <assert.h>");
		exit(EXIT_FAILURE);
	} else {
		puts("FAILURE testing <assert.h>");
		exit(EXIT_FAILURE);
	}
}

static void dummy () {
	int i = 0;
	assert(i == 0);
	assert(i == 1);
}

int main(int argc, char *argv[])
{
	assert(signal(SIGABRT, &field_abort) != SIG_ERR);
	dummy();
	assert(val == 0);
	++val;
	fputs("Sample assertion failure message", stderr);
	assert(val == 0);
	puts("FAILURE testing <assert.h>");
	return 0;
}
