#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	FILE *fp;
	char *line;
	ssize_t len;
	fp = fopen("math.c", "r");
	//int c = fgetc(fp);

	getline(&line, &len, fp);

	printf("%d\n", len);
	puts(line);
	free(line);
	fclose(fp);

	return 0;
}
