#include <stddef.h>
#include <stdio.h>
#include <string.h>
void print2(int (*p)[5], int x, int y);

int main(int argc, char *argv[])
{
  int arr[10] = {1,2,3,4};
  // arr     首元素地址
  // &arr[0] 首元素地址
  // &arr    数组地址
  int (*p)[10] = &arr;
  for (size_t i = 0; i < 10; ++i) {
    printf("%d\n", (*p)[i]);
  }

  char *arr1[5] = {0};
  char* (*p1)[5] = &arr1;   //数组指针
  int (*p2[10])[5] = {0};  // 数组指针数组

  int (*(*p3)[10])[5] = &p2; // 数组指针数组指针

  // 函数指针数组指针 见function_pointer.c

  int arr2[3][5] = {
    {1,2,3,4,5},
    {2,3,4,5,6},
    {3,4,5,6,7}
  };
  print2(arr2, 3, 5);

  //指针数组

}

void print2(int (*p)[5], int x, int y) {
  for (size_t i = 0; i < x; ++i) {
    for (size_t j = 0; j < x; ++j) {
      // printf("%d ", *(*(p+i) + j) );
      printf("%d ", p[i][j] );
    }
    printf("\n");
  }

}
