#include <stddef.h>
#include <stdio.h>
#include <string.h>

// 一维数组传参
void test(int arr[]) {}    // OK
void test(int arr[10]) {}  // OK
void test(int *arr) {}     // OK

void test2(int *arr[20]) {} // OK
void test2(int **arr) {} // OK

// 二维数组传参
test3(int(*arr)[3]) {}

int main(int argc, char *argv[])
{
  int arr[10] = {0};
  int *arr2[20] = {0};

  test(arr);
  test2(arr2);

  int array[3][5] = {0};
  test3(array); // OK

}
