#include <stdio.h>
#include <stdlib.h>

int main() {

  int a[] = {0x11, 0x22};
  int *p = a;
  printf("%#X \n", *p);

  return 0;
}
