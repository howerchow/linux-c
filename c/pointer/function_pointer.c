#include <stddef.h>
#include <stdio.h>
#include <string.h>

void f(char *p) {
  printf("f: %s\n",p);
}

void g(char *p) {
  printf("g: %s\n",p);
}

int main(int argc, char *argv[])
{
  void (*pf)(char *p) = f; // 函数指针
  pf("hello world");

  void (*pfa[2])(char *p) = {f,g}; // 函数指针数组 用途：转移表
  for (size_t i = 0; i < 2; ++i) {
    pfa[i]("hello world");
  }

  void (*(*p)[2])(char *p) = &pfa; // 函数指针数组指针

}
