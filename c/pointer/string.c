#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main( int argc, char *argv[] )
{
  const char *p = "abcdef";
  printf("%c\n", *p);
  printf("%s\n", p);

  char arr1[] = "abcdef";
  char arr2[] = "acbdef";
  char *p1 = "abcd";
  char *p2 = "abcd";

  printf("%d\n", *p1);

  return 0;

}
