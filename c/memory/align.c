
/* 结构体的内存对齐是拿空间来换取时间的做法 */

#include <stdalign.h>
#include <stdio.h>
#include <stddef.h>

typedef struct test1 {
        char a;
        char b;
        int c;
} t1;

typedef struct test2 {
        char a;
        int c;
        char b;
} t2;

typedef struct test3 {
        char a;
        char b;
	int c;
        t1 d;
} t3;

typedef struct S {
        char a : 1;
        char b : 2;
        char c : 3;
        char d : 4;
	char e : 5;
} S;


int main( int argc, char *argv[] ) {
        printf( "%zu\n", sizeof( t1 ) );
        printf( "%zu\n", sizeof( t2 ) );
        printf( "%zu\n", sizeof( t3 ) );

        printf( "a= %zu\n", offsetof( t3, a ) );
        printf( "b= %zu\n", offsetof( t3, b ) );
        printf( "b= %zu\n", offsetof( t3, c ) );
        printf( "b= %zu\n", offsetof( t3, d ) );

	S s;
        printf( "%zu\n", sizeof( s ) );

        return 0;
}
