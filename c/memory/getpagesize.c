#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	size_t pagesize = {0};
	pagesize = getpagesize();

	printf("%zu\n",pagesize);

	return 0;
}
