#include <stdio.h>
#include <stdlib.h>

int main( int argc, char *argv[] ) {

        struct test {
                int a[ 10 ];
                char b[ 20 ];
        };

        struct test *p = calloc( 10, sizeof( struct test ) );
        p->a[ 1 ] = 1;
        p->b[ 1 ] = 2;

        printf( "%d\n", p->a[ 1 ] );
        printf( "%d\n", p->b[ 1 ] );
        printf( "calloc: %p\n", p );

        struct test *p1 = realloc( p, 1024 ); // 自动释放p
        printf( "%d\n", p->a[ 1 ] );
        printf( "reallo: %p\n", p1 );

        free( p1 );
        return 0;
}
