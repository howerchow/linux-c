#include <stdio.h>
#include <stdlib.h>
#include <stdalign.h>
#include <string.h>
#include <stddef.h>

struct S1 {
  char c1;
  int a;
  char c2;
  char c3;
};

/* 位段 */
struct S {
  int a : 2;
  int b : 5;
  int c : 10;
  int d : 30;
};

int main(int argc, char *argv[]) {
  struct S1 s1 = {0};
  printf("%zu\n", sizeof(s1)); // 12 byte
  printf("%zu\n", offsetof(struct S1, a)); // 4 byte

  printf("%zu\n", sizeof(struct S)); // 8 byte

}
