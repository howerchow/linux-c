#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
  char json[] = "key-value\r\nhelloworld";

  char *p = strpbrk(json, "\r\n");
  //char *p = strstr(json, "\r\n");
  p += strspn(p, "\r\n");

  printf("%s\n", p);

}
