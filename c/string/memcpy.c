#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
  char *p1 = "hello world";
  char *p2 = malloc(strlen(p1));
  char *p3 = memcpy(p2, p1, strlen(p1));

  printf("p2: %s\np3: %s\n" ,p2,p3);

  free(p2);
  p3 = p2 = NULL;


  char str[] = "hello world";
  char str1[11];
  memccpy(str1, str, ' ', strlen(str));
  printf("%s\n" ,str1);

  char name[] = "Tom Hanks";
  char name1[10];
  strcpy(name1, name);
  puts(name1);

  return 0;
}
