#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char *argv[])
{
  char str[] = "dd123abc";
  char *endptr = NULL;
  int i = 0, j = 0, len = 0;

  while (j < strlen(str)) { // 去除非数字
      if (isdigit(str[j]))
	{
	  str[i++] = str[j];
	  ++len;
	}
      ++j;
  }

  char str1[BUFSIZ] = {0};
  strncpy(str1, str, len);

  puts(str1);

  double a = strtod(str, &endptr);
  printf("%f %s\n",a,endptr);

  return 0;
}
