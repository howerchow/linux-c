#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

/*
  char *strstr(const char *haystack, const char *needle);

  - 查找子串(needle)在母串(haystack)中出现的位置
  - 找到子串, 则返回子串第一次出现的地址pointer
  - 未找到子串, 则返回NULL

 */

/* 1. 单次查找，返回查找成功的字符串以及后面的字符串*/
void test1() {
  char *haystck = "The  strstr() function finds the first occurrence of the substring nee‐";
  char *needle  = "substring";
  char *p = strstr(haystck, needle);
  puts(p); // substring nee‐
}

/* 2. 单次查找,返回子串在母串中位置*/
void test2() {
  char *haystck = "The substring nee‐";
  char *needle  = "substring";
  char *p = strstr(haystck, needle);
  if(p != NULL) {
      ptrdiff_t pdf = p  - haystck;
  printf("%td\n", pdf);
}
}

/* 3. 多次查找,记录子串在母串中出现次数 */
void test3() {
  char *haystck = "The substring nee‐substring， substring";
  char *needle  = "substring";
  int k = 0;
  char *p = haystck;
  size_t len = strlen(needle);
  while ((p = strstr(p, needle)) != NULL) {
    ++k;
    p += len;
  }
  printf("%d\n", k);
}

int main(int argc, char *argv[])
{

  test1();
  test2();
  test3();

  return 0;
}
