#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
  char dest[40] = {0}, src[12] = {0};

  strcpy(src, "hello world");
  strncpy(dest, src, 5);
  printf("src = %s, dest = %s\n" ,src, dest);
  return 0;
}
