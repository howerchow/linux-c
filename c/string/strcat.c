#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// 字符串追加

int main(int argc, char *argv[])
{
  char dest[50] = {0}, src[50] = {0};

  strcpy(dest, "world"); // "world"
  strcpy(src, "hello "); // "hello world"

  strcat(src, dest);
  size_t len = strlen(src); // 11
  printf("%d\n",len);
  puts(src);
  puts(dest);
  return 0;
}
