#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
  size_t strspn(const char *s, const char *accept);
  - 字符串s从第一个字符开始有几个字符在字符串accept中
 */

int main(int argc, char *argv[])
{
  char str1[]   = "CDEFG012345";
  char str2[]   = "FEDC";
  char str3[]   = "012";

  // 连续包含str2的字符数
  size_t index   = strspn(str1, str2);

  // 连续不包含str3的字符数 5
  size_t indexc  = strcspn(str1, str3);

  printf("%zu\n" , index);  // 3
  printf("%zu\n" , indexc); // 5
  return 0;
}
