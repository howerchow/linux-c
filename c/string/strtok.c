#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* 切割字符串 */

int main(int argc, char *argv[]) {
  char arr[] = "zpw@bit@.tech"; /* 会被改变 */
  char *p = "@.";

  char buffer[BUFSIZ] = {0};
  strncpy(buffer, arr, strlen(arr));

  char *ret = strtok(arr,p);
  while(ret) {
    puts(ret);
    ret = strtok(NULL,p); // 继续切割
  }

}
