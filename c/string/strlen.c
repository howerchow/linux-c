#include <stdio.h>
#include <string.h>


int main(int argc, char *argv[])
{
  char *string = "helloworld";

  size_t str_len, sizeof_len;
  str_len = strlen(string);
  sizeof_len = sizeof(string);

  printf("str_len = %d, sizeof_len = %d\n", str_len, sizeof_len);

  return 0;
}
