#include <stdio.h>
#include <stdlib.h>

//#define MY_CONSTANT 31.4

#define HELLO(x) puts("Hello, " #x);
#define TOKENCONCAT( x, y ) x##y


int main( ) {

#ifdef DEBUG
        printf( "%a \n", MY_CONSTANT*3 );
#else
        printf( "MY_CONSTANT Not defined \n" );
#endif

	int a  = TOKENCONCAT(11,22);
        printf( "%d \n", a );

        char *s1 = "hello";
	char* s2 = "world";

	char*s1s2 = "helloworld";
        puts( TOKENCONCAT( s1, s2 ));

        return 0;
}
