#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>

void test_1() {
  struct timespec ts;
  timespec_get(&ts, TIME_UTC);
  char buff[100];
  strftime(buff, sizeof buff, "%D %T", gmtime(&ts.tv_sec));
  printf("Current time: %s.%09ld UTC\n", buff, ts.tv_nsec);
}

void test_2(){
  struct timeval tv;
  struct timezone tz;
  gettimeofday(&tv, &tz);
}

int main(void)
{
  test_2();
}
