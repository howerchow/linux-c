#include <stdio.h>
#include <stdlib.h>
#include <stdalign.h>
#include <string.h>

int main(int argc, char *argv[]) {
  int a = 10000;
  FILE *pf = fopen("text.txt", "wb");
  fwrite(&a, 4, 1, pf);
  fclose(pf); pf = NULL;

  return 0;
}
