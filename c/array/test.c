#include <stddef.h>
#include <stdio.h>
#include <string.h>

typedef int (*P)(int, int);

int f1(int a, int b) {
  return 1;

}
int f2(int a, int b) { return 2; }

int f3(int a, int b) {
  return 3;

}
int f4(int a, int b) {
  return 4;
}

void F(P (*p)[2], int s) {

  for (size_t i = 0; i < 2; ++i) {
    for (size_t j = 0; j < 2; ++j) {
      printf("%d\n",p[i][j](1,2));
    }
  }

}

int main(int argc, char *argv[]) {
  P a[2][2] = {
    {f1, f2},
    {f3, f4} };

  F(a,2);

}
