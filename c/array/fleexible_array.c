#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct S {
  char n;
  int arr[0]; /*柔性数组 未知大小 数组大小可以是调整的*/
};

int main(int argc, char *argv[]) {
  struct S s;
  printf("%zu\n", sizeof(s)); // 4 byte

  //struct S *ps = malloc(sizeof(struct S) + 5 * sizeof(int));
  struct S *ps = malloc(sizeof(*ps) + 5 * sizeof(int));
  printf("%zu\n", sizeof(*ps)); // 4 byte
  for (size_t i = 0; i < 5; ++i) {
    ps->arr[i] = i;
  }
  for (size_t i = 0; i < 5; ++i) {
    printf("%d\n",ps->arr[i]);
  }

  free(ps);
  ps = NULL;

}
