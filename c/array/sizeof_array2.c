#include <stddef.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
  // 二维数组
  int a[3][4] = {
    {1,6,4,5},
    {2,3,4,5},
    {3,4,5,6}
  };
  printf("%zu \n", sizeof(a)); // 3*4*4=48
  printf("%zu \n", sizeof(a[0][0])); // 4
  printf("%zu \n", sizeof(a[0])); // sizeof(第一行数组名) 4*4=16
  printf("%zu \n", sizeof(a[0]+1)); // sizeof(&a[0][1]) 8
  printf("%zu \n", sizeof(*(a[0]+1))); // sizeof(a[0][1]) 4
  printf("%zu \n", sizeof(a + 1));     // 第二行地址 8
  printf("%zu \n", sizeof(*(a + 1)));  // sizeof(a[1]) 16
  printf("%zu \n", sizeof(&a[0] + 1)); // &a[1] 8
  printf("%zu \n", sizeof(*a)); // sizeof(a[0]) 16
  printf("%zu \n", sizeof(a[3])); // 16

}
