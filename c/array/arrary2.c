#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

void print1(int (*p)[4], int row, int col) {
  for (size_t i = 0; i < row; ++i) {
    for (size_t j = 0; j < col; ++j) {
      printf("%d ", *(*(p + i) + j));
      if (j == 3) {
	printf("\n");
      }
    }
  }
}

int main(int argc, char *argv[])
{
  int arr[3][4] = {
    {1,2,3,4},
    {5,6,7,8},
    {7,8,9,0}
  };

  //print1(arr,3,4);

  /* char a[][10] = {"12345","678921"}; */
  /* char *p = a[0]; */
  /* printf("%s\n",strlen(p + 1)); */

  printf("%d\n", sizeof *arr);

  return 0;
}
