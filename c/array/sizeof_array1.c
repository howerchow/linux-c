#include <stddef.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
  //一维数组
  // 数组名: 首元素地址 两个例外
  // 1. sizeof(数组名) - 数组名表示整个数组
  // 2. &数组名        - 数组名表示整个数组
  // 3. 除此之外的所有数组名都表示首元素地址
  // 整形数组
  int a[4] = {1,2,3,4};
  printf("%zu\n", sizeof(int)); // int: 4 byte
  printf("%zu\n", sizeof(a));   // sizeof(数组名) 数组总大小 4*4 = 16 byte
  printf("%zu\n", sizeof(a+0)); // sizeof(首元素地址)  sizeof(int*) 8 byte
  printf("%zu\n", sizeof(*a));  // sizeof(首元素) 4 byte
  printf("%zu\n", sizeof(a+1));  // sizeof(第二个元素地址)  sizeof(int*) 8 byte
  printf("%zu\n", sizeof(&a));  // sizeof(数组指针) 8 byte
  printf("%zu\n", sizeof(*&a));  // sizeof(数组名) 16 byte
  printf("%zu\n", sizeof(&a + 1));      // 跳过整个数组后的地址 8 byte
  printf("%zu\n", sizeof(&a[0]));      //  地址 8 byte
  printf("%zu\n", sizeof(&a[0] + 1)); // 第二个元素地址 8 byte

  // 字符数组
  printf("字符数组 sizeof:\n");
  char arr[] = {'a', 'b', 'c', 'd','e','f'};
  printf("%zu\n", sizeof(char)); // char: 1 byte
  printf("%zu\n", sizeof(arr));   // sizeof(数组名) 数组总大小 1*6 = 6 byte
  printf("%zu\n", sizeof(arr+0)); // sizeof(首元素地址)  sizeof(int*) 8 byte
  printf("%zu\n", sizeof(*arr));  // sizeof(首元素) 1 byte
  printf("%zu\n", sizeof(arr+1));  // sizeof(第二个元素地址)  sizeof(int*) 8 byte
  printf("%zu\n", sizeof(&arr));  // sizeof(数组指针) 8 byte
  printf("%zu\n", sizeof(*&arr));  // sizeof(数组名)  数组总大小 1*6 = 6 byte
  printf("%zu\n", sizeof(&arr + 1));      // 跳过整个数组后的地址 8 byte
  printf("%zu\n", sizeof(&arr[0]));      //  地址 8 byte
  printf("%zu\n", sizeof(&arr[0] + 1)); // 第二个元素地址 8 byte

  printf("字符数组 strlen:\n");    // strlen 求字符串长度 目的是找'\0'
  printf("%zu\n", strlen(arr));   // 随机值 6
  printf("%zu\n", strlen(arr+0)); // 随机值 6
  //printf("%zu\n", strlen(*arr));  // strlen(97) ERROR
  printf("%zu\n", strlen(arr+1));  // 5
  //printf("%zu\n", strlen(&arr));  // 随机值 6
  printf("%zu\n", strlen(*&arr));  // strlen(arr) 随机值6
  //printf("%zu\n", strlen(&arr + 1));      // 随机值 0
  printf("%zu\n", strlen(&arr[0]));      //  随机值 6
  printf("%zu\n", strlen(&arr[0] + 1)); // 随机值 5

  // 字符串
  printf("字符串 sizeof:\n");
  char arrs[] = "abcdef"; // 'a' 'b' 'c' 'd' 'e' 'f' '\0'
  printf("%zu\n", sizeof(arrs)); // 7
  printf("%zu\n", sizeof(arrs+0)); // 8
  printf("%zu\n", sizeof(*arrs)); // 1
  printf("%zu\n", sizeof(arrs[1])); // 1
  printf("%zu\n", sizeof(&arrs)); // 8
  printf("%zu\n", sizeof(&arrs+1)); // 8
  printf("%zu\n", sizeof(&arrs[0] + 1)); // 8

  printf("字符串 strlen:\n");
  printf("%zu\n", strlen(arrs)); // 6
  printf("%zu\n", strlen(arrs+0)); // 6
  //printf("%zu\n", strlen(*arrs));
  //printf("%zu\n", strlen(arrs[1]));
  //printf("%zu\n", strlen(&arrs)); // 6
  //printf("%zu\n", strlen(&arrs+1)); // 随机值 0
  printf("%zu\n", strlen(&arrs[0] + 1)); // 5

  // 常量字符串
  printf("常量字符串 sizeof:\n");
  char *p = "abcdf";
  printf("%zu \n", sizeof(p)); // 8
  printf("%zu \n", sizeof(p+1)); // 8
  printf("%zu \n", sizeof(*p)); // 1
  printf("%zu \n", sizeof(p[0])); // 1
  printf("%zu \n", sizeof(&p)); // 8
  printf("%zu \n", sizeof(&p + 1)); // 8
  printf("%zu \n", sizeof(&p[0] + 1)); // 8

  printf("常量字符串 strlen:\n");
  printf("%zu \n", strlen(p)); // 5
  printf("%zu \n", strlen(p+1)); // 4
  //printf("%zu \n", strlen(*p)); // ERROR
  //printf("%zu \n", strlen(p[0])); // ERROR
  //printf("%zu \n", strlen(&p)); // 随机值
  //printf("%zu \n", strlen(&p + 1)); // 随机值
  printf("%zu \n", strlen(&p[0] + 1)); // 4

  return 0;
}
