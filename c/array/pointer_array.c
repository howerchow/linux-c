#include <stdio.h>
#include <stdlib.h>

void examine1() {
  int a[5] = {1, 2, 3, 4, 5};
  int *ptr = (int *)(&a + 1);
  printf("%d,%d \n", *(a+1), *(ptr-1)); // 2,5
}

void examine2() {
  struct Test {
    int Num;
    char *pcName;
    short sDate;
    char cha[2];
    short sBa[4];
  } *p;
  p = (struct Test *)0x100000;
  // 指针 + 整数
  // 取决于指针类型
  printf("%zu\n",sizeof(struct Test));
  printf("%p\n", p + 0x1);
  printf("%p\n", (unsigned long)p + 0x1);
  printf("%p\n", (unsigned int*)p + 0x1);
}

void examine3() {
  int a[4] = {1, 2, 3, 4};
  int *ptr1 = (int *)(&a + 1);
  //int *ptr2 = (int *)((int)a + 1);
}

/* void examine4() { */
/*   int a[5][5]; */
/*   int(*p)[4]; */
/*   p = a; */
/*   printf("%p %d \n", &p[4][2] - &a[4][2], &p[4][2] - &a[4][2]); */
/* } */

void examine5() {
  int aa[2][5] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  int *ptr1 = (int *)(&aa + 1);
  int *ptr2 = (int *)(*(aa + 1));
  printf("%d, %d\n", *(ptr1 - 1), *(ptr2 - 1)); // 10 5
}

void examine6() {
  char *a[] = {"work", "at", "alibaba"};
  char **pa = a;
  ++pa;
  printf("%s\n", *pa); // at
}

void examine7() {
  char *c[] = {"ENTER", "NEW", "POINT", "FIRST"};
  char **cp[] = {c + 3, c + 2, c + 1, c};
  char ***cpp = cp;
  printf("%s\n", **++cpp);
  printf("%s\n", *--*++cpp + 3);
  printf("%s\n", *cpp[-2] + 3);
  printf("%s\n", cpp[-1][-1] + 1);
}

int main(int argc, char *argv[])
{
  // examine1();
  // examine2();
  // examine3();
  // examine4();
  // examine5();
  // examine6();
  examine7();
  return 0;
}
