#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int int_compare(const void *one,const void *two) {
	return *(int *)one - *(int *)two;
}

int main(int argc, char *argv[])
{
	/* int arr[3] = {1,2,3}; */
	/* int *p = arr; */
	/* printf("%p\n",p); */
	/* printf("%p\n",arr); */
	/* printf("%p\n",&arr); */
	/* printf("%p\n",&arr[0]); */

	/* printf("----------\n"); */
	/* int a[4] = {1,2,3,4}; */

	/* printf("%d %d\n",sizeof(*a)); */
	/* printf("%d\n", sizeof(&a)); */
	/* printf("%d\n",sizeof(&a + 1)); */

	/* printf("b----------\n"); */
	/* int b[4] = {1,2,3,4}; */
	/* int *ptr1 = (int *)(&b + 1); */
	/* //int *ptr2 = (int *)((int)b + 1); */
	/* printf("%#x, %#x\n",ptr1[-1] /\*,*ptr2 *\/); */

	/* printf("----------\n"); */

	/* int c[3][2] = {(0,1),(2,3),(4,5)}; */
	/* int *pc  = c[0]; */
	/* printf("%d\n",pc[0]); */

	/* int ap = -4; */
	/* int aa = 4; */
	/* printf("%p %d %p\n",ap,ap,aa); */

	printf("++++++++++++++++++++\n");
	int int_arr[] = {10, 5, 30, 15,20,30};
	int int_arr_len = sizeof(int_arr) / sizeof(*int_arr);

	int a = int_arr[3];
	qsort(int_arr, int_arr_len, sizeof(*int_arr), int_compare);

	for (size_t i = 0; i < int_arr_len; ++i) {
		printf("%d ",int_arr[i]);
	}
	printf("\n");

	return 0;
}
