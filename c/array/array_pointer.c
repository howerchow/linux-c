#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void myprint_obsolate(int arr[3][5], int x, int y) {
  for (size_t i = 0; i < x; ++i) {
    for (size_t j = 0; j < y; ++j) {
      printf("%d ",arr[i][j]);
    }
    printf("\n");
  }
}

void myprint(int (*p)[5], int x, int y) {
  for (size_t i = 0; i < x ; ++i) {
    for (size_t j = 0; j < y; ++j) {
      printf("%d ",*(*(p + i) + j));
    }
    printf("\n");
  }
}

int main(int argc, char *argv[])
{

  /* int array[10]; */
  /* int (*ap1)[] = &array; */
  /* int (*ap2)[10] = &array; */

  /* int *ip = array; */
  /* printf("%p, sizeof array == %zu\n", (void *)array, sizeof array); */

  /* // We cannot get sizeof *ap1, it is an incomplete type. */
  /* printf("%p\n", (void *)*ap1); */
  /* printf("%p, sizeof *ap2 == %zu (%zu)\n", */
  /* 	 (void *)*ap2, sizeof *ap2, 10 * sizeof(int)); */

  /* //  array_func(array); */

  //数组指针 (常用二维数组 函数传参)
  int arr[10] = {1,2,3,4,5,6,7,8,9,10}; // arr 数组名-> 首元素地址(绝大部分时候)
  int (*p)[10] = &arr;// &arr 数组的地址

  int arr2[3][5] = {
    {1,2,3,4,5},
    {2,3,4,5,6},
    {3,4,5,6,7}
  };

  int (*p2)[5] = arr2[0];

  myprint_obsolate(arr2, 3, 5);
  myprint(arr2, 3, 5);

}
