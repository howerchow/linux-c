#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <inttypes.h>
#include <assert.h>

int main(int argc, char *argv[])
{
  int array[5];

  printf("array == %p\n", (void *)array);

  for (size_t i = 0; i < 5; ++i) {
    printf("&array[%zu] == %p\n",i, (void*)&array[i]);
  }

  printf("sizeof(array) == %zu\n", sizeof(array));
  printf("5 * sizeof(int) == %zu\n", 5 * sizeof(int));

  int i;
  int *ip = &i;
  volatile int *ip2 = ip;
  assert(ip == ip2);

}
