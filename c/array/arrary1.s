	.file	"arrary1.c"
	.intel_syntax noprefix
	.text
	.globl	int_compare
	.type	int_compare, @function
int_compare:
.LFB6:
	.cfi_startproc
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	mov	rbp, rsp
	.cfi_def_cfa_register 6
	mov	QWORD PTR -8[rbp], rdi
	mov	QWORD PTR -16[rbp], rsi
	mov	rax, QWORD PTR -8[rbp]
	mov	edx, DWORD PTR [rax]
	mov	rax, QWORD PTR -16[rbp]
	mov	eax, DWORD PTR [rax]
	sub	edx, eax
	mov	eax, edx
	pop	rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6:
	.size	int_compare, .-int_compare
	.section	.rodata
.LC0:
	.string	"++++++++++++++++++++"
.LC1:
	.string	"%d "
	.text
	.globl	main
	.type	main, @function
main:
.LFB7:
	.cfi_startproc
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	mov	rbp, rsp
	.cfi_def_cfa_register 6
	sub	rsp, 64
	mov	DWORD PTR -52[rbp], edi
	mov	QWORD PTR -64[rbp], rsi
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR -8[rbp], rax
	xor	eax, eax
	lea	rax, .LC0[rip]
	mov	rdi, rax
	call	puts@PLT
	mov	DWORD PTR -32[rbp], 10
	mov	DWORD PTR -28[rbp], 5
	mov	DWORD PTR -24[rbp], 30
	mov	DWORD PTR -20[rbp], 15
	mov	DWORD PTR -16[rbp], 20
	mov	DWORD PTR -12[rbp], 30
	mov	DWORD PTR -48[rbp], 6
	mov	eax, DWORD PTR -20[rbp]
	mov	DWORD PTR -44[rbp], eax
	mov	eax, DWORD PTR -48[rbp]
	movsx	rsi, eax
	lea	rax, -32[rbp]
	lea	rdx, int_compare[rip]
	mov	rcx, rdx
	mov	edx, 4
	mov	rdi, rax
	call	qsort@PLT
	mov	QWORD PTR -40[rbp], 0
	jmp	.L4
.L5:
	mov	rax, QWORD PTR -40[rbp]
	mov	eax, DWORD PTR -32[rbp+rax*4]
	mov	esi, eax
	lea	rax, .LC1[rip]
	mov	rdi, rax
	mov	eax, 0
	call	printf@PLT
	add	QWORD PTR -40[rbp], 1
.L4:
	mov	eax, DWORD PTR -48[rbp]
	cdqe
	cmp	QWORD PTR -40[rbp], rax
	jb	.L5
	mov	edi, 10
	call	putchar@PLT
	mov	eax, 0
	mov	rdx, QWORD PTR -8[rbp]
	sub	rdx, QWORD PTR fs:40
	je	.L7
	call	__stack_chk_fail@PLT
.L7:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	main, .-main
	.ident	"GCC: (GNU) 13.2.1 20230801"
	.section	.note.GNU-stack,"",@progbits
