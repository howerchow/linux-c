#include <stddef.h>
#include <stdio.h>

static int j;

void func1( void ) {
        static int i = 0;
        ++i;
}

void func2( void ) {
        j = 0;
        ++j;
}

int main( ) {

        for ( size_t k = 0; k < 10; ++k ) {
                {
                        func1( );
                        func2( );
                }
        }

        printf( "j = %d\n", j );

        return 0;
}
