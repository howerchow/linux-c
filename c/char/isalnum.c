#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <threads.h>
#include <stdbool.h>

int main(int argc, char *argv[])
{
  char str[] = "123c@&#FDsp[e!/]";
  puts(str);
  for (size_t i = 0; i < strlen(str); ++i) {
    if ( isalnum(str[i]) ) { // 测试字符是否为英文字母或数字
			printf("%c is an alphanumeric character\n",str[i]);
    }
  }

  return 0;
}
