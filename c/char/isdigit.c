#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char *argv[])
{
	char str[] = "123c@&#FDsp[e!/]";
	puts(str);
	for (size_t i = 0; i < strlen(str); ++i) {
		if ( isdigit(str[i]) ) { // 测试字符是阿拉伯数字
			printf("%c is an alphanumeric character\n",str[i]);
		}
	}
	return 0;
}
