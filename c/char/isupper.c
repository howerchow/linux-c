#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char *argv[]) {
  char arr[] = "I Am Student";
  for (size_t i = 0; i < strlen(arr); ++i) {
    if (islower(arr[i])) {
      printf("%c\n",arr[i]);
    }
  }

}
